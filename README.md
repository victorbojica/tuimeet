## Student Meeting Web-Based Scheduling Solution

After the school year ends, universities all around the world start the selection process for new students. These selection processes are complicated, require careful planning, a lot of busy work for tracking all the updates, reporting and making sure all the data is correct.


Even if the selection process looks different for every university, the main pain-points are the same. Allowing students to register, making sure the student’s registration contains all the required paperwork, assessing the student and assigning the student to the right field of study, or rejecting it.
Nowadays, a lot of the work has moved on-line, specifically, most of the work that doesn’t require human contact – registration, discipline assignation, paperwork validation, etc. The one thing that is still off-line is the student assessment and guidance. This is because, as mentioned before, it requires human contact.


In the face of the COVID-19 pandemic, all across the world, business, people, and in our case, universities, struggle to move on-line. Some of them had already taken the step long before the pandemic and some, are doing it now. The main reason for different entities sticking to the off-line solutions, is because human contact is invaluable, it is required for properly establishing trust. But because of our current state of happenings, we have been forced to move on-line even the last parts of the processes that were still happening off-line.


In our case, this refers to the student assessment and guidance. Moving it on-line comes with multiple requirements. The most important of which are the on-line video-conference support and the process pipeline tracking.
