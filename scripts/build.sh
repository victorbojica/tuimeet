#!/usr/bin/env bash
set -e

npm set audit false

npm install --silent --no-warnings --no-progress --no-fund

cd packages/api
npm install --silent --no-warnings --no-progress --no-fund
npm run build
cd ../../

cd packages/admin-client
npm install --silent --no-warnings --no-progress --no-fund
npm run build
cd ../../

cd packages/student-client/builds
npm install --silent --no-warnings --no-progress --no-fund
npm run build
cd ../../
