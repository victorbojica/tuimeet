#!/usr/bin/env bash
set -e

if test -z "$SSH_KEY"
then
  echo "[0] Deploying from local"
else
  echo "[0] Deploying from CI/CD"

  mkdir -p ~/.ssh
  touch ~/.ssh/config
  echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config
  chmod 600 ~/.ssh/config
  # Lets write the public key of our aws instance
  eval $(ssh-agent -s)
  echo -e "$SSH_KEY" > ~/.ssh/tuimeet
  chmod 600 ~/.ssh/tuimeet
fi

echo "[1] Copying files to server"
cd packages/api/builds
scp -q -o StrictHostKeyChecking=no -i ~/.ssh/tuimeet -P 63333 -r "$(ls -t * | head -1)" root@81.180.222.47:/root/provision/builds
cd ../../../

cd packages/admin-client/builds
scp -q -o StrictHostKeyChecking=no -i ~/.ssh/tuimeet -P 63333 -r "$(ls -t * | head -1)" root@81.180.222.47:/root/provision/builds
cd ../../../

cd packages/student-client/builds
scp -q -o StrictHostKeyChecking=no -i ~/.ssh/tuimeet -P 63333 -r "$(ls -t * | head -1)" root@81.180.222.47:/root/provision/builds
cd ../../../

scp -q -o StrictHostKeyChecking=no -i ~/.ssh/tuimeet -P 63333 -r provision/provision.sh root@81.180.222.47:/root/provision
scp -q -o StrictHostKeyChecking=no -i ~/.ssh/tuimeet -P 63333 -r provision/deploy.setup.sh root@81.180.222.47:/root/provision

scp -q -o StrictHostKeyChecking=no -i ~/.ssh/tuimeet -P 63333 -r provision/nginx.conf root@81.180.222.47:/root/provision
scp -q -o StrictHostKeyChecking=no -i ~/.ssh/tuimeet -P 63333 -r provision/api.tuimeet.wesync.io.conf root@81.180.222.47:/root/provision
scp -q -o StrictHostKeyChecking=no -i ~/.ssh/tuimeet -P 63333 -r provision/admin.tuimeet.wesync.io.conf root@81.180.222.47:/root/provision
scp -q -o StrictHostKeyChecking=no -i ~/.ssh/tuimeet -P 63333 -r provision/student.tuimeet.wesync.io.conf root@81.180.222.47:/root/provision

ssh -q -o StrictHostKeyChecking=no -i ~/.ssh/tuimeet -p 63333 root@81.180.222.47 "cd /root/provision && ./deploy.setup.sh"
