require("dotenv").config();

const debug_port = 9229;

let api_config = {
  name: "api",
  script: "./lib/bin/api.js",
  watch: false,
};

if (process.env.NODE_ENV === "development") {
  api_config.node_args = [`--inspect=0.0.0.0:${debug_port}`];
}

if (process.env.NODE_ENV === "production") {
  api_config.exec_mode = "cluster";
  api_config.instances = "max";
}

module.exports = {
  apps: [api_config],
};
