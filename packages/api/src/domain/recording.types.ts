import { Id, Nullable } from "./base.types";

export type RecordingDTO = {
  id: Id;
  path: string;

  organizationId: Id;

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Nullable<Date>;
};
