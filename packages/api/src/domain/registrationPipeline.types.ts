import { Registration } from "./Registration";
import { Waitinglist } from "./Waitinglist";
import { Meeting } from "./Meeting";
import { Recording } from "./Recording";
import { User } from "./User";

export type MeetingSchedule = {
  estimatedStartTime: Date;
  estimatedDuration: number;
};
export enum RegistrationPipelineStatus {
  complete = "complete",
  canceled = "canceled",
  pending = "pending",
  waitinglist = "waitinglist",
  meeting = "meeting",
  happening = "happening",
  unknown = "unknown",
}

export type RegistrationPipelineDTO = {
  registration: Registration;
  waitinglist?: Waitinglist;
  meeting?: Meeting;
  recording?: Recording;
  user?: User;
};
