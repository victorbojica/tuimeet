import { RegistrationDTO, RegistrationStatus } from "./registrations.types";
import { Id, Nullable } from "./base.types";
import { Base } from "./Base";
import { v4 as uuid } from "uuid";

export type RegistrationCreateDTO = {
  refId: string;
  firstName: string;
  lastName: string;
  email: string;
  organizationId: string;
};

export class Registration extends Base<RegistrationDTO> {
  id: string;
  refId: string;
  firstName: string;
  lastName: string;
  email: string;
  status: RegistrationStatus;
  startedAt: Nullable<Date>;
  endedAt: Nullable<Date>;
  organizationId: Id;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Nullable<Date>;
  cancellationCount: number;

  get isFinished(): boolean {
    return this.status === RegistrationStatus.complete || this.status === RegistrationStatus.canceled;
  }

  get fullName(): string {
    return `${this.firstName} ${this.lastName}`;
  }

  static create(data: RegistrationCreateDTO) {
    return new Registration({
      id: uuid(),
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      organizationId: data.organizationId,
      refId: data.refId,
      status: RegistrationStatus.pending,
      startedAt: null,
      endedAt: null,
      cancellationCount: 0,
      createdAt: new Date(),
      updatedAt: new Date(),
      deletedAt: null,
    });
  }
}
