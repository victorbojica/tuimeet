import { Id, Nullable } from "./base.types";

export enum RegistrationStatus {
  pending = "pending",
  waitinglist = "waitinglist",
  complete = "complete",
  canceled = "canceled",
}

export type RegistrationField = "";

export interface RegistrationDTO {
  id: Id;

  refId: string;
  firstName: string;
  lastName: string;
  email: string;
  status: RegistrationStatus;
  startedAt: Nullable<Date>;
  endedAt: Nullable<Date>;
  cancellationCount: number;

  organizationId: Id;

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Nullable<Date>;
}
