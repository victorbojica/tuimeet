export class ResourceNotFoundError extends Error {
  constructor(_message: string) {
    const message = `[NOT_FOUND][ERROR]: ${_message}`;
    super(message);
  }
}
