import { Base } from "./Base";
import { Id, Nullable } from "./base.types";
import { UserDTO, UserRole, UserScheduleItem } from "./user.types";

export class User extends Base<UserDTO> {
  id: Id;

  role: UserRole;
  refId: Nullable<string>;
  firstName: string;
  lastName: string;
  email: string;
  avatarURL: Nullable<string>;
  meetingDuration: Nullable<number>;
  schedule: Array<UserScheduleItem>;
  enabled: boolean;

  organizationId: Id;

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Nullable<Date>;

  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }
}
