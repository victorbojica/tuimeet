import { ErrorTypes } from "./error.types";

export class BusinessLogicError extends Error {
  type: ErrorTypes;

  constructor(_message: string, deadlock?: boolean) {
    const message = `${_message}`;
    super(message);
    this.type = ErrorTypes.error;
  }
}

export class BusinessLogicWarning extends Error {
  type: ErrorTypes;
  constructor(_message: string) {
    const message = `${_message}`;
    super(message);
    this.type = ErrorTypes.warning;
  }
}
