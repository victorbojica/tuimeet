import { Id, Nullable } from "./base.types";

export enum UserRole {
  administrator = "administrator",
  member = "member",
}

export type UserScheduleItem = {
  startTime: Date;
  endTime: Date;
};

export type UserDTO = {
  id: Id;

  role: UserRole;
  refId: Nullable<string>;
  firstName: string;
  lastName: string;
  email: string;
  avatarURL: Nullable<string>;
  schedule: Array<UserScheduleItem>;
  meetingDuration: Nullable<number>;
  enabled: boolean;

  organizationId: Id;

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Nullable<Date>;
};
