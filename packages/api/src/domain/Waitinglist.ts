import { Id, Nullable } from "./base.types";
import { Base } from "./Base";
import { WaitinglistDTO, WaitinglistStatus } from "./waitinglist.types";

export class Waitinglist extends Base<WaitinglistDTO> {
  id: Id;
  status: WaitinglistStatus;
  joinedAt: Date;
  scheduledAt: Nullable<Date>;
  userId: Id;
  registrationId: Id;
  organizationId: Id;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Nullable<Date>;
}
