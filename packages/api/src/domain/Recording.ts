import { v4 as uuid } from "uuid";
import { Base } from "./Base";
import { RecordingDTO } from "./recording.types";
import { Id, Nullable } from "./base.types";

export class RecordingCreateDTO {
  id?: string;
  path: string;
  organizationId: string;
}

export class Recording extends Base<RecordingDTO> {
  id: Id;
  path: string;

  organizationId: Id;

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Nullable<Date>;

  static create(data: RecordingCreateDTO) {
    return new Recording({
      id: data.id ? data.id : uuid(),
      path: data.path,
      organizationId: data.organizationId,
      createdAt: new Date(),
      updatedAt: new Date(),
      deletedAt: null,
    });
  }
}
