export type ReferenceRegistration = {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  rest: any;
};
