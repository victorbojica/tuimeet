import { Id, Nullable } from "./base.types";

export enum WaitinglistStatus {
  pending = "pending",
  meeting = "meeting",
  rescheduled = "rescheduled",
  complete = "complete",
  canceled = "canceled",
}

export type WaitinglistDTO = {
  id: Id;

  status: WaitinglistStatus;
  joinedAt: Date;
  scheduledAt: Nullable<Date>;

  userId: Id;
  registrationId: Id;
  organizationId: Id;

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Nullable<Date>;
};
