import { Id, Nullable } from "./base.types";

export type Organization = {
  id: Id;

  name: string;
  logoURL: string;
  meetingDuration: number;
  meetingMaxReschedules: number;
  refId: string;

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Nullable<Date>;
};
