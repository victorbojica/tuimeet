import { Id, Nullable } from "./base.types";
import { Base } from "./Base";
import { MeetingDTO, MeetingStatus } from "./meeting.types";
import { Waitinglist } from "./Waitinglist";
import { Registration } from "./Registration";
import { Recording } from "./Recording";
import * as DateUtils from "date-fns";

export class Meeting extends Base<MeetingDTO> {
  id: Id;

  status: MeetingStatus;
  notes: Nullable<string>;
  endReason: Nullable<string>;
  estimatedStartTime: Date;
  estimatedDuration: Number;
  userURL?: string;
  registrationURL?: string;
  startedAt: Nullable<Date>;
  endedAt: Nullable<Date>;
  userJoinedAt: Nullable<Date>;
  registrationJoinedAt: Nullable<Date>;

  userId: Id;
  registrationId: Id;
  waitinglistId: Id;
  organizationId: Id;
  recordingId: Nullable<Id>;

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Nullable<Date>;
  registration?: Registration;
  waitinglist?: Waitinglist;
  recording?: Recording;

  get isAboutToHappen() {
    if (this.status !== MeetingStatus.pending) return false;

    const now = new Date();
    const estimatedStartTimeThreshold = DateUtils.sub(this.estimatedStartTime, { minutes: 10 });

    return now > estimatedStartTimeThreshold && now < this.estimatedStartTime;
  }
}
