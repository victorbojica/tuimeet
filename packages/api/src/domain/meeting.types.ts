import { Id, Nullable } from "./base.types";
import { Registration } from "./Registration";
import { Waitinglist } from "./Waitinglist";
import { Recording } from "./Recording";

export enum MeetingStatus {
  pending = "pending",
  happening = "happening",
  complete = "complete",
  canceled = "canceled",
  rescheduled = "rescheduled",
}

export type MeetingDTO = {
  id: Id;

  status: MeetingStatus;
  notes: Nullable<string>;
  endReason: Nullable<string>;
  estimatedStartTime: Date;
  estimatedDuration: Number;
  userURL?: string;
  registrationURL?: string;
  startedAt: Nullable<Date>;
  endedAt: Nullable<Date>;
  userJoinedAt: Nullable<Date>;
  registrationJoinedAt: Nullable<Date>;

  userId: Id;
  registrationId: Id;
  waitinglistId: Id;
  organizationId: Id;
  recordingId: Nullable<Id>;

  registration?: Registration;
  waitinglist?: Waitinglist;
  recording?: Recording;

  createdAt: Date;
  updatedAt: Date;
  deletedAt: Nullable<Date>;
};
