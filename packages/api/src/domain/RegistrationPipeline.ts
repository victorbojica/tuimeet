import { v4 as uuid } from "uuid";

import { Base } from "./Base";
import { Registration } from "./Registration";
import { Waitinglist } from "./Waitinglist";
import { Meeting } from "./Meeting";
import { User } from "./User";
import { MeetingSchedule, RegistrationPipelineDTO, RegistrationPipelineStatus } from "./registrationPipeline.types";
import { Recording } from "./Recording";
import { BusinessLogicError } from "./BussinessLogicError";
import { WaitinglistStatus } from "./waitinglist.types";
import { RegistrationStatus } from "./registrations.types";
import { MeetingStatus } from "./meeting.types";
import config from "../lib/config";

export class RegistrationPipeline extends Base<RegistrationPipelineDTO> {
  registrationId: string;
  waitinglistId?: string;
  meetingId?: string;
  recordingId?: string;
  userId?: string;

  private _registration: Registration;
  private _waitinglist?: Waitinglist;
  private _meeting?: Meeting;
  private _recording?: Recording;
  private _user?: User;

  get isFinished() {
    return (
      this.registration.status === RegistrationStatus.complete ||
      this.registration.status === RegistrationStatus.canceled
    );
  }

  get status(): RegistrationPipelineStatus {
    let status;
    if (this.registration.status === RegistrationStatus.complete) status = RegistrationPipelineStatus.complete;
    else if (this.registration.status === RegistrationStatus.canceled) status = RegistrationPipelineStatus.canceled;
    else if (this.registration.status === RegistrationStatus.pending) status = RegistrationPipelineStatus.pending;
    else {
      // status of registration should be waitinglist
      // handle not defined cases
      if (!this.waitinglist) return RegistrationPipelineStatus.unknown;
      if (this.waitinglist.status === WaitinglistStatus.canceled) return RegistrationPipelineStatus.unknown;
      else if (this.waitinglist.status === WaitinglistStatus.complete) return RegistrationPipelineStatus.unknown;
      else if (this.waitinglist.status === WaitinglistStatus.pending) status = RegistrationPipelineStatus.waitinglist;
      else {
        // status of waitinglist should be meeting
        if (!this.meeting) return RegistrationPipelineStatus.unknown;
        if (this.meeting.status === MeetingStatus.canceled) return RegistrationPipelineStatus.unknown;
        else if (this.meeting.status === MeetingStatus.complete) status = RegistrationPipelineStatus.complete;
        else if (this.meeting.status === MeetingStatus.pending) status = RegistrationPipelineStatus.meeting;
        else status = RegistrationPipelineStatus.happening; // status of meeting should be happening
      }
    }

    return status;
  }

  set registration(registration: Registration) {
    this._registration = registration;
    this.registrationId = registration?.id;
  }

  get registration() {
    return this._registration;
  }

  set waitinglist(waitinglist: Waitinglist) {
    this._waitinglist = waitinglist;
    this.waitinglistId = waitinglist?.id;
  }

  get waitinglist() {
    return this._waitinglist;
  }

  set meeting(meeting: Meeting) {
    this._meeting = meeting;
    this.meetingId = meeting?.id;
  }

  get meeting() {
    return this._meeting;
  }

  set user(user: User) {
    this._user = user;
    this.userId = user?.id;
  }

  get user() {
    return this._user;
  }

  set recording(recording: Recording) {
    this._recording = recording;
    this.recordingId = recording?.id;
  }

  get recording() {
    return this._recording;
  }

  joinWaitinglist(rest: { user: User }): RegistrationPipeline {
    if (this.isFinished) throw new BusinessLogicError("Registration already finished");
    if (this.waitinglist) throw new BusinessLogicError("Already joined waitinglist");
    if (this.registration.status !== RegistrationStatus.pending)
      throw new BusinessLogicError("Registration status must be pending");

    this.registration.status = RegistrationStatus.waitinglist;
    this.registration.startedAt = new Date();

    this.waitinglist = new Waitinglist({
      id: uuid(),
      status: WaitinglistStatus.pending,
      joinedAt: new Date(),
      scheduledAt: null,
      userId: rest.user.id,
      registrationId: this.registration.id,
      organizationId: this.registration.organizationId,
      createdAt: new Date(),
      updatedAt: new Date(),
      deletedAt: null,
    });

    this.user = rest.user;

    return this;
  }

  cancelWaitinglist() {
    if (this.isFinished) throw new BusinessLogicError("Registration already finished");
    if (!this.waitinglist) throw new BusinessLogicError("Not joined waitinglist");
    if (this.waitinglist.status !== WaitinglistStatus.pending)
      throw new BusinessLogicError("Can only cancel a waitinglist if status is pending");
    if (this.registration.cancellationCount >= config.app.maxWaitinglistCancellations)
      throw new BusinessLogicError("Maximum number of cancellations reached");

    this.waitinglist.status = WaitinglistStatus.canceled;
    this.waitinglist.deletedAt = new Date();

    this.registration.status = RegistrationStatus.pending;
    this.registration.startedAt = null;
    this.registration.cancellationCount += 1;

    return this;
  }

  scheduleMeeting(rest: { schedule: MeetingSchedule }): RegistrationPipeline {
    if (this.isFinished) throw new BusinessLogicError("Registration already finished");
    if (!this.waitinglist) throw new BusinessLogicError("Not joined waitinglist");
    if (this.meeting) throw new BusinessLogicError("Meeting already scheduled");
    if (rest.schedule.estimatedStartTime < new Date())
      throw new BusinessLogicError("Meeting has to start after current time");

    this.meeting = new Meeting({
      id: uuid(),
      status: MeetingStatus.pending,
      estimatedDuration: rest.schedule.estimatedDuration,
      estimatedStartTime: rest.schedule.estimatedStartTime,
      registrationURL: null,
      userURL: null,
      notes: null,
      endReason: null,
      startedAt: null,
      endedAt: null,
      userJoinedAt: null,
      registrationJoinedAt: null,
      registrationId: this.registration.id,
      waitinglistId: this.waitinglist.id,
      organizationId: this.registration.organizationId,
      userId: this.waitinglist.userId,
      recordingId: null,
      createdAt: new Date(),
      updatedAt: new Date(),
      deletedAt: null,
    });

    this.waitinglist.scheduledAt = new Date();
    this.waitinglist.status = WaitinglistStatus.meeting;

    return this;
  }

  rescheduleMeeting(rest: { schedule: MeetingSchedule; reason: string }): RegistrationPipeline {
    if (this.isFinished) throw new BusinessLogicError("Registration already finished");
    if (!this.waitinglist) throw new BusinessLogicError("Not joined waitinglist");
    if (!this.meeting) throw new BusinessLogicError("Meeting was not scheduled beforehand");
    if (this.meeting.status === MeetingStatus.complete || this.meeting.status === MeetingStatus.canceled)
      throw new BusinessLogicError("Meeting already finished");
    if (rest.schedule.estimatedStartTime < new Date())
      throw new BusinessLogicError("Meeting has to start after current time");

    this.meeting.estimatedStartTime = rest.schedule.estimatedStartTime;
    this.meeting.estimatedDuration = rest.schedule.estimatedDuration;
    this.meeting.endReason = rest.reason;
    this.meeting.status = MeetingStatus.pending;
    this.meeting.startedAt = null;
    this.meeting.endedAt = null;
    this.meeting.userJoinedAt = null;
    this.meeting.registrationJoinedAt = null;
    this.meeting.userURL = null;
    this.meeting.registrationURL = null;

    return this;
  }

  cancelMeeting(rest: { reason: string }): RegistrationPipeline {
    if (this.isFinished) throw new BusinessLogicError("Registration already finished");
    if (!this.waitinglist) throw new BusinessLogicError("Not joined waitinglist");
    if (!this.meeting) throw new BusinessLogicError("Meeting was not scheduled beforehand");
    if (this.meeting.status !== MeetingStatus.pending)
      throw new BusinessLogicError("Meeting can be canceled only if it's pending");

    this.meeting.status = MeetingStatus.canceled;
    this.meeting.endReason = rest.reason;
    this.meeting.deletedAt = new Date();

    this.waitinglist.status = WaitinglistStatus.pending;
    this.waitinglist.scheduledAt = null;

    return this;
  }

  startMeeting(): RegistrationPipeline {
    if (this.isFinished) throw new BusinessLogicError("Registration already finished");
    if (!this.waitinglist) throw new BusinessLogicError("Not joined waitinglist");
    if (!this.meeting) throw new BusinessLogicError("Meeting was not scheduled beforehand");
    if (
      this.meeting.status === MeetingStatus.complete ||
      this.meeting.status === MeetingStatus.canceled ||
      this.meeting.status === MeetingStatus.rescheduled
    )
      throw new BusinessLogicError("Meeting can be started only if it's pending");

    if (this.meeting.status === MeetingStatus.happening) return this;

    this.meeting.status = MeetingStatus.happening;
    this.meeting.startedAt = new Date();

    return this;
  }

  registrationJoinMeeting(rest: { registrationURL: string }): RegistrationPipeline {
    if (this.isFinished) throw new BusinessLogicError("Registration already finished");
    if (!this.waitinglist) throw new BusinessLogicError("Not joined waitinglist");
    if (!this.meeting) throw new BusinessLogicError("Meeting was not scheduled beforehand");
    if (this.meeting.status !== MeetingStatus.happening)
      throw new BusinessLogicError("Meeting can be joined only if it's happening");

    if (!this.meeting.registrationJoinedAt) this.meeting.registrationJoinedAt = new Date();
    this.meeting.registrationURL = rest.registrationURL;

    return this;
  }

  userJoinMeeting(rest: { userURL: string }): RegistrationPipeline {
    if (this.isFinished) throw new BusinessLogicError("Registration already finished");
    if (!this.waitinglist) throw new BusinessLogicError("Not joined waitinglist");
    if (!this.meeting) throw new BusinessLogicError("Meeting was not scheduled beforehand");
    if (this.meeting.status !== MeetingStatus.happening)
      throw new BusinessLogicError("Meeting can be joined only if it's happening");

    if (!this.meeting.userJoinedAt) this.meeting.userJoinedAt = new Date();
    this.meeting.userURL = rest.userURL;

    return this;
  }

  completeMeeting(rest: { notes: string; recordingPath?: string }): RegistrationPipeline {
    if (this.isFinished) throw new BusinessLogicError("Registration already finished");
    if (!this.waitinglist) throw new BusinessLogicError("Not joined waitinglist");
    if (!this.meeting) throw new BusinessLogicError("Meeting was not scheduled beforehand");
    if (this.meeting.status !== MeetingStatus.happening)
      throw new BusinessLogicError("Meeting can be completed only if it's happening"); //TODO check to see if this isn't to restrictive

    if (rest.recordingPath) {
      this.recording = Recording.create({ path: rest.recordingPath, organizationId: this.registration.organizationId });
      this.meeting.recordingId = this.recording.id;
    }

    this.meeting.status = MeetingStatus.complete;
    this.meeting.notes = rest.notes;
    this.meeting.endedAt = new Date();

    return this;
  }

  cancelRegistrationPipeline(rest: { reason: string }): RegistrationPipeline {
    if (this.isFinished) throw new BusinessLogicError("Registration already finished");

    this.registration.status = RegistrationStatus.canceled;
    this.registration.deletedAt = new Date();

    if (this.waitinglist) {
      this.waitinglist.status = WaitinglistStatus.canceled;
      this.waitinglist.deletedAt = new Date();
    }

    if (this.meeting) {
      this.meeting.status = MeetingStatus.canceled;
      this.meeting.deletedAt = new Date();
    }

    return this;
  }

  completeRegistrationPipeline(): RegistrationPipeline {
    if (this.isFinished) throw new BusinessLogicError("Registration already finished");
    if (!this.waitinglist) throw new BusinessLogicError("Not joined waitinglist");
    if (!this.meeting) throw new BusinessLogicError("Meeting was not scheduled beforehand");
    if (this.meeting.status !== MeetingStatus.complete) throw new BusinessLogicError("Meeting must be complete");
    this.waitinglist.status = WaitinglistStatus.complete;
    this.registration.status = RegistrationStatus.complete;

    return this;
  }

  toJSON() {
    return {
      status: this.status,
      registration: this.registration,
      waitinglist: this.waitinglist,
      meeting: this.meeting,
      recording: this.recording,
      user: this.user,
    };
  }
}
