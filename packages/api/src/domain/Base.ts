export class Base<T> {
  constructor(props: T) {
    Object.assign(this, props);
  }
}
