import "reflect-metadata";

import { GlobalAcceptMimesMiddleware, ServerLoader, ServerSettings, $log } from "@tsed/common";
import { CronJob } from "cron";
import bodyParser from "body-parser";
import cors from "cors";
import helmet from "helmet";

import config from "../config";

import "./providers";

export const rootDir = `${__dirname}/../../`;

@ServerSettings({
  rootDir,
  port: config.env.port,
  env: config.env.stage,
  logger: config.log,
  uploadDir: `${rootDir}/../.tmp/uploads`,
  passport: {},
  multer: {},
  componentsScan: [
    `${rootDir}/lib/bootstrap/protocols/*{.ts,.js}`, // scan protocols directory
    `${rootDir}/lib/bootstrap/middleware/*{.ts,.js}`, // scan middlwware directory
  ],
  mount: {
    "/api/v1": "${rootDir}/controllers/**/*.ts",
  },
  acceptMimes: ["application/json"],
})
export class APIServer extends ServerLoader {
  public static async start() {
    try {
      const server = await ServerLoader.bootstrap(APIServer);
      await server.listen();
    } catch (err) {
      $log.error(err);
    }
  }

  public static $cronTicks = 1;

  public $beforeRoutesInit(): void | Promise<any> {
    this.use(GlobalAcceptMimesMiddleware)
      .use(bodyParser.urlencoded({ extended: true }))
      .use(bodyParser.json())
      .use(cors())
      .use(helmet());
  }

  public async $afterInit(): Promise<any> {
    await this.injector.emit("$onCronReady"); // create a custom hook (event) for your services

    const cron = new CronJob(config.app.cronTime, async () => {
      $log.info("APP :: CRON :: START");
      await this.injector.emit("$onCronTick", APIServer.$cronTicks); // create a custom hook (event) for your services
      APIServer.$cronTicks += 1;
      $log.info("APP :: CRON :: END");
    });
    cron.start();
  }

  public $onReady(): void | Promise<any> {
    $log.debug(`APP :: STARTED :: Listening at http://${config.app.endpoint}`);
  }
}
