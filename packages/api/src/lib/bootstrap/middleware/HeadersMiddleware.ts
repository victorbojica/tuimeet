import { Middleware, Next, Req, Res } from "@tsed/common";

@Middleware()
export class HeadersMiddleware {
  use(@Req() request: Req, @Res() response: Res, @Next() next: Next): any {
    // DO SOMETHING
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", `Origin, X-Requested-With, Content-Type, Accept, Authorization`);
    next();
  }
}
