import { Err, GlobalErrorHandlerMiddleware, $log, OverrideProvider, Req, Res } from "@tsed/common";

import { BusinessLogicError, BusinessLogicWarning, ResourceNotFoundError } from "../../../domain/";

@OverrideProvider(GlobalErrorHandlerMiddleware)
export class CustomGlobalErrorHandlerMiddleware extends GlobalErrorHandlerMiddleware {
  use(@Err() error: any, @Req() request: Req, @Res() response: Res): any {
    if (
      error instanceof BusinessLogicError ||
      error instanceof BusinessLogicWarning ||
      error instanceof ResourceNotFoundError
    ) {
      $log.error(error);

      response.header("Content-Type", "application/json");
      response.status(400).send({ error: { message: error.message } });
      return;
    } else {
      // could be SQLError or anything else
      $log.error(error);

      response.header("Content-Type", "application/json");
      response.status(400).send({ error: { message: "Unknown error" } });
      return;
    }
  }
}
