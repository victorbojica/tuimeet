import { Context, Req } from "@tsed/common";
import { Arg, OnVerify, Protocol } from "@tsed/passport";
import { ExtractJwt, Strategy, StrategyOptions } from "passport-jwt";

import config from "../../config";

import { JWTPayload } from "./protocol.types";
import { AuthorizationService } from "../../../services/AuthorizationService";

@Protocol<StrategyOptions>({
  name: "registration",
  useStrategy: Strategy,
  settings: {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.app.secret,
    issuer: config.app.endpoint,
    audience: "*",
  },
})
export class RegistrationProtocol implements OnVerify {
  constructor(private authorizationService: AuthorizationService) {}

  async $onVerify(@Req() req: Req, @Context() context: Context, @Arg(0) jwtPayload: JWTPayload) {
    const isAuthorized = await this.authorizationService.authorizeRegistration(jwtPayload);
    if (isAuthorized) context.set("registration", { id: jwtPayload.sub, organizationId: jwtPayload.organizationId });
    return isAuthorized;
  }
}
