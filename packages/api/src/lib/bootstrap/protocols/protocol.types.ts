import { UserRole } from "../../../domain/user.types";
import { Id } from "../../../domain/base.types";

export type JWTPayload = {
  sub: string;
  role: UserRole;
  organizationId: Id;
};
