import { Context, Req } from "@tsed/common";
import { Arg, OnVerify, Protocol } from "@tsed/passport";
import { ExtractJwt, Strategy, StrategyOptions } from "passport-jwt";

import config from "../../config";

import { JWTPayload } from "./protocol.types";
import { AuthorizationService } from "../../../services/AuthorizationService";

@Protocol<StrategyOptions>({
  name: "admin",
  useStrategy: Strategy,
  settings: {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.app.secret,
    issuer: config.app.endpoint,
    audience: "*",
  },
})
export class AdminProtocol implements OnVerify {
  constructor(private authorizationService: AuthorizationService) {}

  async $onVerify(@Req() req: Req, @Context() context: Context, @Arg(0) jwtPayload: JWTPayload) {
    let isAutorized = await this.authorizationService.authorizeAdmin(jwtPayload);

    if (isAutorized)
      context.set("user", { id: jwtPayload.sub, role: jwtPayload.role, organizationId: jwtPayload.organizationId });

    return isAutorized;
  }
}
