import { Context, Req } from "@tsed/common";
import { Arg, OnVerify, Protocol } from "@tsed/passport";
import { ExtractJwt, Strategy, StrategyOptions } from "passport-jwt";

import config from "../../config";

import { JWTPayload } from "./protocol.types";
import { AuthorizationService } from "../../../services";

@Protocol<StrategyOptions>({
  name: "member",
  useStrategy: Strategy,
  settings: {
    jwtFromRequest: ExtractJwt.fromExtractors([
      ExtractJwt.fromAuthHeaderAsBearerToken(),
      ExtractJwt.fromUrlQueryParameter("tk"),
    ]),
    secretOrKey: config.app.secret,
    issuer: config.app.endpoint,
    audience: "*",
  },
})
export class MemberProtocol implements OnVerify {
  constructor(private authorizationService: AuthorizationService) {}

  async $onVerify(@Req() req: Req, @Context() context: Context, @Arg(0) jwtPayload: JWTPayload) {
    const isAuthorized = await this.authorizationService.authorizeMember(jwtPayload);
    if (isAuthorized)
      context.set("user", { id: jwtPayload.sub, role: jwtPayload.role, organizationId: jwtPayload.organizationId });
    return isAuthorized;
  }
}
