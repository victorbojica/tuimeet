import { registerProvider } from "@tsed/di";
import SendgridMail, { MailService } from "@sendgrid/mail";

import config from "../../config";

export const Mailer = Symbol.for("Mailer");

registerProvider({
  provide: Mailer,
  async useAsyncFactory(): Promise<MailService> {
    SendgridMail.setApiKey(config.mailer.apiKey);
    return SendgridMail;
  },
});
