import { registerProvider } from "@tsed/di";
import Knex from "knex";

import config from "../../config";
import { $log } from "@tsed/common";

export const InternalDatabase = Symbol.for("InternalDatabase");

registerProvider({
  provide: InternalDatabase,
  async useAsyncFactory(): Promise<Knex> {
    const connection = Knex({
      client: "mysql",
      connection: {
        user: config.db.user,
        password: config.db.pass,
        host: config.db.host,
        port: config.db.port,
        database: config.db.name,
      },
    });

    connection.on("query", (data) => {
      $log.debug("DB :: QUERY", data);
    });

    try {
      await connection.raw("SELECT NOW()");
      $log.debug("APP :: DB :: Connected");
    } catch (err) {
      $log.error(err);
      throw new Error("Couldn't establish database connection");
    }

    return connection;
  },
});
