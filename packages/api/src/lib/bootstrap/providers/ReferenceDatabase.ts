import { registerProvider } from "@tsed/di";
import Knex from "knex";

import config from "../../config";
import { $log } from "@tsed/common";

export const ReferenceDatabase = Symbol.for("ReferenceDatabase");

registerProvider({
  provide: ReferenceDatabase,
  async useAsyncFactory(): Promise<Knex> {
    const connection = Knex({
      client: "mysql",
      connection: {
        user: config.refDb.user,
        password: config.refDb.pass,
        host: config.refDb.host,
        port: config.refDb.port,
        database: config.refDb.name,
      },
    });

    connection.on("query", (data) => {
      $log.debug("REF_DB :: QUERY", data);
    });

    try {
      await connection.raw("SELECT NOW()");
      $log.debug("APP :: REF_DB :: Connected");
    } catch (err) {
      $log.error(err);
      throw new Error("Couldn't establish database connection");
    }

    return connection;
  },
});
