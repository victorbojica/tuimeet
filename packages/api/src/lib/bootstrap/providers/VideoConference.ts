import { registerProvider } from "@tsed/di";
import axios from "axios";
import * as Querystring from "querystring";
import * as crypto from "crypto";
import _ from "lodash";
import { $log } from "@tsed/common";
import xml2js from "xml2js";

import config from "../../config";
import { IVideoConferenceConfig } from "../../config/config.types";
import { Nullable } from "../../../domain";

export const VideoConference = Symbol.for("VideoConference");

export interface IVideoConference {
  create(id: string, options: { startTime: Date }): Promise<VideoConferenceCreateDTO>;
  get(id: string): Promise<VideoConferenceGetDTO>;
  getURL(id: string, options: { fullName: string; password: string }): Promise<string>;
  end(id: string, options: { password: string }): Promise<void>;
  list(): Promise<Array<any>>;
}

export class VideoConferenceError extends Error {
  data: any;
  constructor(message, data?: any) {
    super(message);
    this.data = data;
  }
}

export type VideoConferenceCreateDTO = {
  meetingID: string;
  internalID: string;
  attendeePW: string;
  moderatorPW: string;
};

export type VideoConferenceGetDTO = {
  meetingID: string;
  internalID: string;
  meetingName: string;
  createDate: Date;
  attendeePW: string;
  moderatorPW: string;
  running: boolean;
  duration: number;
  hasUserJoined: boolean;
  recording: boolean;
  hasBeenForciblyEnded: boolean;
  startTime: number;
  endTime: number;
  participantCount: number;
  listenerCount: number;
  moderatorCount: number;
};

class VideoConferenceAdapter implements IVideoConference {
  protected connection;

  static requestLogger(request) {
    const { url, method, data, headers, params } = request;
    $log.debug(`VIDEO_CONFERENCE :: REQUEST`, { url, method, params, data, headers });
    return request;
  }

  static responseLogger(response) {
    const {
      config: { url, method },
      status,
      statusText,
      data,
      headers,
    } = response;
    $log.debug(`VIDEO_CONFERENCE :: RESPONSE`, { url, method, status, statusText, data, headers });
    return response;
  }

  static handleRequestResponseError(error) {
    if (error instanceof VideoConferenceError) return Promise.reject(error);
    if (error instanceof Error) return Promise.reject(error);
    return Promise.reject(new Error("Unknown error"));
  }

  static buildQuery(url, params) {
    const query = Querystring.encode(params);

    const checksumString = `${_.trim(url, "/")}${query}${config.videoConference.secret}`;
    const checksum = crypto.createHash("sha1").update(checksumString).digest("hex");

    return `${query}&checksum=${checksum}`;
  }

  static handleRequestAuthorization(request) {
    request._params = request.params;
    request.params = null;

    const query = VideoConferenceAdapter.buildQuery(request.url, request._params);

    request.url = `${request.url}?${query}`;

    return request;
  }

  static checkResponseDataExistence(response) {
    if (!response.data) return Promise.reject(new Error("Unknown video conference error"));
    return response;
  }

  static checkResponseResult(response) {
    if (response.data.returncode === "FAILED")
      return Promise.reject(new VideoConferenceError(response.data.message, response.data));

    return response.data;
  }

  static async normalizeResponse(response) {
    if (typeof response.data === "string") {
      try {
        response.data = await xml2js.parseStringPromise(response.data);
        _.each(response.data.response, (value, key) => {
          if (key === "responsecode") return;
          response.data[key] = value[0]; // xml babeeeh!
        });
      } catch (e) {
        $log.error(e);
      }
    } else if (response.data.response) {
      response.data = response.data.response;
    }

    return response;
  }

  constructor(config: IVideoConferenceConfig) {
    this.connection = axios.create();
    this.connection.defaults.baseURL = config.host;

    this.connection.interceptors.request.use(
      VideoConferenceAdapter.handleRequestAuthorization,
      VideoConferenceAdapter.handleRequestResponseError
    );

    this.connection.interceptors.request.use(VideoConferenceAdapter.requestLogger);
    this.connection.interceptors.response.use(VideoConferenceAdapter.responseLogger);

    this.connection.interceptors.response.use(
      VideoConferenceAdapter.checkResponseDataExistence,
      VideoConferenceAdapter.handleRequestResponseError
    );

    this.connection.interceptors.response.use(
      VideoConferenceAdapter.normalizeResponse,
      VideoConferenceAdapter.handleRequestResponseError
    );

    this.connection.interceptors.response.use(
      VideoConferenceAdapter.checkResponseResult,
      VideoConferenceAdapter.handleRequestResponseError
    );
  }

  async create(id: string, options: { startTime: Date }): Promise<VideoConferenceCreateDTO> {
    return (await this.connection.get("/create", {
      params: {
        meetingID: id,
        maxParticipants: 4,
        record: true,
        autoStartRecording: true,
        allowStartStopRecording: false,
        muteOnStart: true,
        logoutURL: "#",
      },
    })) as VideoConferenceCreateDTO;
  }

  async get(id: string): Promise<Nullable<VideoConferenceGetDTO>> {
    try {
      return (await this.connection.get("/getMeetingInfo", {
        params: {
          meetingID: id,
        },
      })) as VideoConferenceGetDTO;
    } catch (e) {
      if (e.data.messageKey === "notFound") return null;

      throw e;
    }
  }

  async getURL(id: string, options: { fullName: string; password: string }): Promise<string> {
    const params = {
      meetingID: id,
      fullName: options.fullName,
      password: options.password,
      joinViaHtml5: true,
      redirect: true,
    };
    const url = `${_.trim(config.videoConference.host, "/")}/join`;
    const query = VideoConferenceAdapter.buildQuery("join", params);

    return `${url}?${query}`;
  }

  async end(id: string, options: { password: string }): Promise<void> {
    await this.connection.get("/end", {
      params: {
        meetingID: id,
        password: options.password,
      },
    });
    return;
  }

  async list(): Promise<Array<any>> {
    const meetings = await this.connection.get("/getMeetings");
    return meetings;
  }
}

registerProvider({
  provide: VideoConference,
  async useAsyncFactory(): Promise<IVideoConference> {
    const connection = new VideoConferenceAdapter(config.videoConference);
    return connection;
  },
});
