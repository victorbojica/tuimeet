import _ from "lodash";

const keysToOmit = ["id", "createdAt", "updatedAt"];

export const diff = (o1, o2) => {
  let diffKeys = [];
  const o1Keys = Object.keys(o1);
  o1Keys.forEach((key) => {
    if (!keysToOmit.includes(key) && o1[key] !== o2[key]) diffKeys.push(key);
    // if (typeof o1[key] === "object" && o1[key] !== null) return;
    // if (typeof o2[key] === "object" && o2[key] !== null) return;
  });

  return _.uniq(diffKeys);
};
