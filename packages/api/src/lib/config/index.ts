import {
  IConfig,
  IEnvConfig,
  IAppConfig,
  IDbConfig,
  Stage,
  ILogConfig,
  IMailerConfig,
  IVideoConferenceConfig,
} from "./config.types";
import * as dotenv from "dotenv";

function getStage(_stage: Stage): Stage {
  const stage = (_stage || process.env.NODE_ENV === "production" ? "production" : "development") as Stage;

  return stage;
}

function loadEnv(stage: Stage): any {
  let env = dotenv.config({ path: `./.env.${stage}` });
  if (env.error) throw new Error("Couldn't loadn .env file");
  return env.parsed;
}

function getEnvConfig(stage: Stage): IEnvConfig {
  dotenv.config({ path: `./.env.${stage}` });

  return {
    stage,
    host: process.env.API_HOST,
    port: parseInt(process.env.API_PORT, 10),
  };
}

function getLogConfig(stage: Stage): ILogConfig {
  const log = {
    level: stage === "production" ? "info" : "debug",
  };

  return log;
}

function getAppConfig(stage: Stage): IAppConfig {
  const app = {
    name: process.env.APP_NAME,
    endpoint: process.env.APP_ENDPOINT,
    secret: process.env.APP_SECRET,
    cronTime: process.env.APP_CRON_TIME,
    maxWaitinglistCancellations: 3,
  };

  return app;
}

function getDbConfig(stage: Stage): IDbConfig {
  const db = {
    name: process.env.DB_NAME,
    user: process.env.DB_USER,
    pass: process.env.DB_PASS,
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT, 10),
  };

  return db;
}

function getRefDbConfig(stage: Stage): IDbConfig {
  const db = {
    name: process.env.REF_DB_NAME,
    user: process.env.REF_DB_USER,
    pass: process.env.REF_DB_PASS,
    host: process.env.REF_DB_HOST,
    port: parseInt(process.env.REF_DB_PORT, 10),
  };

  return db;
}

function getMailerConfig(stage: Stage): IMailerConfig {
  const mailer = {
    apiKey: process.env.MAILER_API_KEY,
    from: { email: process.env.MAILER_FROM_EMAIL, name: process.env.MAILER_FROM_NAME },
  };
  return mailer;
}

function getVideoConferenceConfig(stage: Stage): IVideoConferenceConfig {
  const videoConference = {
    host: process.env.VIDEOCONFERENCE_HOST,
    secret: process.env.VIDEOCONFERENCE_SECRET,
  };
  return videoConference;
}

export function getConfig(_stage?: Stage): IConfig {
  const stage = getStage(_stage);
  loadEnv(stage);

  const env = getEnvConfig(stage);
  const log = getLogConfig(stage);
  const app = getAppConfig(stage);
  const mailer = getMailerConfig(stage);
  const db = getDbConfig(stage);
  const refDb = getRefDbConfig(stage);
  const videoConference = getVideoConferenceConfig(stage);

  return { env, app, log, mailer, db, refDb, videoConference };
}

const config = getConfig();

export default config;
