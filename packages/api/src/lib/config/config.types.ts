export type Stage = "production" | "development";

export interface ILogConfig {
  level: string;
}

export interface IAppConfig {
  name: string;
  secret: string;
  endpoint: string;
  cronTime: string;
  maxWaitinglistCancellations: number;
}

export interface IEnvConfig {
  stage: Stage;
  host: string;
  port: number;
}

export interface IDbConfig {
  host: string;
  port: number;
  user: string;
  pass: string;
  name: string;
}

export interface IMailerConfig {
  apiKey: string;
  from: { email: string; name: string };
}

export interface IVideoConferenceConfig {
  host: string;
  secret: string;
}

export interface IConfig {
  env: IEnvConfig;
  log: ILogConfig;
  app: IAppConfig;
  mailer: IMailerConfig;
  db: IDbConfig;
  refDb: IDbConfig;
  videoConference: IVideoConferenceConfig;
}
