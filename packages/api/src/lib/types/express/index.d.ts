export interface IRequestContext {
  [key: string]: any;
}

declare module "express" {
  export interface Request {
    context: IRequestContext;
  }
}
