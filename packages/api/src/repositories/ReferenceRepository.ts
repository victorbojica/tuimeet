import { IRepository } from "./repository.types";
import Knex from "knex";
import { Inject, Injectable, $log } from "@tsed/common";
import { ReferenceDatabase } from "../lib/bootstrap/providers";
import { SQLError } from "./SQLError";
import { Nullable } from "../domain";
import { ReferenceRegistration } from "../domain";

@Injectable()
export class ReferenceRepository implements IRepository<Knex> {
  constructor(@Inject(ReferenceDatabase) public connection: Knex) {}

  async transaction(ops) {
    let transaction = await this.connection.transaction();
    try {
      await ops(transaction);
    } catch (e) {
      await transaction.rollback();
      throw e as SQLError;
    }
    await transaction.commit();
  }

  async findAllRegistrationsByOrganizationRefId(refId: string): Promise<Array<ReferenceRegistration>> {
    let results = [];
    try {
      results = await this.connection
        .select("*")
        .from("listaOptiuniTUIASI")
        .rightJoin(
          "listaOptiuniCandidat",
          "listaOptiuniCandidat.FK_ID_listaoptiunituiasi",
          "=",
          "listaOptiuniTUIASI.PK_ID_optiune"
        )
        .leftJoin("datePersonale", "datePersonale.PK_ID_datepersonale", "=", "listaOptiuniCandidat.FK_ID_datepersonale")
        .where("listaOptiuniCandidat.indexOptiune", "=", 1)
        .where("listaOptiuniTUIASI.acronimFacultate", "=", refId);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return results.map((result) => ({
      id: `${result.FK_ID_datepersonale}|${result.FK_ID_listaoptiunituiasi}`,
      firstName: result.CI_prenume,
      lastName: result.CI_numeActual,
      email: result.email,
      rest: result,
    }));
  }

  async findRegistrationByRefId(refId: string): Promise<Nullable<ReferenceRegistration>> {
    if (!refId) return null;
    const [dpId, lotId] = refId.split("|");
    if (!dpId || !lotId) return null;

    let result;
    try {
      result = (
        await this.connection
          .select(["datePersonale.*"])
          .from("listaOptiuniTUIASI")
          .rightJoin(
            "listaOptiuniCandidat",
            "listaOptiuniCandidat.FK_ID_listaoptiunituiasi",
            "=",
            "listaOptiuniTUIASI.PK_ID_optiune"
          )
          .leftJoin(
            "datePersonale",
            "datePersonale.PK_ID_datepersonale",
            "=",
            "listaOptiuniCandidat.FK_ID_datepersonale"
          )
          .where("listaOptiuniCandidat.indexOptiune", "=", 1)
          .where("listaOptiuniCandidat.FK_ID_datepersonale", "=", dpId)
          .where("listaOptiuniCandidat.FK_ID_listaoptiunituiasi", "=", lotId)
      )[0];
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    if (!result) return null;

    return {
      id: `${result.FK_ID_datepersonale}|${result.FK_ID_listaoptiunituiasi}`,
      firstName: result.CI_prenume,
      lastName: result.CI_numeActual,
      email: result.email,
      rest: result,
    };
  }
}
