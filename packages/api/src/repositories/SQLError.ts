export class SQLError extends Error {
  constructor(_message: string) {
    const message = `[SQL][ERROR]: ${_message}`;
    super(message);
  }
}
