import Knex from "knex";
import { Inject, Injectable } from "@tsed/common";

import { SQLError } from "./SQLError";
import { IRepository } from "./repository.types";
import { InternalDatabase } from "../lib/bootstrap/providers";

@Injectable()
export class BaseRepository implements IRepository<Knex> {
  constructor(@Inject(InternalDatabase) public connection: Knex) {}

  async transaction(ops) {
    let transaction = await this.connection.transaction();
    try {
      await ops(transaction);
    } catch (e) {
      await transaction.rollback();
      throw e as SQLError;
    }
    await transaction.commit();
  }
}
