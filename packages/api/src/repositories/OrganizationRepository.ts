import { Organization } from "../domain";
import { QueryOptions } from "./repository.types";
import { $log } from "@tsed/common";
import { SQLError } from "./SQLError";
import { BaseRepository } from "./BaseRepository";

export class OrganizationRepository extends BaseRepository {
  static tableName = "organizations";

  public async findAll(queryOptions?: QueryOptions): Promise<Array<Organization>> {
    let result = [];
    try {
      result = await this.connection(OrganizationRepository.tableName).select("*").where("deletedAt", null);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return result as Array<Organization>;
  }
}
