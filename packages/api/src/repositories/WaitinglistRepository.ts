import { Injectable } from "@tsed/di";
import { $log } from "@tsed/common";

import { BaseRepository } from "./BaseRepository";
import { Nullable, WaitinglistDTO, Waitinglist, WaitinglistStatus } from "../domain";
import { QueryOptions } from "./repository.types";
import { SQLError } from "./SQLError";

@Injectable()
export class WaitinglistRepository extends BaseRepository {
  static tableName = "waitinglists";

  public async findById(id: string, queryOptions?: QueryOptions): Promise<Nullable<Waitinglist>> {
    if (!id) return null;

    let result;
    try {
      result = (
        await this.connection(WaitinglistRepository.tableName).select("*").where("id", id).where("deletedAt", null)
      )[0] as WaitinglistDTO;
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    if (!result) return null;

    return new Waitinglist(result);
  }

  public async findByRegistrationId(
    registrationId: string,
    queryOptions?: QueryOptions
  ): Promise<Nullable<Waitinglist>> {
    if (!registrationId) return null;

    let result;
    try {
      result = (
        await this.connection(WaitinglistRepository.tableName)
          .select("*")
          .where("registrationId", registrationId)
          .where("deletedAt", null)
      )[0] as WaitinglistDTO;
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    if (!result) return null;

    return new Waitinglist(result);
  }

  public async findAllByStatusAndUserId(
    status: Array<WaitinglistStatus>,
    userId: string,
    queryOptions?: QueryOptions
  ): Promise<Array<Waitinglist>> {
    if (!status || !userId) return [];

    let results = [];
    try {
      results = await this.connection(WaitinglistRepository.tableName)
        .select("*")
        .where("status", "in", status)
        .where("userId", userId)
        .where("deletedAt", null);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return results.map((result) => new Waitinglist(result));
  }

  public async findAllByStatus(
    status: Array<WaitinglistStatus>,
    queryOptions?: QueryOptions
  ): Promise<Array<Waitinglist>> {
    if (!status) return [];

    let results = [];
    try {
      results = await this.connection(WaitinglistRepository.tableName)
        .select("*")
        .where("status", "in", status)
        .where("deletedAt", null);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return results.map((result) => new Waitinglist(result));
  }

  public async save(waitinglist: Waitinglist, queryOptions?: QueryOptions): Promise<Waitinglist> {
    try {
      let query = this.connection(WaitinglistRepository.tableName);
      if (queryOptions?.transaction) query = query.transacting(queryOptions.transaction);
      await query.insert(waitinglist).returning("id");
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return waitinglist;
  }

  public async update(waitinglist: Waitinglist, fields = [], queryOptions?: QueryOptions): Promise<Waitinglist> {
    if (!fields) return waitinglist;
    try {
      let updateFields = {};
      fields.forEach((field) => (updateFields[field] = waitinglist[field]));

      let query = this.connection(WaitinglistRepository.tableName);
      if (queryOptions?.transaction) query = query.transacting(queryOptions.transaction);
      await query.update(updateFields).where("id", waitinglist.id);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return waitinglist;
  }

  public async remove(waitinglist: Waitinglist, queryOptions?: QueryOptions): Promise<boolean> {
    try {
      let query = this.connection(WaitinglistRepository.tableName);
      if (queryOptions?.transaction) query = query.transacting(queryOptions.transaction);
      await query.update("deletedAt", new Date()).where("id", waitinglist.id);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return true;
  }
}
