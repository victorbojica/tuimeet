import Knex from "knex";

export interface IRepository<T> {
  connection: T;
}

export type QueryOptions = {
  includeDeleted?: boolean;
  transaction?: Knex.Transaction;
};
