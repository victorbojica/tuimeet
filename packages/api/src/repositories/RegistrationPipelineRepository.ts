import Knex from "knex";
import { Injectable } from "@tsed/di";
import { $log, Inject } from "@tsed/common";

import { Nullable, RegistrationPipeline } from "../domain";
import { QueryOptions } from "./repository.types";
import { BaseRepository } from "./BaseRepository";
import { InternalDatabase } from "../lib/bootstrap/providers";
import { RegistrationRepository } from "./RegistrationRepository";
import { WaitinglistRepository } from "./WaitinglistRepository";
import { MeetingRepository } from "./MeetingRepository";
import { RecordingRepository } from "./RecordingRepository";
import { UserRepository } from "./UserRepository";
import { diff } from "../lib/__garbage/diff";
import { SQLError } from "./SQLError";

@Injectable()
export class RegistrationPipelineRepository extends BaseRepository {
  constructor(
    @Inject(InternalDatabase) public connection: Knex,
    @Inject(RegistrationRepository) protected registrationRepository: RegistrationRepository,
    @Inject(WaitinglistRepository) protected waitinglistRepository: WaitinglistRepository,
    @Inject(MeetingRepository) protected meetingRepository: MeetingRepository,
    @Inject(RecordingRepository) protected recordingRepository: RecordingRepository,
    @Inject(UserRepository) protected userRepository: UserRepository
  ) {
    super(connection);
  }

  async findById(registrationId: string, queryOptions?: QueryOptions): Promise<Nullable<RegistrationPipeline>> {
    try {
      const registration = await this.registrationRepository.findById(registrationId);
      if (!registration) return null;

      const waitinglist = await this.waitinglistRepository.findByRegistrationId(registrationId);
      const meeting = await this.meetingRepository.findByRegistrationId(registrationId);
      const recording = await this.recordingRepository.findById(meeting?.recordingId);
      const user = await this.userRepository.findById(waitinglist?.userId);

      return new RegistrationPipeline({ registration, waitinglist, meeting, recording, user });
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }
  }

  async update(registrationPipeline: RegistrationPipeline): Promise<RegistrationPipeline> {
    try {
      await this.transaction(async (transaction) => {
        const opts = { transaction };
        const prev = await this.findById(registrationPipeline.registration.id);

        const updatedRegistrationFields = diff(registrationPipeline.registration, prev.registration);
        await this.registrationRepository.update(registrationPipeline.registration, updatedRegistrationFields, opts);

        if (registrationPipeline.recording) {
          if (!prev.recording && registrationPipeline.recording) {
            await this.recordingRepository.save(registrationPipeline.recording, opts);
          } else {
            const updatedRecordingFields = diff(registrationPipeline.recording, prev.recording);
            await this.recordingRepository.update(registrationPipeline.recording, updatedRecordingFields, opts);
          }
        } else {
          if (prev.recording) {
            await this.recordingRepository.remove(prev.recording, opts);
          }
        }

        if (registrationPipeline.waitinglist) {
          if (!prev.waitinglist && registrationPipeline.waitinglist) {
            await this.waitinglistRepository.save(registrationPipeline.waitinglist, opts);
          } else {
            const updatedWaitinglistFields = diff(registrationPipeline.waitinglist, prev.waitinglist);
            await this.waitinglistRepository.update(registrationPipeline.waitinglist, updatedWaitinglistFields, opts);
          }
        } else {
          if (prev.waitinglist) {
            await this.waitinglistRepository.remove(prev.waitinglist, opts);
          }
        }

        if (registrationPipeline.meeting) {
          if (!prev.meeting) {
            await this.meetingRepository.save(registrationPipeline.meeting, opts);
          } else {
            const updatedMeetingFields = diff(registrationPipeline.meeting, prev.meeting);
            await this.meetingRepository.update(registrationPipeline.meeting, updatedMeetingFields, opts);
          }
        } else {
          if (prev.meeting) {
            await this.meetingRepository.remove(prev.meeting, opts);
          }
        }
      });
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return registrationPipeline;
  }
}
