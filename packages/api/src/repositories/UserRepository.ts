import { Injectable } from "@tsed/di";
import { $log } from "@tsed/common";

import { BaseRepository } from "./BaseRepository";
import { Nullable, User, UserRole } from "../domain";
import { QueryOptions } from "./repository.types";
import { SQLError } from "./SQLError";

@Injectable()
export class UserRepository extends BaseRepository {
  static tableName = "users";

  public async findById(id: string, queryOptions?: QueryOptions): Promise<Nullable<User>> {
    if (!id) return null;

    let result;
    try {
      result = (
        await this.connection(UserRepository.tableName).select("*").where("id", id).where("deletedAt", null)
      )[0];
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    if (!result) return null;

    return new User(result);
  }

  public async findByEmail(email, queryOptions?: QueryOptions): Promise<Nullable<User>> {
    if (!email) return null;

    let result;
    try {
      result = (
        await this.connection(UserRepository.tableName).select("*").where("email", email).where("deletedAt", null)
      )[0];
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    if (!result) return null;

    return new User(result);
  }

  public async findAllByRole(role: UserRole, queryOptions?: QueryOptions): Promise<Array<User>> {
    if (!role) return [];

    let result = [];
    try {
      result = await this.connection(UserRepository.tableName).select("*").where("role", role).where("deletedAt", null);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return result.map((user) => new User(user));
  }

  public async update(user: User, fields: Array<string>, queryOptions?: QueryOptions): Promise<User> {
    if (!fields.length) return user;
    try {
      let updateFields = {};
      fields.forEach((field) => {
        updateFields[field] = field === "schedule" ? JSON.stringify(user[field]) : user[field]; // TODO move this to DTO and mapper
      });
      let query = this.connection(UserRepository.tableName);
      if (queryOptions?.transaction) query = query.transacting(queryOptions.transaction);
      await query.update(updateFields).where("id", user.id);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return user;
  }
}
