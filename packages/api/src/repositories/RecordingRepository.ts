import { Injectable } from "@tsed/di";
import { $log } from "@tsed/common";

import { SQLError } from "./SQLError";
import { Nullable, Recording } from "../domain";
import { QueryOptions } from "./repository.types";
import { BaseRepository } from "./BaseRepository";

@Injectable()
export class RecordingRepository extends BaseRepository {
  static tableName = "recordings";

  async findById(id: string, queryOptions?: QueryOptions): Promise<Nullable<Recording>> {
    if (!id) return null;

    let result;
    try {
      result = (
        await this.connection(RecordingRepository.tableName).select("*").where("id", id).where("deletedAt", null)
      )[0];
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    if (!result) return null;

    return new Recording(result);
  }

  public async save(recording: Recording, queryOptions?: QueryOptions): Promise<Recording> {
    try {
      let query = this.connection(RecordingRepository.tableName);
      if (queryOptions?.transaction) query = query.transacting(queryOptions.transaction);
      query = query.insert(recording).returning("id");
      await query;
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return recording;
  }

  public async update(recording: Recording, fields = [], queryOptions?: QueryOptions): Promise<Recording> {
    if (!fields.length) return recording;
    try {
      let updateFields = {};
      fields.forEach((field) => (updateFields[field] = recording[field]));
      let query = this.connection(RecordingRepository.tableName);
      if (queryOptions?.transaction) query = query.transacting(queryOptions.transaction);
      await query.update(updateFields).where("id", recording.id);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return recording;
  }

  public async remove(recording: Recording, queryOptions?: QueryOptions): Promise<boolean> {
    try {
      let query = this.connection(RecordingRepository.tableName);
      if (queryOptions?.transaction) query = query.transacting(queryOptions.transaction);
      await query.update("deletedAt", new Date()).where("id", recording.id);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return true;
  }
}
