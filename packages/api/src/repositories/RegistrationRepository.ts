import { Injectable } from "@tsed/di";
import { $log } from "@tsed/common";

import { BaseRepository } from "./BaseRepository";
import { Nullable, RegistrationDTO, Registration, RegistrationStatus } from "../domain";
import { QueryOptions } from "./repository.types";
import { SQLError } from "./SQLError";

@Injectable()
export class RegistrationRepository extends BaseRepository {
  static tableName = "registrations";

  public async findById(id: string, queryOptions?: QueryOptions): Promise<Nullable<Registration>> {
    if (!id) return null;

    let result;
    try {
      result = (
        await this.connection(RegistrationRepository.tableName).select("*").where("id", id).where("deletedAt", null)
      )[0] as RegistrationDTO;
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    if (!result) return null;

    return new Registration(result);
  }

  public async findByEmail(email: string, queryOptions?: QueryOptions): Promise<Nullable<Registration>> {
    if (!email) return null;

    let result;
    try {
      result = (
        await this.connection(RegistrationRepository.tableName)
          .select("*")
          .where("email", email)
          .where("deletedAt", null)
      )[0] as RegistrationDTO;
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    if (!result) return null;

    return new Registration(result);
  }

  public async findAllById(id: Array<string>, queryOptions?: QueryOptions): Promise<Array<Registration>> {
    if (!id) return [];

    let results = [];
    try {
      results = await this.connection(RegistrationRepository.tableName)
        .select("*")
        .where("id", "in", id)
        .where("deletedAt", null);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return results.map((result) => new Registration(result));
  }

  public async findAllByStatus(
    status: Array<RegistrationStatus>,
    queryOptions?: QueryOptions
  ): Promise<Array<Registration>> {
    if (!status) return [];

    let results = [];
    try {
      results = await this.connection(RegistrationRepository.tableName)
        .select("*")
        .where("status", "in", status)
        .where("deletedAt", null);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return results.map((result) => new Registration(result));
  }

  public async findAllByOrganizationId(
    organizationId: string,
    queryOptions?: QueryOptions
  ): Promise<Array<Registration>> {
    if (!organizationId) return [];

    let results = [];
    try {
      results = await this.connection(RegistrationRepository.tableName)
        .select("*")
        .where("organizationId", "=", organizationId)
        .where("deletedAt", null);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return results.map((result) => new Registration(result));
  }

  public async update(registration: Registration, fields = [], queryOptions?: QueryOptions): Promise<Registration> {
    if (!fields.length) return registration;
    try {
      let updateFields = {};
      fields.forEach((field) => (updateFields[field] = registration[field]));
      let query = this.connection(RegistrationRepository.tableName);
      if (queryOptions?.transaction) query = query.transacting(queryOptions.transaction);
      await query.update(updateFields).where("id", registration.id);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return registration;
  }

  public async saveBulk(registrations: Array<Registration>, queryOptions?: QueryOptions): Promise<Array<Registration>> {
    try {
      let query = this.connection(RegistrationRepository.tableName);
      if (queryOptions?.transaction) query = query.transacting(queryOptions.transaction);
      query = query.insert(registrations);
      await query;
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return registrations;
  }
}
