import { Injectable } from "@tsed/di";
import { $log } from "@tsed/common";

import { SQLError } from "./SQLError";
import { QueryOptions } from "./repository.types";
import { BaseRepository } from "./BaseRepository";
import { Nullable, Meeting, MeetingDTO, MeetingStatus } from "../domain";

@Injectable()
export class MeetingRepository extends BaseRepository {
  static tableName = "meetings";

  public async findById(id: string, queryOptions?: QueryOptions): Promise<Nullable<Meeting>> {
    if (!id) return null;
    let result;
    try {
      result = (
        await this.connection(MeetingRepository.tableName).select("*").where("id", id).where("deletedAt", null)
      )[0] as MeetingDTO;
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    if (!result) return null;

    return new Meeting(result);
  }

  public async findByRegistrationId(registrationId): Promise<Nullable<Meeting>> {
    if (!registrationId) return null;

    let result;
    try {
      result = (
        await this.connection(MeetingRepository.tableName)
          .select("*")
          .where("registrationId", registrationId)
          .where("deletedAt", null)
      )[0] as MeetingDTO;
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    if (!result) return null;

    return new Meeting(result);
  }

  public async save(meeting: Meeting, queryOptions?: QueryOptions): Promise<Meeting> {
    try {
      let query = this.connection(MeetingRepository.tableName);
      if (queryOptions?.transaction) query = query.transacting(queryOptions.transaction);
      query = query.insert(meeting).returning("id");
      await query;
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return meeting;
  }

  public async findAllByStatusAndUserId(
    status: Array<MeetingStatus>,
    userId: string,
    queryOptions?: QueryOptions
  ): Promise<Array<Meeting>> {
    if (!userId || !status) return [];

    let results = [];
    try {
      let query = this.connection(MeetingRepository.tableName)
        .select("*")
        .where("status", "in", status)
        .where("userId", userId);
      if (!queryOptions?.includeDeleted) query = query.where("deletedAt", null);

      results = await query;
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return results.map((result) => new Meeting(result));
  }

  public async findAllByStatus(status: Array<MeetingStatus>, queryOptions?: QueryOptions): Promise<Array<Meeting>> {
    if (!status) return [];

    let results = [];
    try {
      let query = this.connection(MeetingRepository.tableName).select("*").where("status", "in", status);
      if (!queryOptions?.includeDeleted) query = query.where("deletedAt", null);

      results = await query;
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return results.map((result) => new Meeting(result));
  }

  public async update(meeting: Meeting, fields = [], queryOptions?: QueryOptions): Promise<Meeting> {
    if (!fields.length) return meeting;
    try {
      let updateFields = {};
      fields.forEach((field) => (updateFields[field] = meeting[field]));
      let query = this.connection(MeetingRepository.tableName);
      if (queryOptions?.transaction) query = query.transacting(queryOptions.transaction);
      await query.update(updateFields).where("id", meeting.id);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return meeting;
  }

  public async remove(meeting: Meeting, queryOptions?: QueryOptions): Promise<boolean> {
    try {
      let query = this.connection(MeetingRepository.tableName);
      if (queryOptions?.transaction) query = query.transacting(queryOptions.transaction);
      await query.update("deletedAt", new Date()).where("id", meeting.id);
    } catch (e) {
      $log.error(e);
      throw e as SQLError;
    }

    return true;
  }
}
