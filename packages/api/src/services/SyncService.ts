import { v4 as uuid } from "uuid";
import { Inject, Injectable, ProviderScope, ProviderType } from "@tsed/di";
import { $log } from "@tsed/common";

import { Registration, RegistrationStatus } from "../domain";
import { RegistrationRepository, ReferenceRepository, OrganizationRepository } from "../repositories";
import { ITickService } from "./service.types";

@Injectable({
  type: ProviderType.SERVICE,
  scope: ProviderScope.SINGLETON,
})
export class SyncService implements ITickService {
  constructor(
    @Inject(OrganizationRepository) protected organizationRepository: OrganizationRepository,
    @Inject(RegistrationRepository) protected registrationRepository: RegistrationRepository,
    @Inject(ReferenceRepository) protected referenceRepository: ReferenceRepository
  ) {}

  // every n minutes
  static readonly nthTick = 5;

  async $onCronTick(ticks: number): Promise<void> {
    if (!this.tickRunScheduled(ticks)) return;
    await this.syncRegistrations();
  }

  tickRunScheduled(ticks): boolean {
    return ticks % SyncService.nthTick === 0;
  }

  async syncRegistrations(): Promise<Array<Registration>> {
    const registrations = [];
    const organizations = await this.organizationRepository.findAll();
    for await (const organization of organizations) {
      const referenceRegistrations = await this.referenceRepository.findAllRegistrationsByOrganizationRefId(
        organization.refId
      );
      const existingRegistrations = await this.registrationRepository.findAllByOrganizationId(organization.id);

      const newRegistrations = referenceRegistrations.reduce((acc, referenceRegistration) => {
        const existingRegistration = existingRegistrations.find(
          (registration) => registration.refId === referenceRegistration.id
        );
        if (existingRegistration) return acc;

        const registration = Registration.create({
          firstName: referenceRegistration.firstName,
          lastName: referenceRegistration.lastName,
          email: referenceRegistration.email,
          refId: referenceRegistration.id,
          organizationId: organization.id,
        });

        return [registration, ...acc];
      }, []);

      await this.registrationRepository.saveBulk(newRegistrations);

      registrations.push(...newRegistrations);
    }

    return registrations;
  }
}
