import { Inject, Injectable, ProviderScope, ProviderType } from "@tsed/di";

import { RegistrationPipeline } from "../domain";
import { EmailService } from "./EmailService";

@Injectable({
  type: ProviderType.SERVICE,
  scope: ProviderScope.SINGLETON,
})
export class RegistrationNotificationService {
  constructor(@Inject(EmailService) protected emailService: EmailService) {}

  async meetingScheduled(registrationPipeline: RegistrationPipeline): Promise<void> {
    await this.emailService.send({
      to: registrationPipeline.registration.email,
      template: {
        name: "meetingScheduled.registration",
        subject: "Meeting has been scheduled",
        params: registrationPipeline,
      },
    });
  }

  async meetingRescheduled(registrationPipeline: RegistrationPipeline): Promise<void> {
    await this.emailService.send({
      to: registrationPipeline.registration.email,
      template: {
        name: "meetingRescheduled.registration",
        subject: "Meeting has been rescheduled",
        params: registrationPipeline,
      },
    });
  }

  async meetingCanceled(registrationPipeline: RegistrationPipeline): Promise<void> {
    await this.emailService.send({
      to: registrationPipeline.registration.email,
      template: {
        name: "meetingCanceled.registration",
        subject: "Meeting has been canceled",
        params: registrationPipeline,
      },
    });
  }

  async meetingStarting(registrationPipeline: RegistrationPipeline): Promise<void> {
    await this.emailService.send({
      to: registrationPipeline.registration.email,
      template: {
        name: "meetingStarting.registration",
        subject: "Meeting is about to start",
        params: registrationPipeline,
      },
    });
  }

  async registrationCanceled(registrationPipeline: RegistrationPipeline): Promise<void> {
    await this.emailService.send({
      to: registrationPipeline.registration.email,
      template: {
        name: "registrationCanceled.registration",
        subject: "Your registration has been canceled",
        params: registrationPipeline,
      },
    });
  }

  async registrationCompleted(registrationPipeline: RegistrationPipeline): Promise<void> {
    await this.emailService.send({
      to: registrationPipeline.registration.email,
      template: {
        name: "registrationCompleted.registration",
        subject: "Your registration has been completed",
        params: registrationPipeline,
      },
    });
  }
}
