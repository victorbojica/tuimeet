import { Inject, Injectable, ProviderScope, ProviderType } from "@tsed/di";

import { UserRole } from "../domain";
import { UserRepository, RegistrationRepository } from "../repositories";
import { JWTPayload } from "../lib/bootstrap/protocols/protocol.types";

@Injectable({
  type: ProviderType.SERVICE,
  scope: ProviderScope.SINGLETON,
})
export class AuthorizationService {
  constructor(
    @Inject(UserRepository) protected userRepository: UserRepository,
    @Inject(RegistrationRepository) protected registrationRepository: RegistrationRepository
  ) {}

  public async authorizeAdmin(payload: JWTPayload): Promise<boolean> {
    let user = await this.userRepository.findById(payload.sub);
    if (user && user.role === UserRole.administrator) return true;
    return false;
  }

  public async authorizeMember(payload: JWTPayload): Promise<boolean> {
    let user = await this.userRepository.findById(payload.sub);
    if (user && user.role === UserRole.member) return true;
    return false;
  }

  public async authorizeRegistration(payload: JWTPayload): Promise<boolean> {
    let registration = await this.registrationRepository.findById(payload.sub);
    if (registration) return true;
    return false;
  }
}
