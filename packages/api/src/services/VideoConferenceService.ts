import { v4 as uuid } from "uuid";
import { Inject, Injectable, ProviderScope, ProviderType } from "@tsed/di";
import { $log } from "@tsed/common";

import { BusinessLogicError, Registration, RegistrationPipeline, RegistrationStatus } from "../domain";
import { RegistrationRepository, ReferenceRepository, OrganizationRepository } from "../repositories";
import { IVideoConference, VideoConference } from "../lib/bootstrap/providers";
import { ITickService } from "./service.types";

@Injectable({
  type: ProviderType.SERVICE,
  scope: ProviderScope.SINGLETON,
})
export class VideoConferenceService implements ITickService {
  constructor(@Inject(VideoConference) protected videoConference: IVideoConference) {}

  static nthTick = 1;
  async $onCronTick(ticks: number): Promise<void> {
    if (!this.tickRunScheduled(ticks)) return;
    const meetings = await this.videoConference.list();
    $log.debug("meetings", meetings);
  }

  async create(registrationPipeline: RegistrationPipeline): Promise<any> {
    const existing = await this.videoConference.get(registrationPipeline.registration.id);
    if (existing)
      await this.videoConference.end(registrationPipeline.registration.id, { password: existing.moderatorPW });
    return await this.videoConference.create(registrationPipeline.registration.id, {
      startTime: registrationPipeline.meeting.estimatedStartTime,
    });
  }

  async cancel(registrationPipeline: RegistrationPipeline): Promise<void> {
    const existing = await this.videoConference.get(registrationPipeline.registration.id);
    if (!existing) return;
    await this.videoConference.end(registrationPipeline.registration.id, { password: existing.moderatorPW });
  }

  async getRegistrationURL(registrationPipeline: RegistrationPipeline): Promise<any> {
    const existing = await this.videoConference.get(registrationPipeline.registration.id);
    if (!existing) throw new BusinessLogicError("Vide conference not found");

    return await this.videoConference.getURL(registrationPipeline.registration.id, {
      password: existing.attendeePW,
      fullName: registrationPipeline.registration.fullName,
    });
  }

  async getUserURL(registrationPipeline: RegistrationPipeline): Promise<string> {
    const existing = await this.videoConference.get(registrationPipeline.registration.id);
    if (!existing) throw new BusinessLogicError("Vide conference not found");

    return await this.videoConference.getURL(registrationPipeline.registration.id, {
      password: existing.moderatorPW,
      fullName: registrationPipeline.user.fullName,
    });
  }

  tickRunScheduled(ticks): boolean {
    return ticks % VideoConferenceService.nthTick === 0;
  }
}
