import { Inject, Injectable, ProviderScope, ProviderType } from "@tsed/di";
import { $log } from "@tsed/common";
import {
  MeetingSchedule,
  MeetingStatus,
  RegistrationPipeline,
  RegistrationStatus,
  ResourceNotFoundError,
  UserRole,
} from "../domain";
import {
  MeetingRepository,
  RegistrationPipelineRepository,
  RegistrationRepository,
  UserRepository,
} from "../repositories";
import { MemberNotificationService } from "./MemberNotificationService";
import { RegistrationNotificationService } from "./RegistrationNotificationService";
import { ITickService } from "./service.types";
import { VideoConferenceService } from "./VideoConferenceService";

@Injectable({
  type: ProviderType.SERVICE,
  scope: ProviderScope.SINGLETON,
})
export class RegistrationPipelineService implements ITickService {
  constructor(
    @Inject(UserRepository) protected userRepository: UserRepository,
    @Inject(MeetingRepository) protected meetingRepository: MeetingRepository,
    @Inject(RegistrationRepository) protected registrationRepository: RegistrationRepository,
    @Inject(RegistrationPipelineRepository) protected registrationPipelineRepository: RegistrationPipelineRepository,
    @Inject(MemberNotificationService) protected memberNotificationService: MemberNotificationService,
    @Inject(RegistrationNotificationService) protected registrationNotificationService: RegistrationNotificationService,
    @Inject(VideoConferenceService) protected videoConferenceService: VideoConferenceService
  ) {}

  static readonly nthTick = 1;
  static sentNotifications = {};

  async $onCronTick(ticks): Promise<void> {
    if (!this.tickRunScheduled(ticks)) return;

    // again, not pretty or fast, but easy
    let registrations = await this.registrationRepository.findAllByStatus([RegistrationStatus.waitinglist]);
    for await (let registration of registrations) {
      try {
        await this.completeRegistration(registration.id);
        $log.info(`APP :: CRON :: Completed registration for ${registration.id}`);
      } catch (e) {}
    }

    const meetings = await this.meetingRepository.findAllByStatus([MeetingStatus.pending]);
    for await (const meeting of meetings) {
      if (meeting.isAboutToHappen) {
        const registrationPipeline = await this.registrationPipelineRepository.findById(meeting.registrationId);
        const notificationKey = `${
          registrationPipeline.registration.id
        }/${registrationPipeline.meeting.estimatedStartTime.toISOString()}`;
        if (RegistrationPipelineService.sentNotifications[notificationKey]) continue;

        await this.memberNotificationService.meetingStarting(registrationPipeline);
        await this.registrationNotificationService.meetingStarting(registrationPipeline);

        RegistrationPipelineService.sentNotifications[notificationKey] = true;
      }
    }
  }

  tickRunScheduled(ticks): boolean {
    return ticks % RegistrationPipelineService.nthTick === 0;
  }

  async joinWaitinglist(registrationId: string, rest: { userId: string }): Promise<RegistrationPipeline> {
    let registrationPipeline = await this.registrationPipelineRepository.findById(registrationId);
    if (!registrationPipeline) throw new ResourceNotFoundError("Registration not found");
    const user = await this.userRepository.findById(rest.userId);

    registrationPipeline = registrationPipeline.joinWaitinglist({ user });

    await this.registrationPipelineRepository.update(registrationPipeline);

    await this.memberNotificationService.registrationJoinedWaitinglist(registrationPipeline);

    return registrationPipeline;
  }

  async cancelWaitinglist(registrationId: string): Promise<RegistrationPipeline> {
    let registrationPipeline = await this.registrationPipelineRepository.findById(registrationId);
    if (!registrationPipeline) throw new ResourceNotFoundError("Registration not found");

    registrationPipeline = registrationPipeline.cancelWaitinglist();

    await this.registrationPipelineRepository.update(registrationPipeline);

    await this.memberNotificationService.registrationCanceledWaitinglist(registrationPipeline);

    return registrationPipeline;
  }

  async scheduleMeeting(registrationId: string, rest: { schedule: MeetingSchedule }): Promise<RegistrationPipeline> {
    let registrationPipeline = await this.registrationPipelineRepository.findById(registrationId);
    if (!registrationPipeline) throw new ResourceNotFoundError("Registration not found");

    registrationPipeline = registrationPipeline.scheduleMeeting({ schedule: rest.schedule });

    await this.registrationPipelineRepository.update(registrationPipeline);

    await this.registrationNotificationService.meetingScheduled(registrationPipeline);

    await this.videoConferenceService.create(registrationPipeline);

    return registrationPipeline;
  }

  async rescheduleMeeting(
    registrationId: string,
    rest: { schedule: MeetingSchedule; reason: string }
  ): Promise<RegistrationPipeline> {
    let registrationPipeline = await this.registrationPipelineRepository.findById(registrationId);
    if (!registrationPipeline) throw new ResourceNotFoundError("Registration not found");

    registrationPipeline = registrationPipeline.rescheduleMeeting(rest);

    await this.registrationPipelineRepository.update(registrationPipeline);

    await this.videoConferenceService.create(registrationPipeline);

    await this.registrationNotificationService.meetingRescheduled(registrationPipeline);

    return registrationPipeline;
  }

  async joinMeeting(registrationId: string, role: UserRole | "registration"): Promise<RegistrationPipeline> {
    let registrationPipeline = await this.registrationPipelineRepository.findById(registrationId);
    if (!registrationPipeline) throw new ResourceNotFoundError("Registration not found");

    if (role === "registration") {
      const registrationURL = await this.videoConferenceService.getRegistrationURL(registrationPipeline);
      registrationPipeline = registrationPipeline.startMeeting().registrationJoinMeeting({ registrationURL });
    } else {
      const userURL = await this.videoConferenceService.getUserURL(registrationPipeline);
      registrationPipeline = registrationPipeline.startMeeting().userJoinMeeting({ userURL });
    }

    await this.registrationPipelineRepository.update(registrationPipeline);

    return registrationPipeline;
  }

  async completeMeeting(registrationId: string, rest: { notes: string; recordingPath?: string }) {
    let registrationPipeline = await this.registrationPipelineRepository.findById(registrationId);
    if (!registrationPipeline) throw new ResourceNotFoundError("Registration not found");

    registrationPipeline = registrationPipeline.completeMeeting(rest);

    await this.registrationPipelineRepository.update(registrationPipeline);

    return registrationPipeline;
  }

  async cancelMeeting(registrationId: string, rest: { reason: string }): Promise<RegistrationPipeline> {
    let registrationPipeline = await this.registrationPipelineRepository.findById(registrationId);
    if (!registrationPipeline) throw new ResourceNotFoundError("Registration not found");

    registrationPipeline = registrationPipeline.cancelMeeting(rest);

    await this.registrationPipelineRepository.update(registrationPipeline);

    await this.videoConferenceService.cancel(registrationPipeline);

    await this.registrationNotificationService.meetingCanceled(registrationPipeline);

    return registrationPipeline;
  }

  async cancelRegistration(registrationId: string, rest: { reason: string }): Promise<RegistrationPipeline> {
    let registrationPipeline = await this.registrationPipelineRepository.findById(registrationId);
    if (!registrationPipeline) throw new ResourceNotFoundError("Registration not found");

    registrationPipeline = registrationPipeline.cancelRegistrationPipeline(rest);

    await this.registrationPipelineRepository.update(registrationPipeline);

    await this.videoConferenceService.cancel(registrationPipeline);

    await this.registrationNotificationService.registrationCanceled(registrationPipeline);

    return registrationPipeline;
  }

  async completeRegistration(registrationId: string): Promise<RegistrationPipeline> {
    let registrationPipeline = await this.registrationPipelineRepository.findById(registrationId);
    if (!registrationPipeline) throw new ResourceNotFoundError("Registration not found");

    registrationPipeline = registrationPipeline.completeRegistrationPipeline();

    await this.registrationPipelineRepository.update(registrationPipeline);

    await this.registrationNotificationService.registrationCompleted(registrationPipeline);
    await this.memberNotificationService.registrationCompleted(registrationPipeline);

    return registrationPipeline;
  }
}
