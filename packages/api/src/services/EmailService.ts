import { promises } from "fs";
import { MailService } from "@sendgrid/mail";
import { $log } from "@tsed/common";
import { Inject, Injectable, ProviderScope, ProviderType } from "@tsed/di";
import Mustache from "mustache";
import { Memoize } from "typescript-memoize";

import { Mailer } from "../lib/bootstrap/providers";
import config from "../lib/config";

export type EmailSendDTO = {
  to: string;
  from?: string;
  template: EmailTemplate;
};

export type EmailTemplate = { name: string; params: any; subject: string };

export type EmailSendOptions = { waitForResponse: boolean };

@Injectable({
  type: ProviderType.SERVICE,
  scope: ProviderScope.SINGLETON,
})
export class EmailService {
  constructor(@Inject(Mailer) protected mailer: MailService) {}

  static handleResponseError(response: any) {
    if (response[0].statusCode > 299) {
      $log.debug(response);
      throw new Error("Email could not be sent");
    }
  }

  @Memoize()
  static async loadTemplate(name: string) {
    const path = `${__dirname}/../lib/resources/email-templates/${name}.mustache`;
    let data = await promises.readFile(path);
    return data instanceof Buffer ? data.toString() : data;
  }

  static buildTemplate(templateString: string, params: any): string {
    return Mustache.render(templateString, params);
  }

  async send(data: EmailSendDTO, options?: EmailSendOptions): Promise<boolean | void> {
    const templateString = await EmailService.loadTemplate(data.template.name);
    const templateHTML = EmailService.buildTemplate(templateString, data.template.params);

    const promise = this.mailer.send({
      to: data.to,
      from: data.from || config.mailer.from,
      subject: data.template.subject,
      html: templateHTML,
    });

    if (options?.waitForResponse) {
      let result = await promise;
      EmailService.handleResponseError(result);
      return true;
    } else {
      promise.then((result) => EmailService.handleResponseError(result));
    }
  }
}
