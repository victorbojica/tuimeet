import * as _ from "lodash";
import { Inject, Injectable, ProviderScope, ProviderType } from "@tsed/di";

import { User, UserRole, Waitinglist, WaitinglistStatus } from "../domain";
import {
  RegistrationPipelineRepository,
  RegistrationRepository,
  UserRepository,
  WaitinglistRepository,
} from "../repositories";
import { RegistrationPipelineService } from "./RegistrationPipelineService";

@Injectable({
  type: ProviderType.SERVICE,
  scope: ProviderScope.SINGLETON,
})
export class StudentService {
  constructor(
    @Inject(UserRepository) protected userRepository: UserRepository,
    @Inject(RegistrationRepository) protected registrationRepository: RegistrationRepository,
    @Inject(WaitinglistRepository) protected waitinglistRepository: WaitinglistRepository,
    @Inject(RegistrationPipelineRepository) protected registrationPipelineRepository: RegistrationPipelineRepository,
    @Inject(RegistrationPipelineService) protected registrationPipelineService: RegistrationPipelineService
  ) {}

  async getProfile(id: string) {
    const registrationPipeline = await this.registrationPipelineRepository.findById(id);

    return {
      status: registrationPipeline.status,
      firstName: registrationPipeline.registration.firstName,
      lastName: registrationPipeline.registration.lastName,
      email: registrationPipeline.registration.email,
      memberFullName: registrationPipeline.user?.fullName,
      waitTime: 20, // TODO add proper calculation
      meetingTime: registrationPipeline.meeting?.estimatedStartTime,
      meetingURL: registrationPipeline.meeting?.registrationURL,
      meetingIsAboutToHappen: registrationPipeline.meeting?.isAboutToHappen,
    };
  }

  async joinWaitinglist(id: string): Promise<Waitinglist> {
    const waitinglistUser = await this.getWaitinglistUser();
    const registrationPipeline = await this.registrationPipelineService.joinWaitinglist(id, {
      userId: waitinglistUser.id,
    });

    return registrationPipeline.waitinglist;
  }

  async cancelWaitinglist(id: string): Promise<boolean> {
    await this.registrationPipelineService.cancelWaitinglist(id);
    return true;
  }

  async joinMeeting(id: string): Promise<boolean> {
    await this.registrationPipelineService.joinMeeting(id, "registration");
    return true;
  }

  protected async getWaitinglistUser(): Promise<User> {
    const users = await this.userRepository.findAllByRole(UserRole.member);
    const usersById = _.keyBy(users, "id");

    const waitinglist = await this.waitinglistRepository.findAllByStatus([
      WaitinglistStatus.pending,
      WaitinglistStatus.meeting,
    ]);
    const waitinglistByUserId = _.groupBy(waitinglist, "userId");

    let sum = 0;
    let count = 0;
    let avg = 0;
    let userWorkloads = [];
    _.each(users, (user) => {
      const userId = user.id;
      const waitinglists = waitinglistByUserId[userId];

      sum += waitinglists?.length || 0;
      count += 1;
      avg = sum / count;
      let lastCreatedAt;
      if (!waitinglistByUserId[userId] || !waitinglistByUserId[userId].length) {
        lastCreatedAt = new Date("1970-01-02Z00:00:00:000");
      } else {
        waitinglistByUserId[userId] = _.orderBy(waitinglists, "createdAt");
        lastCreatedAt = waitinglistByUserId[userId][waitinglistByUserId[userId].length - 1].createdAt;
      }

      userWorkloads.push({ userId, size: waitinglists?.length || 0, lastCreatedAt });
    });
    avg = Math.ceil(avg);
    userWorkloads = _.orderBy(userWorkloads, ["size", "lastCreatedAt"]);

    let leastWorkedUsers = [];
    let mostWorkedUsers = [];
    userWorkloads.forEach((userWorkload) => {
      if (userWorkload.size < avg) leastWorkedUsers.push(userWorkload);
      else mostWorkedUsers.push(userWorkload);
    });

    let selectedUserId;
    if (!leastWorkedUsers.length && !mostWorkedUsers.length) {
      let userIds = _.map(users, "id");
      let max = userIds.length;
      let min = 0;
      let randomIndex = Math.floor(Math.random() * (max - min) + min);
      selectedUserId = userIds[randomIndex];
    } else {
      let userSelectionList = leastWorkedUsers.length ? leastWorkedUsers : mostWorkedUsers;
      let firstSize = userSelectionList[0].size;
      selectedUserId = _.groupBy(userSelectionList, "size")[firstSize][0].userId;
    }

    return usersById[selectedUserId];
  }
}
