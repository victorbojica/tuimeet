import { Inject, Injectable, ProviderScope, ProviderType } from "@tsed/di";

import { RegistrationPipeline, ResourceNotFoundError } from "../domain";
import { EmailService } from "./EmailService";
import { MeetingRepository, RegistrationPipelineRepository } from "../repositories";

@Injectable({
  type: ProviderType.SERVICE,
  scope: ProviderScope.SINGLETON,
})
export class MemberNotificationService {
  constructor(
    @Inject(EmailService) protected emailService: EmailService,
    @Inject(MeetingRepository) protected meetingRepository: MeetingRepository,
    @Inject(RegistrationPipelineRepository) protected registrationPipelineRepository: RegistrationPipelineRepository
  ) {}

  async registrationJoinedWaitinglist(registrationPipeline: RegistrationPipeline): Promise<void> {
    if (!registrationPipeline.user) throw new ResourceNotFoundError("User not found");

    await this.emailService.send({
      to: registrationPipeline.user.email,
      template: {
        name: "joinedWaitinglist.member",
        subject: "Someone joined the waitinglist",
        params: registrationPipeline,
      },
    });
  }

  async registrationCanceledWaitinglist(registrationPipeline: RegistrationPipeline): Promise<void> {
    if (!registrationPipeline.user) throw new ResourceNotFoundError("User not found");

    await this.emailService.send({
      to: registrationPipeline.user.email,
      template: {
        name: "canceledWaitinglist.member",
        subject: "Someone canceled the waitinglist",
        params: registrationPipeline,
      },
    });
  }

  async meetingStarting(registrationPipeline: RegistrationPipeline): Promise<void> {
    if (!registrationPipeline.user) throw new ResourceNotFoundError("User not found");

    await this.emailService.send({
      to: registrationPipeline.user.email,
      template: {
        name: "meetingStarting.member",
        subject: "Meeting about to start",
        params: registrationPipeline,
      },
    });
  }

  async registrationCompleted(registrationPipeline: RegistrationPipeline): Promise<void> {
    if (!registrationPipeline.user) throw new ResourceNotFoundError("User not found");

    await this.emailService.send({
      to: registrationPipeline.user.email,
      template: {
        name: "registrationCompleted.member",
        subject: "Registration completed",
        params: registrationPipeline,
      },
    });
  }
}
