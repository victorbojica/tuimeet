import { promises as fs } from "fs";
import { Injectable, ProviderScope, ProviderType } from "@tsed/di";

@Injectable({
  type: ProviderType.SERVICE,
  scope: ProviderScope.SINGLETON,
})
export class StorageService {
  // TODO move to config
  static readonly storagePath = `${__dirname}/../../.tmp/uploads`;

  async saveUpload(name: string, file: Express.Multer.File): Promise<string> {
    const data = await fs.readFile(file.path);
    const extension = file.originalname.split(".")[file.originalname.split(".").length - 1];

    const filePath = `${StorageService.storagePath}/${name}.${extension}`;
    await fs.writeFile(filePath, data);

    await fs.unlink(file.path);

    return filePath;
  }
}
