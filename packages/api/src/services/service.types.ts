export interface ITickService {
  $onCronTick(ticks: number): Promise<void> | void;
  tickRunScheduled(ticks: number): boolean;
}
