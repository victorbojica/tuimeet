import { Inject, Injectable, ProviderType, ProviderScope } from "@tsed/di";
import jwt from "jsonwebtoken";

import config from "../lib/config";
import { Registration, User } from "../domain";
import { UserRepository, RegistrationRepository } from "../repositories";

@Injectable({
  type: ProviderType.SERVICE,
  scope: ProviderScope.SINGLETON,
})
export class AuthenticationService {
  constructor(
    @Inject(UserRepository) protected userRepository: UserRepository,
    @Inject(RegistrationRepository) protected registrationRepository: RegistrationRepository
  ) {}

  // TODO change to email and pass or whatever is needed
  public async authenticateUser(email: string): Promise<string | boolean> {
    const user = await this.userRepository.findByEmail(email);
    if (!user) return false;

    return this.issueUserToken(user);
  }

  public async authenticateRegistration(email: string): Promise<string | boolean> {
    const registration = await this.registrationRepository.findByEmail(email);
    if (!registration) return false;
    // if (registration.isFinished) return false;

    return this.issueRegistrationToken(registration);
  }

  private issueUserToken(user: User): string {
    return this.issueToken({ sub: user.id, role: user.role, organizationId: user.organizationId });
  }

  private issueRegistrationToken(registration: Registration): string {
    return this.issueToken({ sub: registration.id, organizationId: registration.organizationId });
  }

  private issueToken(payload: any): string {
    return jwt.sign(payload, config.app.secret, {
      issuer: config.app.endpoint,
      audience: "*",
      expiresIn: "200m",
    });
  }
}
