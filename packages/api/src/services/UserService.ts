import { Inject, Injectable, ProviderScope, ProviderType } from "@tsed/di";

import { User, UserDTO, ResourceNotFoundError } from "../domain";
import { UserRepository } from "../repositories";

@Injectable({
  type: ProviderType.SERVICE,
  scope: ProviderScope.SINGLETON,
})
export class UserService {
  constructor(@Inject(UserRepository) protected userRepository: UserRepository) {}

  async updateUserSettings(id: string, rest: Pick<UserDTO, "schedule" | "meetingDuration">): Promise<User> {
    const user = await this.userRepository.findById(id);
    if (!user) throw new ResourceNotFoundError("Could not find user");

    user.schedule = rest.schedule;
    user.meetingDuration = rest.meetingDuration;
    await this.userRepository.update(user, ["schedule", "meetingDuration"]);

    return user;
  }

  async getUser(id: string): Promise<User> {
    return this.userRepository.findById(id);
  }
}
