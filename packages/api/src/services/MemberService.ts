import { Inject, Injectable, ProviderScope, ProviderType } from "@tsed/di";
import {
  Meeting,
  MeetingSchedule,
  MeetingStatus,
  Nullable,
  Recording,
  Registration,
  RegistrationPipeline,
  ResourceNotFoundError,
  User,
  UserDTO,
  UserRole,
  WaitinglistStatus,
} from "../domain";
import {
  MeetingRepository,
  RecordingRepository,
  ReferenceRepository,
  RegistrationPipelineRepository,
  RegistrationRepository,
  WaitinglistRepository,
} from "../repositories";
import { UserService } from "./UserService";
import { RegistrationPipelineService } from "./RegistrationPipelineService";

@Injectable({
  type: ProviderType.SERVICE,
  scope: ProviderScope.SINGLETON,
})
export class MemberService {
  constructor(
    @Inject(MeetingRepository) protected meetingRepository: MeetingRepository,
    @Inject(WaitinglistRepository) protected waitinglistRepository: WaitinglistRepository,
    @Inject(RegistrationRepository) protected registrationRepository: RegistrationRepository,
    @Inject(RegistrationPipelineRepository) protected registrationPipelineRepository: RegistrationPipelineRepository,
    @Inject(RecordingRepository) protected recordingRepository: RecordingRepository,
    @Inject(UserService) protected userService: UserService,
    @Inject(RegistrationPipelineService) protected registrationPipelineService: RegistrationPipelineService,
    @Inject(ReferenceRepository) protected referenceRepository: ReferenceRepository
  ) {}

  async completeMeeting(
    registrationId: string,
    rest: { notes: string; recodingPath?: string }
  ): Promise<RegistrationPipeline> {
    return await this.registrationPipelineService.completeMeeting(registrationId, {
      notes: rest.notes,
      recordingPath: rest.recodingPath,
    });
  }

  async joinMeeting(registrationId: string): Promise<RegistrationPipeline> {
    return await this.registrationPipelineService.joinMeeting(registrationId, UserRole.member);
  }

  async cancelMeeting(registrationId: string, rest: { reason: string }): Promise<RegistrationPipeline> {
    return await this.registrationPipelineService.cancelMeeting(registrationId, rest);
  }

  async rescheduleMeeting(
    registrationId: string,
    rest: { schedule: MeetingSchedule; reason: string }
  ): Promise<RegistrationPipeline> {
    return await this.registrationPipelineService.rescheduleMeeting(registrationId, rest);
  }

  async scheduleMeeting(registrationId: string, rest: { schedule: MeetingSchedule }): Promise<RegistrationPipeline> {
    return await this.registrationPipelineService.scheduleMeeting(registrationId, rest);
  }

  async cancelRegistration(registrationId: string, rest: { reason: string }): Promise<RegistrationPipeline> {
    return await this.registrationPipelineService.cancelRegistration(registrationId, rest);
  }

  async listMeetings(userId: string): Promise<Array<{ meeting: Meeting; registration: Registration }>> {
    const meetings = await this.meetingRepository.findAllByStatusAndUserId([MeetingStatus.complete], userId, {
      includeDeleted: true,
    });
    const registrations = await this.registrationRepository.findAllById(
      meetings.map((meeting) => meeting.registrationId)
    );

    return meetings.map((meeting) => {
      const registration = registrations.find((r) => r.id === meeting.registrationId);
      return { meeting, registration };
    });
  }

  async listWaitinglist(userId: string): Promise<Array<any>> {
    // dumb, easy and pretty. just how i like them. TODO optimise if there is the need
    const waitinglists = await this.waitinglistRepository.findAllByStatusAndUserId(
      [WaitinglistStatus.pending, WaitinglistStatus.meeting],
      userId
    );

    let registrationPipelines = [];
    for await (let waitinglist of waitinglists) {
      let registrationPipeline = await this.registrationPipelineRepository.findById(waitinglist.registrationId);

      registrationPipelines.push(registrationPipeline);
    }

    return registrationPipelines;
  }

  async registrationInfo(registrationId: string): Promise<any> {
    const registrationPipeline = await this.registrationPipelineRepository.findById(registrationId);
    if (!registrationPipeline) throw new ResourceNotFoundError("Registration not found");

    const referenceRegistration = await this.referenceRepository.findRegistrationByRefId(
      registrationPipeline.registration.refId
    );
    return { ...registrationPipeline.toJSON(), referenceRegistration };
  }

  async getRecording(registrationId: string): Promise<Nullable<Recording>> {
    const meeting = await this.meetingRepository.findByRegistrationId(registrationId);
    if (!meeting) return null;

    return await this.recordingRepository.findById(meeting.recordingId);
  }

  async updateProfile(userId: string, rest: Pick<UserDTO, "schedule" | "meetingDuration">): Promise<User> {
    return await this.userService.updateUserSettings(userId, rest);
  }

  async getProfile(userId: string): Promise<User> {
    return await this.userService.getUser(userId);
  }
}
