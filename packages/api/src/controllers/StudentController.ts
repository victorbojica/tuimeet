import { Controller, Get, Post, ContentType, Context, PathParams } from "@tsed/common";
import { Authorize } from "@tsed/passport";
import { StudentService } from "../services";

@Controller("/student")
export class StudentController {
  constructor(protected readonly studentService: StudentService) {}

  @Authorize("registration")
  @ContentType("json")
  @Get("/profile")
  async getMe(@Context("registration") registration: any) {
    return await this.studentService.getProfile(registration.id);
  }

  @Authorize("registration")
  @ContentType("json")
  @Post("/join")
  async joinWaitinglist(@Context("registration") registration: any) {
    return await this.studentService.joinWaitinglist(registration.id);
  }

  @Authorize("registration")
  @ContentType("json")
  @Post("/cancel")
  async cancelWaitinglist(@Context("registration") registration: any) {
    return await this.studentService.cancelWaitinglist(registration.id);
  }

  @Authorize("registration")
  @ContentType("json")
  @Post("/join-meeting")
  async joinMeeting(@Context("registration") registration: any) {
    return await this.studentService.joinMeeting(registration.id);
  }
}
