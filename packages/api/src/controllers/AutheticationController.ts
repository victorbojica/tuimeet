import { Controller, Post, ContentType, BodyParams } from "@tsed/common";
import { AuthenticationService } from "../services";

@Controller("/auth")
export class AuthenticationController {
  constructor(private readonly authenticationService: AuthenticationService) {}

  @ContentType("json")
  @Post("/student")
  student(@BodyParams("email") email: string) {
    return this.authenticationService.authenticateRegistration(email);
  }

  @ContentType("json")
  @Post("/user")
  admin(@BodyParams("email") email: string) {
    return this.authenticationService.authenticateUser(email);
  }
}
