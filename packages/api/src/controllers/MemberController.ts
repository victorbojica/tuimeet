import fs from "fs";
import { Controller, Get, ContentType, Context, Post, BodyParams, PathParams, Res, Req } from "@tsed/common";
import { MultipartFile } from "@tsed/multipartfiles";
import { Authorize } from "@tsed/passport";
import { Request, Response } from "express";

import { MemberService } from "../services";
import { UserScheduleItem } from "../domain";
import { StorageService } from "../services/StorageService";

@Controller("/member")
@Authorize("member")
export class MemberController {
  constructor(protected readonly memberService: MemberService, protected readonly storageService: StorageService) {}

  @ContentType("json")
  @Get("/waitinglist")
  async listWaitinglist(@Context("user") user: any) {
    return await this.memberService.listWaitinglist(user.id);
  }

  @ContentType("json")
  @Get("/meeting")
  async listMeetings(@Context("user") user: any) {
    return await this.memberService.listMeetings(user.id);
  }

  @ContentType("json")
  @Post("/pipeline/:registrationId/schedule-meeting")
  async scheduleMeeting(
    @PathParams("registrationId") registrationId: string,
    @BodyParams("estimatedStartTime") _estimatedStartTime: string,
    @BodyParams("estimatedDuration") estimatedDuration: number
  ) {
    const estimatedStartTime = new Date();
    estimatedStartTime.setHours(parseInt(_estimatedStartTime.split(":")[0], 10));
    estimatedStartTime.setMinutes(parseInt(_estimatedStartTime.split(":")[1], 10));
    estimatedStartTime.setSeconds(0);

    return await this.memberService.scheduleMeeting(registrationId, {
      schedule: { estimatedStartTime, estimatedDuration },
    });
  }

  @ContentType("json")
  @Post("/pipeline/:registrationId/reschedule-meeting")
  async rescheduleMeeting(
    @PathParams("registrationId") registrationId: string,
    @BodyParams("estimatedStartTime") _estimatedStartTime: string,
    @BodyParams("estimatedDuration") estimatedDuration: number,
    @BodyParams("reason") reason: string
  ) {
    const estimatedStartTime = new Date();
    estimatedStartTime.setHours(parseInt(_estimatedStartTime.split(":")[0], 10));
    estimatedStartTime.setMinutes(parseInt(_estimatedStartTime.split(":")[1], 10));
    estimatedStartTime.setSeconds(0);

    return await this.memberService.rescheduleMeeting(registrationId, {
      schedule: { estimatedStartTime, estimatedDuration },
      reason,
    });
  }

  @ContentType("json")
  @Post("/pipeline/:registrationId/join-meeting")
  async joinMeeting(@PathParams("registrationId") registrationId: string) {
    return await this.memberService.joinMeeting(registrationId);
  }

  @ContentType("json")
  @Post("/pipeline/:registrationId/complete-meeting")
  async completeMeeting(
    @PathParams("registrationId") registrationId: string,
    @BodyParams("notes") notes: string,
    @MultipartFile("recording") recording: Express.Multer.File
  ) {
    let recodingPath;
    if (recording) recodingPath = await this.storageService.saveUpload(`recording-${registrationId}`, recording);

    return await this.memberService.completeMeeting(registrationId, { notes, recodingPath });
  }

  @ContentType("json")
  @Post("/pipeline/:registrationId/cancel-meeting")
  async cancelMeeting(@PathParams("registrationId") registrationId: string, @BodyParams("reason") reason: string) {
    return await this.memberService.cancelMeeting(registrationId, { reason });
  }

  @ContentType("json")
  @Post("/pipeline/:registrationId/cancel")
  async cancelRegistration(@PathParams("registrationId") registrationId: string, @BodyParams("reason") reason: string) {
    return await this.memberService.cancelRegistration(registrationId, { reason });
  }

  @ContentType("application/octet-stream")
  @Get("/pipeline/:registrationId/recording")
  async getRecording(
    @PathParams("registrationId") registrationId: string,
    @Req() request: Request,
    @Res() response: Response
  ) {
    const recording = await this.memberService.getRecording(registrationId);
    if (!recording) {
      return "Not Found";
    } else {
      let fileName: any = recording.path.split("/");
      fileName = fileName[fileName.length - 1];

      response.status(200);
      response.setHeader("Content-Type", "application/octet-stream");
      response.setHeader("Content-Disposition", `attachment; filename=${fileName}`);

      fs.createReadStream(recording.path).pipe(response);
    }
  }

  @ContentType("json")
  @Get("/registration/:id")
  async getRegistrationInfo(@PathParams("id") id: string) {
    return await this.memberService.registrationInfo(id);
  }

  @ContentType("json")
  @Get("/profile")
  async getProfile(@Context("user") user: any) {
    return await this.memberService.getProfile(user.id);
  }

  @ContentType("json")
  @Post("/profile")
  async updateProfile(
    @Context("user") user: any,
    @BodyParams("schedule") schedule: Array<UserScheduleItem>,
    @BodyParams("meetingDuration") meetingDuration: number
  ) {
    return await this.memberService.updateProfile(user.id, { schedule, meetingDuration });
  }
}
