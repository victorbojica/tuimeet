require("ts-node/register");

const dotenv = require("dotenv");
const stage = process.env.NODE_ENV || "development";
const env = dotenv.config({ path: `./.env.${stage}` });

if (env.error) {
  console.error(env.error);
  throw new Error("Can't load .env file");
}

const defaultConfig = {
  client: "mysql",
  connection: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
  },
  migrations: {
    tableName: "migrations",
    directory: "./scripts/db/migrations",
    extension: "ts",
  },
  seeds: {
    directory: "./scripts/db/seeds",
    extension: "ts",
  },
};

module.exports = {
  development: {
    ...defaultConfig,
  },
  production: {
    ...defaultConfig,
  },
};
