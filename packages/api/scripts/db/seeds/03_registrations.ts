import * as Knex from "knex";

import { data as organizationData } from "./01_organizations";
import { RegistrationStatus } from "../../../src/domain/registrations.types";

export const data = [
  {
    id: "474a2a40-c93a-45f1-a070-bf4ebac60089",
    refId: "789735fc-c548-4571-8f0f-33fa77101c05",
    firstName: "Regan",
    lastName: "Newiss",
    email: "rnewiss0@google.es",
    status: RegistrationStatus.pending,
    startedAt: null,
    endedAt: null,
    organizationId: organizationData[0].id,
  },
  {
    id: "ed3587b8-78a5-4d1c-9a3b-23254dd52476",
    refId: "c3c0a6ac-5cf5-40a4-8db7-e95804c70608",
    firstName: "Moyra",
    lastName: "Collumbell",
    email: "mcollumbell1@jugem.jp",
    status: RegistrationStatus.pending,
    startedAt: null,
    endedAt: null,
    organizationId: organizationData[0].id,
  },
  {
    id: "42af72e6-7794-4b8d-9426-cd9967f3f8e2",
    refId: "61ae963c-cd0c-4e0c-8981-bedc310ac615",
    firstName: "Andre",
    lastName: "Stronough",
    email: "astronough2@domainmarket.com",
    status: RegistrationStatus.pending,
    startedAt: null,
    endedAt: null,
    organizationId: organizationData[0].id,
  },
  {
    id: "b1b06228-12db-485a-a741-8f99ea35108d",
    refId: "18766495-5f0b-4675-b124-8babd806e201",
    firstName: "Emelina",
    lastName: "Totton",
    email: "etotton3@economist.com",
    status: RegistrationStatus.pending,
    startedAt: null,
    endedAt: null,
    organizationId: organizationData[0].id,
  },
  {
    id: "0a851c47-33c1-4bb6-aea4-41ad54ce95dd",
    refId: "d4fccb81-df20-4819-a192-08e8092e94d5",
    firstName: "Jolie",
    lastName: "Zanazzi",
    email: "jzanazzi4@t-online.de",
    status: RegistrationStatus.pending,
    startedAt: null,
    endedAt: null,
    organizationId: organizationData[0].id,
  },
];

export async function seed(knex: Knex): Promise<any> {
  // Deletes ALL existing entries
  await knex("registrations").del();
  return await knex("registrations").insert(data);
}
