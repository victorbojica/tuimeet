import * as Knex from "knex";

export const data = [
  {
    id: "b1423b1d-deb6-4b7a-a0e6-804625e29e01",
    name: "Facultatea de Automatica si Calculatoare",
    meetingDuration: 20,
    meetingMaxReschedules: 3,
    logoURL: "https://ac.tuiasi.ro/wp-content/uploads/2019/05/cropped-logo_ac_iasi.png",
  },
];

export async function seed(knex: Knex): Promise<any> {
  // Deletes ALL existing entries
  await knex("organizations").del();
  await knex("organizations").insert(data);
}
