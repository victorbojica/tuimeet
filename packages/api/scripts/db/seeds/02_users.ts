import * as Knex from "knex";

import { data as organizationData } from "./01_organizations";
import { UserRole } from "../../../src/domain/user.types";

export const data = [
  {
    id: "53609a3b-55b0-48b6-842d-5dfc3c00dcce",
    role: UserRole.administrator,
    refId: null,
    firstName: "Victor",
    lastName: "Administrator",
    email: "victorbojica+admin@gmail.com",
    avatarURL: null,
    enabled: true,
    organizationId: organizationData[0].id,
  },
  {
    id: "a4a56f1c-c7e3-434a-89fb-293cc7b270fc",
    role: UserRole.member,
    refId: null,
    firstName: "Victor",
    lastName: "Member",
    email: "victorbojica+member@gmail.com",
    avatarURL: null,
    enabled: true,
    organizationId: organizationData[0].id,
  },
];

export async function seed(knex: Knex): Promise<any> {
  // Deletes ALL existing entries
  await knex("users").del();
  return await knex("users").insert(data);
}
