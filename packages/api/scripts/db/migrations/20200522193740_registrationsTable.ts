import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  await knex.schema.createTable("registrations", (table) => {
    table.string("id", 36).primary();

    table.string("refId").notNullable();
    table.string("firstName").notNullable();
    table.string("lastName").notNullable();
    table.string("email").notNullable();
    table.string("status").notNullable();
    table.timestamp("startedAt").nullable();
    table.timestamp("endedAt").nullable();

    table.string("organizationId", 36).notNullable().references("id").inTable("organizations");

    table.timestamp("createdAt").notNullable().defaultTo(knex.fn.now());
    table.timestamp("updatedAt").notNullable().defaultTo(knex.raw("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"));
    table.timestamp("deletedAt").nullable();
  });
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.dropTable("registrations");
}
