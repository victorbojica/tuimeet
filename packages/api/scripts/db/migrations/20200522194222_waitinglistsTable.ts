import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  await knex.schema.createTable("waitinglists", (table) => {
    table.string("id", 36).primary();

    table.string("status").notNullable();
    table.timestamp("joinedAt").notNullable();
    table.timestamp("scheduledAt").nullable();

    table.string("userId", 36).notNullable().references("id").inTable("users");
    table.string("registrationId", 36).notNullable().references("id").inTable("registrations");
    table.string("organizationId", 36).notNullable().references("id").inTable("organizations");

    table.timestamp("createdAt").notNullable().defaultTo(knex.fn.now());
    table.timestamp("updatedAt").notNullable().defaultTo(knex.raw("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"));
    table.timestamp("deletedAt").nullable();
  });
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.dropTable("waitinglists");
}
