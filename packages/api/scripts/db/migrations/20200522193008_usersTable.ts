import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  return await knex.schema.createTable("users", (table) => {
    table.string("id", 36).primary();

    table.string("role").notNullable();
    table.string("refId").nullable();
    table.string("firstName").notNullable();
    table.string("lastName").notNullable();
    table.string("email").notNullable();
    table.string("avatarURL").nullable();
    table.json("schedule").notNullable().defaultTo([]);
    table.boolean("enabled").notNullable().defaultTo(true);

    table.string("organizationId", 36).notNullable().references("id").inTable("organizations");

    table.timestamp("createdAt").notNullable().defaultTo(knex.fn.now());
    table.timestamp("updatedAt").notNullable().defaultTo(knex.raw("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"));
    table.timestamp("deletedAt").nullable();
  });
}

export async function down(knex: Knex): Promise<any> {
  return await knex.schema.dropTable("users");
}
