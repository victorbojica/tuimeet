import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  await knex.schema.table("users", (table) => {
    table.integer("meetingDuration").notNullable().defaultTo(20);
  });
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.table("users", (table) => {
    table.dropColumn("meetingDuration");
  });
}
