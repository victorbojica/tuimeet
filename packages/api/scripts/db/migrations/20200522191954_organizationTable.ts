import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  return await knex.schema.createTable("organizations", (table) => {
    table.string("id", 36).primary();

    table.string("name").notNullable();
    table.string("logoURL").notNullable();
    table.integer("meetingDuration").notNullable().defaultTo(15);
    table.integer("meetingMaxReschedules").notNullable().defaultTo(3);

    table.timestamp("createdAt").notNullable().defaultTo(knex.fn.now());
    table.timestamp("updatedAt").notNullable().defaultTo(knex.raw("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"));
    table.timestamp("deletedAt").nullable();
  });
}

export async function down(knex: Knex): Promise<any> {
  return await knex.schema.dropTable("organizations");
}
