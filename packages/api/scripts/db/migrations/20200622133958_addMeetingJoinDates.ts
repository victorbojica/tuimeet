import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  await knex.schema.table("meetings", (table) => {
    table.timestamp("userJoinedAt").nullable();
    table.timestamp("registrationJoinedAt").nullable();
  });
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.table("meetings", (table) => {
    table.dropColumn("userJoinedAt");
    table.dropColumn("registrationJoinedAt");
  });
}
