import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  await knex.schema.createTable("meetings", (table) => {
    table.string("id", 36).notNullable().primary();

    table.string("url").notNullable();
    table.string("status").notNullable();
    table.timestamp("estimatedStartTime").notNullable();
    table.integer("estimatedDuration").notNullable();
    table.text("notes").nullable();
    table.string("endReason").nullable();

    table.timestamp("startedAt").nullable();
    table.timestamp("endedAt").nullable();

    table.string("userId", 36).notNullable().references("id").inTable("users");
    table.string("waitinglistId", 36).notNullable().references("id").inTable("waitinglists");
    table.string("registrationId", 36).notNullable().references("id").inTable("registrations");
    table.string("organizationId", 36).notNullable().references("id").inTable("organizations");
    table.string("recordingId", 36).nullable().references("id").inTable("recordings");

    table.timestamp("createdAt").notNullable().defaultTo(knex.fn.now());
    table.timestamp("updatedAt").notNullable().defaultTo(knex.raw("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"));
    table.timestamp("deletedAt").nullable();
  });
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.dropTable("meetings");
}
