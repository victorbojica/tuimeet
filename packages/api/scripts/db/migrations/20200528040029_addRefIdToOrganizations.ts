import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  await knex.schema.table("organizations", (table) => {
    table.string("refId").notNullable();
  });
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.table("organizations", (table) => {
    table.dropColumn("refId");
  });
}
