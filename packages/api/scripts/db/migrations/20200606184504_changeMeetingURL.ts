import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  await knex.schema.table("meetings", (table) => {
    table.dropColumn("url");

    table.text("userURL").nullable().defaultTo(null);
    table.text("registrationURL").nullable().defaultTo(null);
  });
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.table("registrations", (table) => {
    table.dropColumn("userURL");
    table.dropColumn("registrationURL");
    table.string("url").notNullable();
  });
}
