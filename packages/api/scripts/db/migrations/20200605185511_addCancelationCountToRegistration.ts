import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  await knex.schema.table("registrations", (table) => {
    table.integer("cancellationCount").notNullable().defaultTo(0);
  });
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.table("registrations", (table) => {
    table.dropColumn("cancellationCount");
  });
}
