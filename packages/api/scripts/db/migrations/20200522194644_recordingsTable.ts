import * as Knex from "knex";

export async function up(knex: Knex): Promise<any> {
  await knex.schema.createTable("recordings", (table) => {
    table.string("id", 36).notNullable().primary();

    table.string("path").notNullable();

    table.string("organizationId", 36).notNullable().references("id").inTable("organizations");

    table.timestamp("createdAt").notNullable().defaultTo(knex.fn.now());
    table.timestamp("updatedAt").notNullable().defaultTo(knex.raw("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"));
    table.timestamp("deletedAt").nullable();
  });
}

export async function down(knex: Knex): Promise<any> {
  await knex.schema.dropTable("recordings");
}
