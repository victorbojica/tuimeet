import React from "react";
import { I18nProvider as LinguiI18nProvider } from "@lingui/react";
import { inject, observer } from "mobx-react";

export const I18nProvider = inject("globalStore")(
  observer(({ globalStore, children }) => {
    return <LinguiI18nProvider i18n={globalStore.i18n}>{children}</LinguiI18nProvider>;
  })
);
