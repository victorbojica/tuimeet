import React, { useState, useEffect } from "react";
import { inject, observer } from "mobx-react";
import { TopBar, Frame, Loading } from "@shopify/polaris";
import { t } from "@lingui/macro";

import { FlashMessageBanner } from "./FlashMessageBanner";
import { LanguageChange } from "./LanguageSelect";

let interval;
export const Layout = inject(
  "studentStore",
  "globalStore"
)(
  observer(({ children, studentStore, globalStore }) => {
    const [isLoading] = useState(false);
    const [userMenuActive, setUserMenuActive] = useState(false);

    useEffect(() => {
      const method = async () => {
        await studentStore.fetchProfile();

        clearInterval(interval);
        interval = setInterval(async () => {
          await studentStore.fetchProfile();
        }, 10 * 1000);
      };
      method();
    }, [studentStore]);

    const toggleUserMenuActive = () => setUserMenuActive(!userMenuActive);
    const userMenuActions = [
      {
        items: [{ content: globalStore.i18n._(t`Log Out`), url: "/logout" }],
      },
    ];

    const userMenuMarkup = (
      <>
        <div style={{ marginRight: "10px" }}>
          <LanguageChange />
        </div>
        <TopBar.UserMenu
          actions={userMenuActions}
          name={`${studentStore.profile.firstName} ${studentStore.profile.lastName}`}
          detail={globalStore.i18n._(t`Student`)}
          initials={studentStore.profile.firstName[0]}
          open={userMenuActive}
          onToggle={toggleUserMenuActive}
        />
      </>
    );

    const topBarMarkup = <TopBar userMenu={userMenuMarkup} />;

    const loadingMarkup = isLoading ? <Loading /> : null;

    return (
      <Frame topBar={topBarMarkup}>
        {loadingMarkup}
        <FlashMessageBanner />
        {children}
      </Frame>
    );
  })
);
