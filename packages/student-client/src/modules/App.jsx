import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from "mobx-react";
import { AppProvider, ThemeProvider } from "@shopify/polaris";
import enTranslations from "@shopify/polaris/locales/en.json";

import "@shopify/polaris/styles.css";

import globalStore from "./common/GlobalStore";
import authStore from "./auth/AuthStore";
import studentStore from "./student/StudentStore";

import { LoginPage, Logout, Authorized } from "./auth/components";
import { I18nProvider } from "./common/components";

import { Student } from "./student";

const theme = {
  colors: {
    topBar: {
      background: "#1f458d",
    },
  },
  logo: {
    width: 35,
    topBarSource: "https://ac.tuiasi.ro/wp-content/uploads/2019/05/cropped-logo_ac_iasi.png",
    contextualSaveBarSource: "https://ac.tuiasi.ro/wp-content/uploads/2019/05/cropped-logo_ac_iasi.png",
    url: "#",
    accessibilityLabel: "Jaded Pixel",
  },
};

const App = () => (
  <AppProvider i18n={enTranslations}>
    <ThemeProvider theme={theme}>
      <Provider globalStore={globalStore} authStore={authStore} studentStore={studentStore}>
        <I18nProvider>
          <Router>
            <Switch>
              <Route exact path="/">
                <Authorized>
                  <Student />
                </Authorized>
              </Route>

              <Route path="/login">
                <LoginPage />
              </Route>

              <Route path="/logout">
                <Logout />
              </Route>
            </Switch>
          </Router>
        </I18nProvider>
      </Provider>
    </ThemeProvider>
  </AppProvider>
);

export default App;
