import React from "react";
import { inject, observer } from "mobx-react";
import { Trans } from "@lingui/macro";
import * as DateFns from "date-fns";

const NotJoinedInfo = () => (
  <>
    <Trans>
      <p>
        You haven't joined any teacher's waiting list. Please join one from below. <br />
        Make sure take you will be available for the next 20-60 minutes.
      </p>
      <br />
    </Trans>
  </>
);

const JoinedInfo = ({ memberFullName, waitTime }) => (
  <Trans>
    <p>
      You have joined the waitinglist for <b>{memberFullName}</b>.
    </p>
    <br />
    <p>
      Currently the waiting time is <b>{waitTime ? `${waitTime} minutes` : "not available"}</b>
    </p>
    <br />
    <p>
      You will be notified with <b>10 minutes in advance</b> of the meeting
    </p>
    <br />
    <p>
      If for some reason you can't make it, you can cancel and reschedule the meeting. You can do this only{" "}
      <b>3 times</b>.
    </p>
    <br />
  </Trans>
);

const MeetingScheduledInfo = ({ meetingTime }) => {
  return (
    <Trans>
      <p>
        The meeting has been scheduled for: <b>{meetingTime ? DateFns.format(meetingTime, "HH:mm") : "-"}</b>.
      </p>
      <br />
    </Trans>
  );
};

const MeetingStartInfo = () => {
  return (
    <Trans>
      <p>The meeting is about to start. Please join the meeting following the link below.</p>
      <br />
    </Trans>
  );
};

const MeetingHappeningInfo = () => {
  return (
    <Trans>
      <p>The meeting has started. Please join the meeting following the link below.</p>
      <br />
    </Trans>
  );
};

const MeetingCanceledInfo = ({ memberFullName }) => {
  return (
    <Trans>
      <p>
        The meeting didn't take place because you have been <b>rescheduled</b> to <b>{memberFullName}</b>.
      </p>
      <br />
      <p>
        Currently the waiting time is about <b>20 minutes</b>
      </p>
      <br />
      <p>
        You will be notified with <b>10 minutes in advance</b> of the meeting
      </p>
      <br />
    </Trans>
  );
};

const MeetingEndedInfo = ({ memberFullName }) => {
  return (
    <Trans>
      <p>
        The meeting is finished. Please get in touch with <b>{memberFullName}</b> for details about the next steps.
      </p>
      <br />
    </Trans>
  );
};

const UnknownStatus = () => {
  return (
    <Trans>
      <p>
        Something happened and we are working on it. Please get in touch with EMAIL_ADDRESS_HERE if the problem
        persists.
      </p>
      <br />
    </Trans>
  );
};

export const StatusInfo = inject("studentStore")(
  observer(({ studentStore }) => {
    const { status, isAboutToHappen, ...props } = studentStore.profile;

    return (
      <>
        {status === "pending" && <NotJoinedInfo {...props} />}
        {status === "waitinglist" && <JoinedInfo {...props} />}
        {status === "meeting" && <MeetingScheduledInfo {...props} />}
        {status === "meeting" && isAboutToHappen && <MeetingStartInfo {...props} />}
        {status === "happening" && <MeetingHappeningInfo {...props} />}
        {status === "complete" && <MeetingEndedInfo {...props} />}
        {status === "canceled" && <MeetingCanceledInfo {...props} />}
        {status === "unknown" && <UnknownStatus {...props} />}
      </>
    );
  })
);
