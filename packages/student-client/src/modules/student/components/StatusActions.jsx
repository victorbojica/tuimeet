import React from "react";
import { Button, Stack } from "@shopify/polaris";
import { inject, observer } from "mobx-react";
import { Trans } from "@lingui/macro";

export const StatusActions = inject("studentStore")(
  observer(({ studentStore }) => {
    const status = studentStore.profile.status;

    const handleJoinWaitinglist = async () => await studentStore.joinWaitinglist();
    const handleCancelWaitinglist = async () => await studentStore.cancelWaitinglist();
    const handleGoToMeeting = async () => await studentStore.joinMeeting();

    const isAboutToHappen = false;

    return (
      <>
        <Stack>
          {status === "pending" && (
            <Button primary onClick={handleJoinWaitinglist}>
              <Trans>Join Meeting</Trans>
            </Button>
          )}

          {status === "waitinglist" && (
            <Button primary onClick={handleCancelWaitinglist}>
              <Trans>Cancel Meeting</Trans>
            </Button>
          )}

          {(status === "meeting" && isAboutToHappen) ||
            (status === "happening" && (
              <Button primary onClick={handleGoToMeeting}>
                <Trans>Go To Meeting</Trans>
              </Button>
            ))}
        </Stack>
      </>
    );
  })
);
