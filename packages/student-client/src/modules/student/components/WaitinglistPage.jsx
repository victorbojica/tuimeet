import React from "react";
import { Page, Card } from "@shopify/polaris";
import { StatusInfo } from "./StatusInfo";
import { StatusActions } from "./StatusActions";
import { t } from "@lingui/macro";
import { inject, observer } from "mobx-react";

export const WaitinglistPage = inject("globalStore")(
  observer(({ globalStore }) => {
    return (
      <Page fullWidth>
        <Card sectioned title={globalStore.i18n._(t`Status`)}>
          <StatusInfo />
          <StatusActions />
        </Card>
      </Page>
    );
  })
);
