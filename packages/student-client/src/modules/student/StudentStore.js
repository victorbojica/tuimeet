import { action, observable } from "mobx";
import { t } from "@lingui/macro";
import * as Student from "../../lib/services/Student";
import globalStore from "../common/GlobalStore";

class StudentStore {
  @observable profile = {
    firstName: "",
    lastName: "",
    email: "",
    status: "",
    memberFullName: "",
    waitTime: null,
    meetingTime: null,
    meetingURL: null,
    meetingIsAboutToHappen: false,
  };

  @action
  async fetchProfile() {
    let profile;
    try {
      profile = await Student.fetchProfile();
    } catch (e) {
      throw e;
    }

    profile.meetingTime = profile.meetingTime ? new Date(profile.meetingTime) : null;
    this.profile = profile;
  }

  @action
  async joinWaitinglist() {
    try {
      await Student.joinWaitinglist();
      await this.fetchProfile();

      globalStore.setSuccessMessage(globalStore.i18n._(t`Joined waitinglist`));
    } catch (e) {
      globalStore.setErrorMessage(e);
    }
  }

  @action
  async cancelWaitinglist() {
    try {
      await Student.cancelWaitinglist();
      await this.fetchProfile();

      globalStore.setInfoMessage(globalStore.i18n._(t`Canceled waitinglist`));
    } catch (e) {
      globalStore.setErrorMessage(e);
    }
  }

  @action
  async joinMeeting() {
    try {
      if (!this.profile.meetingURL) {
        await Student.joinMeeting();
        await this.fetchProfile();
      }

      globalStore.setInfoMessage(globalStore.i18n._(t`Canceled waitinglist`));

      window.open(this.profile.meetingURL);
    } catch (e) {
      globalStore.setErrorMessage(e);
    }
  }
}

const studentStore = new StudentStore();
export default studentStore;
