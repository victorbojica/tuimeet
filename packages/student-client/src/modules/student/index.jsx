import React from "react";
import { Layout } from "../common/components";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { WaitinglistPage } from "./components";

export const Student = () => {
  const { path } = useRouteMatch();
  return (
    <Layout>
      <Switch>
        <Route exact path={`${path}/`}>
          <WaitinglistPage />
        </Route>
      </Switch>
    </Layout>
  );
};
