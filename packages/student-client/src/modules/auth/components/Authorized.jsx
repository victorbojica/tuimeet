import { inject, observer } from "mobx-react";
import { useHistory } from "react-router-dom";

export const Authorized = inject("authStore")(
  observer(({ authStore, children }) => {
    const history = useHistory();

    if (authStore.isAuthenticated()) return children;
    history.push("/login");
    return null;
  })
);
