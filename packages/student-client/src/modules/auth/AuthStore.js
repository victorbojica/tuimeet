import { action } from "mobx";
import store from "store";
import jwt from "jsonwebtoken";
import { t } from "@lingui/macro";

import * as Auth from "../../lib/services/Auth";
import globalStore from "../common/GlobalStore";

let interval;
class AuthStore {
  @action
  async authenticate(email, password) {
    const token = await Auth.authenticate(email, password);
    if (token) {
      this.clearAuthentication();

      store.set("accessToken", token);

      interval = setInterval(() => {
        if (!this.isAuthenticated()) this.clearAuthentication();
      }, 10 * 1000);

      return true;
    } else {
      globalStore.setErrorMessage(globalStore.i18n._(t`User not found`));

      this.clearAuthentication();

      return false;
    }
  }

  clearAuthentication() {
    store.set("accessToken", "");

    clearInterval(interval);
  }

  isAuthenticated() {
    const token = store.get("accessToken");
    const payload = jwt.decode(token);
    if (!payload) return false;

    return !!token && new Date(payload.exp * 1000) > new Date();
  }
}

const authStore = new AuthStore();
export default authStore;
