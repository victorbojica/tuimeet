import API from "./API";

export const fetchProfile = () => API.get("student/profile");
export const joinWaitinglist = () => API.post("student/join");
export const cancelWaitinglist = () => API.post("student/cancel");
export const joinMeeting = () => API.post("student/join-meeting");
