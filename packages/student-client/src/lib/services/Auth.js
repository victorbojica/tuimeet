import API from "./API";

export const authenticate = (email, password) => API.post("auth/student", { email, password });
