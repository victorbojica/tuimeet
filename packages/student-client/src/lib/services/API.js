import store from "store";
import axios from "axios";

// eslint-disable-next-line no-undef
export const API_URL =
  process.env.REACT_APP_API_HOST ||
  (window && window.config && window.config.API_HOST) ||
  "http://localhost:3103/api/v1/";

const errorHandler = (err) => {
  let e = null;

  if (err.response) {
    if (err.response.data && err.response.data.error && err.response.data.error.message) {
      e = err.response.data.error.message;
    } else {
      e = "Something happened";
    }
  } else {
    e = err;
  }

  return Promise.reject(e);
};

const successHandler = (response) => {
  if (response.data) return response.data;
  return response.data;
};

const setAuthTokenHandler = (config) => {
  let originalRequest = config;

  let accessToken = store.get("accessToken");
  if (!accessToken) return originalRequest;

  originalRequest.headers.Authorization = `Bearer ${accessToken}`;

  return originalRequest;
};

const Request = axios;

Request.defaults.baseURL = API_URL;

Request.defaults.headers.common["Content-Type"] = "application/json";

Request.interceptors.request.use(setAuthTokenHandler, errorHandler);
Request.interceptors.response.use(successHandler, errorHandler);

export default Request;
