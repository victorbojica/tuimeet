module.exports = {
  locales: ["en", "ro"],
  catalogs: [{ path: "./src/lib/locale/{locale}/messages", include: ["./src"], exclude: ["**/node_modules/**"] }],
  extractBabelOptions: {
    plugins: [["@babel/plugin-proposal-decorators", { legacy: true }]],
  },
  rootDir: "./src",
};
