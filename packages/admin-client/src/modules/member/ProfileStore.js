import { action, observable } from "mobx";
import { map } from "lodash";
import { t } from "@lingui/macro";

import * as Profile from "../../lib/services/Profile";
import globalStore from "../common/GlobalStore";

export class ProfileStore {
  @observable profile = {
    firstName: null,
    lastName: null,
    fullName: null,
    email: null,
    meetingDuration: 0,
    schedule: [],
  };

  @action async fetchProfile() {
    try {
      const result = await Profile.fecthProfle();
      result.fullName = `${result.firstName} ${result.lastName}`;
      result.schedule = map(JSON.parse(result.schedule), (row) => [row.startTime, row.endTime]);
      this.profile = result;
    } catch (e) {
      globalStore.setErrorMessage(e);
    }
  }

  @action async updateProfile({ schedule, meetingDuration }) {
    try {
      const _schedule = map(schedule, (row) => ({ startTime: row[0], endTime: row[1] }));
      await Profile.updateProfile({ schedule: _schedule, meetingDuration });
      await this.fetchProfile();
      globalStore.setSuccessMessage(globalStore.i18n._(t`Updated settings`));
    } catch (e) {
      globalStore.setErrorMessage(e);
    }
  }
}

const store = new ProfileStore();
export default store;
