import { action, computed, observable } from "mobx";
import { map, cloneDeep, reduce } from "lodash";
import * as DateUtils from "date-fns";
import { t } from "@lingui/macro";

import * as RegistrationPipeline from "../../lib/services/RegistrationPipeline";
import * as Registration from "../../lib/services/Registration";
import { enrichRegistrationPipeline } from "../../domain";
import globalStore from "../common/GlobalStore";
import profileStore from "./ProfileStore";

const currentModel = {
  registration: { id: null, firstName: null, lastName: null, email: null, fullName: null },
  meeting: {
    userURL: null,
    estimatedStartTime: null,
    estimatedDuration: null,
    startedAt: null,
    notes: null,
    recordingId: null,
    startingInMinutes: () => null,
    startedForMinutes: () => null,
  },
  waitinglist: {},
  user: {},
  referenceRegistration: {},
};

class RegistrationPipelineStore {
  @observable waitinglists = [];
  @observable currentRegistrationId = null;
  @observable current = cloneDeep(currentModel);

  @computed get meetingEvents() {
    return reduce(
      this.waitinglists,
      (acc, { status, registration, meeting }) => {
        if (!(status === "meeting" || status === "happening" || status === "complete")) return acc;

        return [
          ...acc,
          {
            id: registration.id,
            title: registration.fullName,
            start: meeting.estimatedStartTime,
            end: DateUtils.addMinutes(
              meeting.estimatedStartTime,
              meeting.estimatedDuration || profileStore.profile.meetingDuration
            ),
          },
        ];
      },
      []
    );
  }

  @action setCurrentRegistrationId(id) {
    this.currentRegistrationId = id;
  }

  @action async scheduleMeeting(id, { estimatedStartTime, estimatedDuration }) {
    try {
      await RegistrationPipeline.scheduleMeeting(id, { estimatedStartTime, estimatedDuration });
      await this.fetchWaitinglists();

      globalStore.setSuccessMessage(globalStore.i18n._(t`Meeting scheduled. The student will be notified.`));
    } catch (e) {
      globalStore.setErrorMessage(e);
    }

    this.setCurrentRegistrationId(null);
  }

  @action async rescheduleMeeting(id, { estimatedStartTime, estimatedDuration, reason }) {
    try {
      await RegistrationPipeline.rescheduleMeeting(id, { estimatedStartTime, estimatedDuration, reason });
      await this.fetchRegistration(id);

      globalStore.setSuccessMessage(globalStore.i18n._(t`Meeting rescheduled. The student will be notified.`));
    } catch (e) {
      globalStore.setErrorMessage(e);
    }
    this.setCurrentRegistrationId(null);
  }

  @action async cancelMeeting(id, { reason }) {
    try {
      await RegistrationPipeline.cancelMeeting(id, { reason });
      await this.fetchRegistration(id);

      globalStore.setInfoMessage(globalStore.i18n._(t`Meeting canceled. The student will be notified.`));
    } catch (e) {
      globalStore.setErrorMessage(e);
    }

    this.setCurrentRegistrationId(null);
  }

  @action async joinMeeting(id) {
    try {
      if (!this.current.meeting.userURL) {
        await RegistrationPipeline.joinMeeting(id);
        await this.fetchRegistration(id);
      }

      globalStore.setSuccessMessage(globalStore.i18n._(t`Started meeting`));

      window.open(this.current.meeting.userURL);
    } catch (e) {
      globalStore.setErrorMessage(e);
    }
  }

  @action async completeMeeting(id, { notes, recording }) {
    try {
      await RegistrationPipeline.completeMeeting(id, { notes, recording });
      await this.fetchRegistration(id);

      globalStore.setSuccessMessage(globalStore.i18n._(t`Meeting completed. The registration will now be completed`));
    } catch (e) {
      globalStore.setErrorMessage(e);
    }
  }

  @action
  async fetchWaitinglists() {
    try {
      const result = await RegistrationPipeline.listWaitinglist();
      this.waitinglists = map(result, enrichRegistrationPipeline);
    } catch (e) {
      globalStore.setErrorMessage(e);
    }
  }

  @action async fetchRegistration(id) {
    this.current = cloneDeep(currentModel);

    try {
      let result = await Registration.getRegistration(id);
      this.current = enrichRegistrationPipeline(result);
    } catch (e) {
      globalStore.setErrorMessage(e);
    }
  }
}

const store = new RegistrationPipelineStore();
export default store;
