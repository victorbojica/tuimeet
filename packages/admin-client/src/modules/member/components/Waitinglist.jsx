import { Avatar, ResourceItem, ResourceList, TextStyle, EmptyState } from "@shopify/polaris";
import React, { useEffect } from "react";
import { inject, observer } from "mobx-react";

import { WaitinglistStatusBadge } from "./WaitinglistStatusBadge";

import emptyStateImage from "../../../assets/empty-state.png";
import { t, Trans } from "@lingui/macro";

let interval;
export const Waitinglist = inject(
  "registrationPipelineStore",
  "globalStore"
)(
  observer(({ registrationPipelineStore, handleScheduleModalToggle, globalStore }) => {
    useEffect(() => {
      const method = async () => {
        await registrationPipelineStore.fetchWaitinglists();

        clearInterval(interval);
        interval = setInterval(async () => {
          await registrationPipelineStore.fetchWaitinglists();
        }, 4000);
      };
      method();
    }, [registrationPipelineStore]);

    return registrationPipelineStore.waitinglists.length ? (
      <ResourceList
        items={registrationPipelineStore.waitinglists}
        renderItem={({ registration, waitinglist, meeting, status }) => {
          const media = <Avatar customer size="medium" name={registration.fullName} />;

          const shortcutActions = [];
          if (status === "waitinglist") {
            shortcutActions.push({
              content: globalStore.i18n._(t`Schedule`),
              onAction: () => {
                registrationPipelineStore.setCurrentRegistrationId(registration.id);
                handleScheduleModalToggle();
              },
            });
          }

          if (status === "meeting" || status === "happening") {
            shortcutActions.push({
              content: globalStore.i18n._(t`Details`),
              url: `/member/meet/${registration.id}`,
            });
          }

          let content = "";
          if (status === "waitinglist")
            content = globalStore.i18n._(t`Waiting for: ${waitinglist.minutesSinceJoined()}`);
          else if (status === "meeting") content = globalStore.i18n._(t`Starting in: ${meeting.startingInMinutes()}`);
          else if (status === "happening")
            content = globalStore.i18n._(t`Time elapsed: ${meeting.startedForMinutes()}`);

          return (
            <ResourceItem
              id={registration.id}
              url={`/member/registration/${registration.id}`}
              media={media}
              verticalAlignment={"center"}
              shortcutActions={shortcutActions}
              persistActions
            >
              <h3>
                <TextStyle variation="strong">{registration.fullName}</TextStyle>
                <WaitinglistStatusBadge status={status} />
              </h3>
              <div>
                {content}, {registration.email}
              </div>
            </ResourceItem>
          );
        }}
      />
    ) : (
      <div style={{ marginTop: "-2rem" }}>
        <EmptyState centeredLayout image={emptyStateImage} heading={globalStore.i18n._(t`You're done`)}>
          <Trans>No more students in the waitinglist</Trans>
        </EmptyState>
      </div>
    );
  })
);
