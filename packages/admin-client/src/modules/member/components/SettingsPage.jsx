import React, { useEffect, useState } from "react";
import { filter, map } from "lodash";
import { Page, Card, Stack, Tag, TextField, Button, FormLayout, Form } from "@shopify/polaris";
import { inject, observer } from "mobx-react";
import { t } from "@lingui/macro";

export const SettingsPage = inject(
  "profileStore",
  "globalStore"
)(
  observer(({ profileStore, globalStore }) => {
    const [dateRange, setDateRange] = useState([0, 0]);
    const handleStartChange = (newValue) => {
      return setDateRange([newValue, dateRange[1]]);
    };
    const handleEndChange = (newValue) => {
      return setDateRange([dateRange[0], newValue]);
    };

    const [schedule, setSchedule] = useState([]);
    const handleScheduleAdd = () => {
      setSchedule([...schedule, dateRange]);
      setDateRange([0, 0]);
    };
    const handleScheduleRemove = (index) => {
      setSchedule(filter(schedule, (r, i) => i !== index));
    };

    const [meetingDuration, setMeetingDuration] = useState(0);
    const handleMeetingDurationChange = (value) => setMeetingDuration(value);

    const handleProfileSubmit = async () => await profileStore.updateProfile({ schedule, meetingDuration });

    useEffect(() => {
      setMeetingDuration(`${profileStore.profile.meetingDuration}`);
      setSchedule(profileStore.profile.schedule);
    }, [profileStore.profile.meetingDuration, profileStore.profile.schedule]);

    return (
      <Page
        title={globalStore.i18n._(t`Settings`)}
        breadcrumbs={[{ content: globalStore.i18n._(t`Waitinglist`), url: "/member/waitinglist" }]}
        fullWidth
      >
        <Card primaryFooterAction={{ content: globalStore.i18n._(t`Submit`), onClick: handleProfileSubmit }}>
          <Form onSubmit={handleProfileSubmit}>
            <Card.Section title={globalStore.i18n._(t`General`)}>
              <FormLayout>
                <FormLayout.Group>
                  <TextField
                    onChange={handleMeetingDurationChange}
                    value={meetingDuration}
                    label={globalStore.i18n._(t`Meeting Duration`)}
                    placeholder={globalStore.i18n._(t`Minutes`)}
                    type="number"
                  />
                </FormLayout.Group>
              </FormLayout>
            </Card.Section>
            <Card.Section title={globalStore.i18n._(t`Schedule`)}>
              <FormLayout>
                <FormLayout.Group>
                  <Stack>
                    <TextField
                      label=""
                      prefix={globalStore.i18n._(t`Start`)}
                      type="time"
                      labelHidden
                      value={dateRange[0]}
                      onChange={handleStartChange}
                    />
                    <TextField
                      prefix={globalStore.i18n._(t`End`)}
                      type="time"
                      label=""
                      labelHidden
                      min={dateRange[0]}
                      value={dateRange[1]}
                      onChange={handleEndChange}
                    />
                    <Button onClick={handleScheduleAdd}>Add</Button>
                  </Stack>
                </FormLayout.Group>
                <FormLayout.Group>
                  <Stack>
                    {map(schedule, (row, index) => (
                      <Tag key={index} onRemove={() => handleScheduleRemove(index)}>
                        {row[0]} - {row[1]}
                      </Tag>
                    ))}
                  </Stack>
                </FormLayout.Group>
              </FormLayout>
            </Card.Section>
          </Form>
        </Card>
      </Page>
    );
  })
);
