import React from "react";
import { Badge } from "@shopify/polaris";
import { inject, observer } from "mobx-react";
import { t } from "@lingui/macro";

export const WaitinglistStatusBadge = inject("globalStore")(
  observer(({ status, globalStore }) => {
    let badgeStatus;
    if (status === "meeting") badgeStatus = "success";
    else if (status === "happening") badgeStatus = "attention";
    else badgeStatus = "";

    let badgeContent;
    if (status === "meeting") badgeContent = globalStore.i18n._(t`Scheduled`);
    else if (status === "happening") badgeContent = globalStore.i18n._(t`Happening`);
    else badgeContent = globalStore.i18n._(t`Waiting`);

    return <Badge status={badgeStatus}>{badgeContent}</Badge>;
  })
);
