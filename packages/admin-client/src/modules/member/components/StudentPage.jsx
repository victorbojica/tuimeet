import React, { useEffect } from "react";
import { inject, observer } from "mobx-react";
import { useHistory, useParams } from "react-router-dom";
import { Page, Card, TextContainer, Link, DataTable, DescriptionList, Scrollable } from "@shopify/polaris";
import * as DateUtils from "date-fns";
import { reduce, includes } from "lodash";
import { t, Trans } from "@lingui/macro";

import { ItemDetail, StudentMeetingStatus } from "../../common/components";

const dateFormat = "dd/MM/yyyy hh:mm";

export const StudentPage = inject(
  "registrationPipelineStore",
  "globalStore"
)(
  observer(({ registrationPipelineStore, globalStore }) => {
    const history = useHistory();
    const { id } = useParams();
    if (!id) history.back();

    useEffect(() => {
      const method = async () => {
        await registrationPipelineStore.fetchRegistration(id);
      };
      method();
    }, [id]);

    return (
      <Page
        title={`${registrationPipelineStore.current.registration.fullName}`}
        breadcrumbs={[{ content: globalStore.i18n._(t`Waitinglist`), url: "/member/waitinglist" }]}
        fullWidth
      >
        <Card title={globalStore.i18n._(t`Info`)} sectioned>
          <TextContainer>
            <ItemDetail title={globalStore.i18n._(t`Full Name`)}>
              {registrationPipelineStore.current.registration.fullName}
            </ItemDetail>
            <ItemDetail title={globalStore.i18n._(t`Email`)}>
              {registrationPipelineStore.current.registration.email}
            </ItemDetail>
            <ItemDetail title={globalStore.i18n._(t`Ref ID`)}>
              {registrationPipelineStore.current.registration.refId}
            </ItemDetail>
          </TextContainer>
        </Card>

        <Card>
          <Card.Header title={globalStore.i18n._(t`Meeting`)}>
            <StudentMeetingStatus status={registrationPipelineStore.current.status} />
          </Card.Header>
          <Card.Section sectioned>
            <TextContainer>
              <ItemDetail title={globalStore.i18n._(t`Member`)}>
                {registrationPipelineStore.current.user.fullName} - {registrationPipelineStore.current.user.email}
              </ItemDetail>
              <ItemDetail title={globalStore.i18n._(t`Notes`)}>
                {registrationPipelineStore.current.meeting.notes
                  ? registrationPipelineStore.current.meeting.notes
                  : "-"}
              </ItemDetail>
              <ItemDetail title={globalStore.i18n._(t`Meeting URL`)}>
                {registrationPipelineStore.current.meeting.url ? (
                  <Link url={registrationPipelineStore.current.meeting.url}>
                    {registrationPipelineStore.current.meeting.url}
                  </Link>
                ) : (
                  "-"
                )}
              </ItemDetail>
              <ItemDetail title={globalStore.i18n._(t`Recording`)}>
                {registrationPipelineStore.current.meeting.recordingUrl ? (
                  <Link url={registrationPipelineStore.current.meeting.recordingUrl}>
                    <Trans>Download Recording</Trans>
                  </Link>
                ) : (
                  "-"
                )}
              </ItemDetail>
              <ItemDetail title={globalStore.i18n._(t`Details`)}>
                <DataTable
                  columnContentTypes={["text", "text", "text", "text", "text"]}
                  hideScrollIndicator
                  headings={[
                    globalStore.i18n._(t`Estimated Start Time`),
                    globalStore.i18n._(t`Start Time`),
                    globalStore.i18n._(t`End Time`),
                    globalStore.i18n._(t`Duration`),
                    globalStore.i18n._(t`Time to Response`),
                  ]}
                  rows={[
                    [
                      registrationPipelineStore.current.meeting.estimatedStartTime
                        ? DateUtils.format(registrationPipelineStore.current.meeting.estimatedStartTime, dateFormat)
                        : "-",
                      registrationPipelineStore.current.meeting.startedAt
                        ? DateUtils.format(registrationPipelineStore.current.meeting.startedAt, dateFormat)
                        : "-",
                      registrationPipelineStore.current.meeting.endedAt
                        ? DateUtils.format(registrationPipelineStore.current.meeting.endedAt, dateFormat)
                        : "-",
                      registrationPipelineStore.current.meeting.duration || "-",
                      registrationPipelineStore.current.meeting.timeToResponse || "-",
                    ],
                  ]}
                />
              </ItemDetail>
            </TextContainer>
          </Card.Section>
        </Card>

        <Card title={globalStore.i18n._(t`Registration`)} sectioned>
          <TextContainer>
            <Trans>
              <p>Below are all the details stored for the student's registration</p>
            </Trans>
            {registrationPipelineStore.current.referenceRegistration &&
            registrationPipelineStore.current.referenceRegistration.rest ? (
              <Scrollable shadow style={{ height: "400px" }}>
                <DescriptionList
                  items={reduce(
                    registrationPipelineStore.current.referenceRegistration.rest,
                    (acc, value, key) => {
                      if (includes(key, "PK_") || includes("FK_") || includes("ID_")) return acc;
                      return [{ term: key, description: value }, ...acc];
                    },
                    []
                  )}
                />
              </Scrollable>
            ) : (
              "-"
            )}
          </TextContainer>
        </Card>
      </Page>
    );
  })
);
