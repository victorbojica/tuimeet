import React from "react";
import { Badge } from "@shopify/polaris";
import { inject, observer } from "mobx-react";
import { t } from "@lingui/macro";

export const MeetingStatusBadge = inject("globalStore")(
  observer(({ status, globalStore }) => {
    let badgeStatus;
    if (status === "complete") badgeStatus = "success";
    else if (status === "canceled") badgeStatus = "critical";
    else badgeStatus = "";

    let badgeContent;
    if (status === "complete") badgeContent = globalStore.i18n._(t`Complete`);
    else if (status === "canceled") badgeContent = globalStore.i18n._(t`Canceled`);
    else badgeContent = globalStore.i18n._(t`Unknown`);

    return <Badge status={badgeStatus}>{badgeContent}</Badge>;
  })
);
