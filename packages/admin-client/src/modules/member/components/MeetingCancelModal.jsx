import React, { useEffect, useState } from "react";
import { inject, observer } from "mobx-react";
import { FormLayout, Modal, TextContainer, TextField, Form } from "@shopify/polaris";
import { t, Trans } from "@lingui/macro";

export const MeetingCancelModal = inject(
  "registrationPipelineStore",
  "globalStore"
)(
  observer(({ active, handleToggle, registrationPipelineStore, globalStore }) => {
    const [reason, setReason] = useState("");

    useEffect(() => {
      setReason("");
    }, [setReason]);

    const handleReason = (value) => {
      setReason(value);
    };

    const handleSubmit = async () => {
      await registrationPipelineStore.cancelMeeting(registrationPipelineStore.currentRegistrationId, {
        reason,
      });
      handleToggle();
    };

    return (
      <Modal
        open={active}
        onClose={handleToggle}
        title={globalStore.i18n._(t`Cancel Meeting`)}
        primaryAction={{
          content: globalStore.i18n._(t`Submit`),
          onAction: handleSubmit,
          disabled: !reason,
        }}
        secondaryActions={[
          {
            content: globalStore.i18n._(t`Cancel`),
            onAction: handleToggle,
          },
        ]}
      >
        <Modal.Section>
          <TextContainer>
            <Trans>
              <p>
                Provide a reason and cancel the meeting. The registration will still be appearing in the waitinglist.
              </p>
              <p>The student will be notified about the cancellation of the meeting.</p>
            </Trans>
          </TextContainer>
          <br />
          <Form onSubmit={handleSubmit}>
            <FormLayout>
              <TextField
                onChange={handleReason}
                value={reason}
                labelHidden
                prefix={globalStore.i18n._(t`Reason`)}
                type="text"
                placeholder={globalStore.i18n._(t`Some Reason`)}
              />
            </FormLayout>
          </Form>
        </Modal.Section>
      </Modal>
    );
  })
);
