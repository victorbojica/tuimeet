import React from "react";
import * as DateUtils from "date-fns";
import { inject, observer } from "mobx-react";
import { useHistory } from "react-router-dom";
import { Calendar, Views, dateFnsLocalizer } from "react-big-calendar";

import "react-big-calendar/lib/css/react-big-calendar.css";
import "./MeetingsCalendar.scss";
import { Scrollable } from "@shopify/polaris";

const locales = {
  "en-US": require("date-fns/locale/en-US"),
};

const localizer = dateFnsLocalizer({
  format: DateUtils.format,
  parse: DateUtils.parse,
  startOfWeek: DateUtils.startOfWeek,
  getDay: DateUtils.getDay,
  locales,
});

export const MeetingsCalendar = inject("registrationPipelineStore")(
  observer(({ registrationPipelineStore }) => {
    const history = useHistory();

    return (
      <div className="calendar-container">
        <Scrollable shadow style={{ height: 500 }}>
          <Calendar
            events={registrationPipelineStore.meetingEvents}
            defaultView={Views.DAY}
            defaultDate={new Date()}
            localizer={localizer}
            step={35}
            timeslots={1}
            endAccessor="end"
            startAccessor="start"
            toolbar={false}
            style={{ height: 1690 }} // 1680 height for a full day - gave it extra room
            scrollToTime={DateUtils.subMinutes(new Date(), 30)}
            onSelectEvent={(e) => history.push(`/member/meet/${e.id}`)}
            formats={{
              timeGutterFormat: "HH:mm",
              eventTimeRangeFormat: ({ start, end }) =>
                `${DateUtils.format(start, "HH:mm")} - ${DateUtils.format(end, "HH:mm")}`,
            }}
          />
        </Scrollable>
      </div>
    );
  })
);
