import React, { useEffect, useState } from "react";
import { inject, observer } from "mobx-react";
import { FormLayout, Modal, TextContainer, TextField, Form } from "@shopify/polaris";
import { t, Trans } from "@lingui/macro";
import * as DateUtils from "date-fns";

export const MeetingRescheduleModal = inject(
  "registrationPipelineStore",
  "globalStore",
  "profileStore"
)(
  observer(({ active, handleToggle, registrationPipelineStore, globalStore, profileStore }) => {
    const [estimatedStartTime, setEstimatedStartTime] = useState(DateUtils.format(new Date(), "HH:mm"));
    const [estimatedDuration, setEstimatedDuration] = useState(""); // TODO get default state from user settings
    const [reason, setReason] = useState("");

    useEffect(() => {
      setEstimatedStartTime(DateUtils.format(new Date(), "HH:mm"));
      setEstimatedDuration(`${profileStore.profile.meetingDuration}`);
      setReason("");
    }, [profileStore.profile.meetingDuration]);

    const handleEstimatedStartTime = (value) => {
      setEstimatedStartTime(value);
    };
    const handleEstimatedDuration = (value) => {
      setEstimatedDuration(value);
    };
    const handleReason = (value) => {
      setReason(value);
    };

    const handleSubmit = async () => {
      await registrationPipelineStore.rescheduleMeeting(registrationPipelineStore.currentRegistrationId, {
        estimatedStartTime,
        estimatedDuration,
        reason,
      });
      handleToggle();
    };

    return (
      <Modal
        open={active}
        onClose={handleToggle}
        title={globalStore.i18n._(t`Reschedule Meeting`)}
        primaryAction={{
          content: globalStore.i18n._(t`Submit`),
          onAction: handleSubmit,
          disabled: !(estimatedStartTime && estimatedDuration && reason),
        }}
        secondaryActions={[
          {
            content: globalStore.i18n._(t`Cancel`),
            onAction: handleToggle,
          },
        ]}
      >
        <Modal.Section>
          <TextContainer>
            <Trans>
              <p>
                Set the starting time and duration for your meeting. The default meeting duration is 15 minutes. The
                scheduling is done only for the current day.
              </p>
              <p>The student will be notified 5 minutes before the meeting is planned to start. So will you.</p>
            </Trans>
          </TextContainer>
          <br />
          <Form onSubmit={handleSubmit}>
            <FormLayout>
              <FormLayout.Group>
                <TextField
                  onChange={handleEstimatedStartTime}
                  value={estimatedStartTime}
                  type="time"
                  prefix={globalStore.i18n._(t`Time`)}
                  labelHidden
                />
                <TextField
                  onChange={handleEstimatedDuration}
                  value={estimatedDuration}
                  type="number"
                  prefix={globalStore.i18n._(t`Duration`)}
                  labelHidden
                  placeholder={globalStore.i18n._(t`Minutes`)}
                />
              </FormLayout.Group>
              <TextField
                onChange={handleReason}
                value={reason}
                labelHidden
                prefix={globalStore.i18n._(t`Reason`)}
                type="text"
                placeholder={globalStore.i18n._(t`Some Reason`)}
              />
            </FormLayout>
          </Form>
        </Modal.Section>
      </Modal>
    );
  })
);
