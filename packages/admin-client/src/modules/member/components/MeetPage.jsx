import React, { useCallback, useEffect, useState } from "react";
import { Page, Card, TextField, Stack, Link, Label, Form } from "@shopify/polaris";
import { useParams, useHistory } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { t, Trans } from "@lingui/macro";

import { ItemDetail, RecordingUploadField } from "../../common/components";
import { MeetingRescheduleModal } from "./MeetingRescheduleModal";
import { MeetingCancelModal } from "./MeetingCancelModal";

export const MeetPage = inject(
  "registrationPipelineStore",
  "globalStore"
)(
  observer(({ registrationPipelineStore, globalStore }) => {
    const history = useHistory();
    const { id } = useParams();
    if (!id) history.back();

    useEffect(() => {
      const method = async () => {
        await registrationPipelineStore.fetchRegistration(id);
      };
      method();
    }, [id]);

    const [notes, setNotes] = useState("");
    const handleNotesChange = (value) => setNotes(value);

    const [recording, setRecording] = useState([]);
    const handleRecordingChange = (value) => setRecording(value);

    const [rescheduleModalActive, setRescheduleModalActive] = useState(false);
    const handleRescheduleModalChange = useCallback(() => setRescheduleModalActive(!rescheduleModalActive), [
      setRescheduleModalActive,
      rescheduleModalActive,
    ]);

    const [cancelModalActive, setCancelModalActive] = useState(false);
    const handleCancelModalChange = useCallback(() => setCancelModalActive(!cancelModalActive), [
      setCancelModalActive,
      cancelModalActive,
    ]);

    const handleMeetingComplete = async () => {
      await registrationPipelineStore.completeMeeting(registrationPipelineStore.current.registration.id, {
        notes,
        recording,
      });
    };

    return (
      <Page
        title={globalStore.i18n._(t`Meet ${registrationPipelineStore.current.registration.fullName}`)}
        breadcrumbs={[{ content: globalStore.i18n._(t`Waitinglist`), url: "/member/waitinglist" }]}
        fullWidth
      >
        <Card sectioned title={globalStore.i18n._(t`Details`)}>
          <Stack>
            <ItemDetail title={globalStore.i18n._(t`Starts In`)}>
              {registrationPipelineStore.current.meeting.startingInMinutes() || globalStore.i18n._(t`Started`)}
            </ItemDetail>
            <ItemDetail title={globalStore.i18n._(t`Meeting URL`)}>
              <Link
                onClick={() => registrationPipelineStore.joinMeeting(registrationPipelineStore.current.registration.id)}
              >
                <Trans>Go to meeting</Trans>
              </Link>
            </ItemDetail>
            <ItemDetail title={globalStore.i18n._(t`Time Elapsed`)}>
              {registrationPipelineStore.current.meeting.startedForMinutes() || globalStore.i18n._(t`Not Started`)}
            </ItemDetail>
            <ItemDetail title={globalStore.i18n._(t`Student`)}>
              {registrationPipelineStore.current.registration.email},{" "}
              <Link url={`/member/registration/${registrationPipelineStore.current.registration.id}`}>
                <Trans>Details</Trans>
              </Link>
            </ItemDetail>
          </Stack>
        </Card>
        <Card
          sectioned
          secondaryFooterActions={[
            {
              content: globalStore.i18n._(t`Cancel`),
              onAction: () => {
                registrationPipelineStore.setCurrentRegistrationId(registrationPipelineStore.current.registration.id);
                handleCancelModalChange();
              },
            },
            {
              content: globalStore.i18n._(t`Reschedule`),
              onAction: () => {
                registrationPipelineStore.setCurrentRegistrationId(registrationPipelineStore.current.registration.id);
                handleRescheduleModalChange();
              },
            },
          ]}
          primaryFooterAction={{ content: globalStore.i18n._(t`Complete`), onAction: handleMeetingComplete }}
        >
          <br />
          <Form onSubmit={handleMeetingComplete}>
            <TextField
              onChange={handleNotesChange}
              value={notes || registrationPipelineStore.current.meeting.notes}
              label={globalStore.i18n._(t`Notes`)}
              multiline={4}
              placeholder={globalStore.i18n._(t`A place where to keep a short summary of the meeting ...`)}
            />
            <br />
            <Label>
              <Trans>Recording</Trans>
            </Label>
            <RecordingUploadField value={recording} onChange={handleRecordingChange} />
          </Form>
        </Card>

        <MeetingRescheduleModal handleToggle={handleRescheduleModalChange} active={rescheduleModalActive} />
        <MeetingCancelModal handleToggle={handleCancelModalChange} active={cancelModalActive} />
      </Page>
    );
  })
);
