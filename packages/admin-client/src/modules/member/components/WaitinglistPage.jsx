import React, { useState, useCallback } from "react";
import { Page, Card, Layout } from "@shopify/polaris";
import { inject, observer } from "mobx-react";
import { t } from "@lingui/macro";

import { Waitinglist } from "./Waitinglist";
import { MeetingsCalendar } from "./MeetingsCalendar";
import { MeetingScheduleModal } from "./MeetingScheduleModal";

export const WaitinglistPage = inject("globalStore")(
  observer(({ globalStore }) => {
    const [scheduleModalActive, setScheduleModalActive] = useState(false);
    const handleScheduleModalChange = useCallback(() => setScheduleModalActive(!scheduleModalActive), [
      setScheduleModalActive,
      scheduleModalActive,
    ]);

    return (
      <Page title={globalStore.i18n._(t`Waitinglist`)} fullWidth>
        <Layout>
          <Layout.Section>
            <Card>
              <Waitinglist handleScheduleModalToggle={handleScheduleModalChange} />
            </Card>
          </Layout.Section>

          <Layout.Section secondary>
            <Card sectioned title={globalStore.i18n._(t`Calendar`)}>
              <MeetingsCalendar />
            </Card>
          </Layout.Section>
        </Layout>

        <MeetingScheduleModal handleToggle={handleScheduleModalChange} active={scheduleModalActive} />
      </Page>
    );
  })
);
