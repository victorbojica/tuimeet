import React, { useEffect } from "react";
import { Page, Card, DataTable, Link, EmptyState } from "@shopify/polaris";
import * as DateUtils from "date-fns";
import { t, Trans } from "@lingui/macro";

import { inject, observer } from "mobx-react";
import { MeetingStatusBadge } from "./MeetingStatusBadge";
import emptyStateImage from "../../../assets/empty-state.png";

export const MeetingsPage = inject(
  "genericStore",
  "profileStore",
  "globalStore"
)(
  observer(({ genericStore, profileStore, globalStore }) => {
    useEffect(() => {
      const method = async () => {
        await genericStore.fetchMeetings();
      };
      method();
    }, [genericStore]);

    const data = genericStore.meetings.map(({ meeting, registration }) => {
      return [
        <Link url={`registration/${registration.id}`}>
          {registration.firstName} {registration.lastName}
        </Link>,
        profileStore.profile.fullName,
        <MeetingStatusBadge status={meeting.status} />,
        meeting.startedAt ? DateUtils.format(meeting.startedAt, "dd/MM/yyyy HH:mm") : globalStore.i18n._(t`-`),
        meeting.duration ? meeting.duration : globalStore.i18n._(t`-`),
        meeting.userJoinedAt ? DateUtils.format(meeting.userJoinedAt, "dd/MM/yyyy HH:mm") : globalStore.i18n._(t`-`),
        meeting.registrationJoinedAt
          ? DateUtils.format(meeting.registrationJoinedAt, "dd/MM/yyyy HH:mm")
          : globalStore.i18n._(t`-`),
        meeting.recordingUrl ? (
          <Link external url={meeting.recordingUrl}>
            <Trans>Download</Trans>
          </Link>
        ) : (
          "-"
        ),
      ];
    });

    return (
      <Page
        title={globalStore.i18n._(t`Meetings`)}
        breadcrumbs={[{ content: globalStore.i18n._(t`Waitinglist`), url: "/member/waitinglist" }]}
        fullWidth
      >
        <Card>
          <DataTable
            columnContentTypes={["text", "text", "text", "text", "text", "text", "text"]}
            hideScrollIndicator
            headings={[
              globalStore.i18n._(t`Student`),
              globalStore.i18n._(t`Member`),
              globalStore.i18n._(t`Status`),
              globalStore.i18n._(t`Start Time`),
              globalStore.i18n._(t`Duration`),
              globalStore.i18n._(t`Member Join Time`),
              globalStore.i18n._(t`Registration Join Time`),
              globalStore.i18n._(t`Recording`),
            ]}
            rows={data}
          />
          {!data.length && (
            <div style={{ marginTop: "-2rem" }}>
              <EmptyState centeredLayout image={emptyStateImage} heading={globalStore.i18n._(t`No Meetings`)}>
                <Trans>You haven't met with anyone yet.</Trans>
              </EmptyState>
            </div>
          )}
        </Card>
      </Page>
    );
  })
);
