import React from "react";
import { Provider } from "mobx-react";
import { Switch, Route, useRouteMatch } from "react-router-dom";

import { Layout } from "../common/components/";
import { WaitinglistPage, MeetPage, MeetingsPage, SettingsPage, StudentPage } from "./components";

import profileStore from "./ProfileStore";
import genericStore from "./GenericStore";
import registrationPipelineStore from "./RegistrationPipelineStore";

export const Member = () => {
  const { path } = useRouteMatch();
  return (
    <Provider
      registrationPipelineStore={registrationPipelineStore}
      genericStore={genericStore}
      profileStore={profileStore}
    >
      <Layout>
        <Switch>
          <Route exact path={`${path}/settings`}>
            <SettingsPage />
          </Route>
          <Route exact path={`${path}/`}>
            <WaitinglistPage />
          </Route>
          <Route exact path={`${path}/waitinglist`}>
            <WaitinglistPage />
          </Route>
          <Route exact path={`${path}/registration/:id`}>
            <StudentPage />
          </Route>
          <Route exact path={`${path}/meetings`}>
            <MeetingsPage />
          </Route>
          <Route exact path={`${path}/meet/:id`}>
            <MeetPage />
          </Route>
        </Switch>
      </Layout>
    </Provider>
  );
};
