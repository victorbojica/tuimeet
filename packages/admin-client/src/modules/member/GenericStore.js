import { map } from "lodash";
import { action, observable } from "mobx";

import globalStore from "../common/GlobalStore";
import * as Meeting from "../../lib/services/Meeting";
import { enrichRegistrationPipeline } from "../../domain";

export class GenericStore {
  @observable meetings = [];

  @action async fetchMeetings() {
    try {
      const result = await Meeting.listMeetings();
      this.meetings = map(result, enrichRegistrationPipeline);
    } catch (e) {
      globalStore.setErrorMessage(e);
    }
  }
}

const store = new GenericStore();
export default store;
