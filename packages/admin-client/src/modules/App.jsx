import React from "react";
import { BrowserRouter as Router, Switch, Route, useHistory } from "react-router-dom";
import { inject, observer, Provider } from "mobx-react";
import { AppProvider, ThemeProvider } from "@shopify/polaris";
import enTranslations from "@shopify/polaris/locales/en.json";

import "@shopify/polaris/styles.css";

import globalStore from "./common/GlobalStore";
import authStore from "./auth/AuthStore";

import { Organization } from "./organization";
import { Member } from "./member";
import { LoginPage, Logout, Authorized } from "./auth/components";
import { I18nProvider } from "./common/components";

const theme = {
  colors: {
    topBar: {
      background: "#1f458d",
    },
  },
  logo: {
    width: 35,
    topBarSource: "https://ac.tuiasi.ro/wp-content/uploads/2019/05/cropped-logo_ac_iasi.png",
    contextualSaveBarSource: "https://ac.tuiasi.ro/wp-content/uploads/2019/05/cropped-logo_ac_iasi.png",
    url: "#",
    accessibilityLabel: "Jaded Pixel",
  },
};

const RouteByRole = inject("authStore")(
  observer(({ authStore }) => {
    const history = useHistory();
    const auth = authStore.getAuth();

    if (!auth) {
      history.push("/login");
      return null;
    }

    if (auth.role === "member") {
      history.push("/member");
    } else if (auth.role === "administrator") {
      history.push("/organization");
    } else {
      history.push("/login");
    }

    return null;
  })
);

const App = () => (
  <AppProvider i18n={enTranslations}>
    <ThemeProvider theme={theme}>
      <Provider globalStore={globalStore} authStore={authStore}>
        <I18nProvider>
          <Router>
            <Switch>
              <Route path="/login">
                <LoginPage />
              </Route>

              <Route path="/logout">
                <Logout />
              </Route>

              <Route exact path="/">
                <RouteByRole />
              </Route>

              <Route path="/organization">
                <Authorized role="administrator">
                  <Organization />
                </Authorized>
              </Route>

              <Route path="/member">
                <Authorized role="member">
                  <Member />
                </Authorized>
              </Route>
            </Switch>
          </Router>
        </I18nProvider>
      </Provider>
    </ThemeProvider>
  </AppProvider>
);

export default App;
