import { inject, observer } from "mobx-react";
import { useHistory } from "react-router-dom";

export const Logout = inject("authStore")(
  observer(({ authStore }) => {
    const history = useHistory();
    authStore.clearAuthentication();
    history.push("/login");
    return null;
  })
);
