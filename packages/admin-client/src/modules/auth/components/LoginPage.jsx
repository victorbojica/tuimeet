import React, { useState } from "react";
import { inject, observer } from "mobx-react";
import { useHistory } from "react-router-dom";
import { Card, Form, TextField, Button, FormLayout, Page } from "@shopify/polaris";
import { t, Trans } from "@lingui/macro";

import { FlashMessageBanner } from "../../common/components";

export const LoginPage = inject(
  "authStore",
  "globalStore"
)(
  observer(({ authStore, globalStore }) => {
    const history = useHistory();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const handleEmailChange = (value) => setEmail(value);
    const handlePasswordChange = (value) => setPassword(value);

    const handleLogin = async () => {
      const isAuthenticated = await authStore.authenticate(email, "");
      if (isAuthenticated) history.push("/");
    };

    return (
      <Page>
        <Card sectioned>
          <Form onSubmit={handleLogin}>
            <FlashMessageBanner noPageWrapper />
            <FormLayout>
              <TextField
                label={globalStore.i18n._(t`Email`)}
                placeholder={globalStore.i18n._(t`email@example.com`)}
                value={email}
                onChange={handleEmailChange}
              />
              <TextField
                label={globalStore.i18n._(t`Password`)}
                type="password"
                placeholder={globalStore.i18n._(t`your password here ...`)}
                value={password}
                onChange={handlePasswordChange}
              />
              <Button primary onClick={handleLogin}>
                <Trans>Log In</Trans>
              </Button>
            </FormLayout>
          </Form>
        </Card>
      </Page>
    );
  })
);
