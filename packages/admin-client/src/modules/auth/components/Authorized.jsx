import { inject, observer } from "mobx-react";
import { useHistory } from "react-router-dom";
import { t } from "@lingui/macro";

export const Authorized = inject(
  "authStore",
  "globalStore"
)(
  observer(({ authStore, globalStore, children, role }) => {
    const history = useHistory();
    if (authStore.isAuthenticated()) {
      const roleAuthorized = role && authStore.getAuth().role === role;
      if (roleAuthorized) return children;
      else {
        history.goBack();
        globalStore.setErrorMessage(globalStore.i18n._(t`Not authorized`));
      }
    } else {
      history.push("/login");
      return null;
    }
  })
);
