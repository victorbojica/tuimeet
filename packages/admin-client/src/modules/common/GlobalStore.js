import { action, observable } from "mobx";
import { i18n } from "@lingui/core";
import { t } from "@lingui/macro";

import en from "../../lib/locale/en/messages";
import ro from "../../lib/locale/ro/messages";

i18n.load("en", en.messages);
i18n.load("ro", ro.messages);

i18n.activate("en");

export class GlobalStore {
  flashMessageTimeout = null;

  @observable errors = {};
  @observable successMessage = null;
  @observable language = "en";
  @observable i18n = i18n;

  @observable flashMessage = null;

  @action changeLanguage(language) {
    this.language = language;
    this.i18n.activate(language);
    this.setInfoMessage(this.i18n._(t`Language changed`));
  }

  @action setErrorMessage(message) {
    this.setFlashMessage("error", message);
  }

  @action setSuccessMessage(message) {
    this.setFlashMessage("success", message);
  }

  @action setInfoMessage(message) {
    this.setFlashMessage("info", message);
  }

  @action setFlashMessage(type, message) {
    this.flashMessage = { type, message };
    clearTimeout(this.flashMessageTimeout);
    this.flashMessageTimeout = setTimeout(() => {
      this.unsetFlashMessage();
    }, 5000);
  }

  @action unsetFlashMessage() {
    this.flashMessage = null;
  }
}

const globalStore = new GlobalStore();

export default globalStore;
