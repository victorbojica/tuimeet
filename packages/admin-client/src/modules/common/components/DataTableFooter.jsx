import React from "react";
import { Icon, Link } from "@shopify/polaris";
import { ChevronLeftMinor, ChevronRightMinor } from "@shopify/polaris-icons";

export const DataTableFooter = ({ data }) => {
  return (
    <div style={{ position: "relative" }}>
      Showing {data.length} of {data.length * 3} results
      <div
        style={{
          position: "absolute",
          right: 0,
          top: 0,
          height: "100%",
        }}
      >
        <div
          style={{
            display: "inline-block",
            marginRight: "16px",
            verticalAlign: "middle",
          }}
        >
          <Icon source={ChevronLeftMinor} />
        </div>
        <div
          style={{
            display: "inline-block",
            marginRight: "16px",
            verticalAlign: "middle",
          }}
        >
          <Link url="#">1 </Link>
          <Link url="#">2 </Link>
          <Link url="#">3 </Link>
          <Link url="#">... </Link>
          <Link url="#">6</Link>
        </div>
        <div style={{ display: "inline-block", verticalAlign: "middle" }}>
          <Icon source={ChevronRightMinor} />
        </div>
      </div>
    </div>
  );
};
