import { Button, ButtonGroup } from "@shopify/polaris";
import React from "react";
import { inject, observer } from "mobx-react";

export const LanguageChange = inject("globalStore")(
  observer(({ globalStore }) => {
    return (
      <>
        <ButtonGroup segmented>
          <Button size="slim" primary={globalStore.language === "ro"} onClick={() => globalStore.changeLanguage("ro")}>
            RO
          </Button>
          <Button size="slim" primary={globalStore.language === "en"} onClick={() => globalStore.changeLanguage("en")}>
            EN
          </Button>
        </ButtonGroup>
      </>
    );
  })
);
