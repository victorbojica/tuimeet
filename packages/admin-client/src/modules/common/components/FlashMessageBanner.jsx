import React from "react";
import { Banner, Page } from "@shopify/polaris";
import { inject, observer } from "mobx-react";

export const FlashMessageBanner = inject("globalStore")(
  observer(({ globalStore, noPageWrapper }) => {
    let status;
    if (globalStore.flashMessage && globalStore.flashMessage.type === "error") status = "critical";
    else if (globalStore.flashMessage && globalStore.flashMessage.type === "success") status = "success";
    else if (globalStore.flashMessage) status = "info";

    return globalStore.flashMessage ? (
      noPageWrapper ? (
        <>
          <Banner onDismiss={() => globalStore.unsetFlashMessage()} status={status}>
            {globalStore.flashMessage.message}
          </Banner>
          <br />
        </>
      ) : (
        <Page fullWidth>
          <div style={{ position: "relative", width: "100%", marginTop: "-2rem" }}>
            <div style={{ position: "absolute", width: "100%", top: "2rem", zIndex: 100 }}>
              <Banner onDismiss={() => globalStore.unsetFlashMessage()} status={status}>
                {globalStore.flashMessage.message}
              </Banner>
            </div>
          </div>
        </Page>
      )
    ) : null;
  })
);
