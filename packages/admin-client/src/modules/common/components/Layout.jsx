import React, { useState, useCallback, useEffect } from "react";
import { inject, observer } from "mobx-react";
import { TopBar, Frame, Navigation, Loading } from "@shopify/polaris";
import * as icons from "@shopify/polaris-icons";
import { useRouteMatch } from "react-router-dom";
import { t } from "@lingui/macro";

import { LoadingPage } from "./LoadingPage";
import { LanguageChange } from "./LanguageSelect";
import { FlashMessageBanner } from "./FlashMessageBanner";

export const Layout = inject(
  "profileStore",
  "globalStore"
)(
  observer(({ children, profileStore, globalStore }) => {
    const { path } = useRouteMatch();

    useEffect(() => {
      const method = async () => {
        await profileStore.fetchProfile();
      };
      method();
    }, []);

    const userRole = path.includes("member") ? globalStore.i18n._(t`Member`) : globalStore.i18n._(t`Administrator`);

    const [isLoading, setIsLoading] = useState(false);
    const [userMenuActive, setUserMenuActive] = useState(false);
    const [mobileNavigationActive, setMobileNavigationActive] = useState(false);

    const toggleUserMenuActive = useCallback(() => setUserMenuActive((userMenuActive) => !userMenuActive), []);
    const toggleMobileNavigationActive = useCallback(
      () => setMobileNavigationActive((mobileNavigationActive) => !mobileNavigationActive),
      []
    );
    const toggleIsLoading = useCallback(() => setIsLoading((isLoading) => !isLoading), []);

    const userMenuActions = [
      {
        items: [{ content: globalStore.i18n._(t`Log Out`), url: "/login" }],
      },
    ];

    const userMenuMarkup = (
      <>
        <div style={{ marginRight: "10px" }}>
          <LanguageChange />
        </div>
        <TopBar.UserMenu
          actions={userMenuActions}
          name={profileStore.profile.fullName}
          detail={userRole}
          initials={"M"}
          open={userMenuActive}
          onToggle={toggleUserMenuActive}
        />
      </>
    );

    const topBarMarkup = (
      <TopBar showNavigationToggle userMenu={userMenuMarkup} onNavigationToggle={toggleMobileNavigationActive} />
    );

    const organizationUrl = "/organization";
    const memberUrl = "/member";

    const AdministratorNavigation = (
      <>
        <Navigation.Section
          separator
          title={globalStore.i18n._(t`Menu`)}
          items={[
            {
              label: globalStore.i18n._(t`Dashboard`),
              onClick: toggleIsLoading,
              url: `${organizationUrl}/`,
              icon: icons.AnalyticsMajorMonotone,
            },
            {
              label: globalStore.i18n._(t`Meetings`),
              onClick: toggleIsLoading,
              url: `${organizationUrl}/meetings`,
              icon: icons.CalendarMajorMonotone,
            },
            {
              label: globalStore.i18n._(t`Students`),
              onClick: toggleIsLoading,
              url: `${organizationUrl}/students`,
              icon: icons.CustomersMajorMonotone,
            },
          ]}
        />
        <Navigation.Section
          separator
          title={globalStore.i18n._(t`Settings`)}
          items={[
            {
              label: globalStore.i18n._(t`Organization`),
              url: `${organizationUrl}/settings#organization`,
              icon: icons.GlobeMajorMonotone,
            },
            {
              label: globalStore.i18n._(t`Members`),
              url: `${organizationUrl}/settings#members`,
              icon: icons.HashtagMajorMonotone,
            },
          ]}
        />
      </>
    );

    const MemberNavigation = (
      <>
        <Navigation.Section
          separator
          title={globalStore.i18n._(t`Menu`)}
          items={[
            {
              label: globalStore.i18n._(t`Waitinglist`),
              url: `${memberUrl}/waitinglist`,
              icon: icons.ListMajorMonotone,
            },
            {
              label: globalStore.i18n._(t`Meetings`),
              url: `${memberUrl}/meetings`,
              icon: icons.CalendarMajorMonotone,
            },
            {
              label: globalStore.i18n._(t`Settings`),
              url: `${memberUrl}/settings`,
              icon: icons.SettingsMajorMonotone,
            },
          ]}
        />
      </>
    );

    const navigationMarkup = (
      <Navigation location="/">
        {userRole === globalStore.i18n._(t`Administrator`) && AdministratorNavigation}
        {userRole === globalStore.i18n._(t`Member`) && MemberNavigation}
      </Navigation>
    );

    const loadingMarkup = isLoading ? <Loading /> : null;
    const pageMarkup = isLoading ? <LoadingPage /> : children;

    return (
      <Frame
        topBar={topBarMarkup}
        navigation={navigationMarkup}
        showMobileNavigation={mobileNavigationActive}
        onNavigationDismiss={toggleMobileNavigationActive}
      >
        {loadingMarkup}
        <FlashMessageBanner />
        {pageMarkup}
      </Frame>
    );
  })
);
