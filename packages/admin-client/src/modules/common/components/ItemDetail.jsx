import React from "react";
import { Subheading } from "@shopify/polaris";

export const ItemDetail = ({ title, children }) => (
  <>
    <Subheading>{title}</Subheading>
    <p>{children}</p>
  </>
);
