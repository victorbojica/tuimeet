import React, { useCallback } from "react";
import { Stack, Thumbnail, DropZone, Caption } from "@shopify/polaris";

export const RecordingUploadField = ({ value, onChange }) => {
  const handleDropZoneDrop = useCallback(
    (_dropFiles, acceptedFiles, _rejectedFiles) => onChange((files) => [...files, ...acceptedFiles]),
    [onChange]
  );

  const validImageTypes = ["image/gif", "image/jpeg", "image/png"];

  const fileUpload = !value.length && <DropZone.FileUpload />;
  const uploadedFiles = value.length > 0 && (
    <Stack vertical>
      {value.map((file, index) => (
        <Stack alignment="center" key={index}>
          <Thumbnail
            size="small"
            alt={file.name}
            source={
              validImageTypes.indexOf(file.type) > 0
                ? window.URL.createObjectURL(file)
                : "https://cdn.shopify.com/s/files/1/0757/9955/files/New_Post.png?12678548500147524304"
            }
          />
          <div>
            {file.name} <Caption>{file.size} bytes</Caption>
          </div>
        </Stack>
      ))}
    </Stack>
  );

  return (
    <DropZone onDrop={handleDropZoneDrop}>
      {uploadedFiles}
      {fileUpload}
    </DropZone>
  );
};
