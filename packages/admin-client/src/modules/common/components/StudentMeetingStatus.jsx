import React from "react";
import { Badge } from "@shopify/polaris";
import { inject, observer } from "mobx-react";
import { t } from "@lingui/macro";

export const StudentMeetingStatus = inject("globalStore")(
  observer(({ status, globalStore }) => {
    let badgeStatus;
    let badgeContent;
    if (status === "complete") {
      badgeStatus = "success";
      badgeContent = globalStore.i18n._(t`Complete`);
    } else if (status === "informed") {
      badgeStatus = "attention";
      badgeContent = globalStore.i18n._(t`Informed`);
    } else if (status === "waitinglist") {
      badgeStatus = "warning";
      badgeContent = globalStore.i18n._(t`Waitinglist`);
    } else if (status === "rejected") {
      badgeStatus = "critical";
      badgeContent = globalStore.i18n._(t`Rejected`);
    }

    return <Badge status={badgeStatus}>{badgeContent}</Badge>;
  })
);
