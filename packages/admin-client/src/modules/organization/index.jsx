import React from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { Layout } from "../common/components";
import { Dashboard } from "./Dashboard";
import { Students } from "./Students";
import { Student } from "./Student";
import { Meetings } from "./Meetings";
import { Settings } from "./Settings";

export const Organization = () => {
  const { path } = useRouteMatch();
  return (
    <Layout>
      <Switch>
        <Route exact path={`${path}/settings`}>
          <Settings />
        </Route>

        <Route exact path={`${path}`}>
          <Dashboard />
        </Route>

        <Route exact path={`${path}/students`}>
          <Students />
        </Route>

        <Route exact path={`${path}/meetings`}>
          <Meetings />
        </Route>

        <Route exact path={`${path}/students/:id`}>
          <Student />
        </Route>
      </Switch>
    </Layout>
  );
};
