import React from "react";
import { Page, Card, Layout, TextContainer } from "@shopify/polaris";
import { ItemDetail } from "../common/components/ItemDetail";

export const Dashboard = () => {
  return (
    <Page title="Dashboard" fullWidth>
      <Layout>
        <Layout.Section oneThird>
          <Card title="Students" sectioned>
            <TextContainer>
              <p>Some general statistics about how things are going</p>
              <ItemDetail title="Waiting">10</ItemDetail>
              <ItemDetail title="Completed">12</ItemDetail>
              <ItemDetail title="Rejected">9</ItemDetail>
              <ItemDetail title="Remaining">34</ItemDetail>
            </TextContainer>
          </Card>
        </Layout.Section>

        <Layout.Section oneThird>
          <Card title="Meetings" sectioned>
            <TextContainer>
              <p>Some general statistics about how things are going</p>
              <ItemDetail title="Average Duration">10:44</ItemDetail>
              <ItemDetail title="Total">60</ItemDetail>
              <ItemDetail title="Happening">3</ItemDetail>
            </TextContainer>
          </Card>
        </Layout.Section>

        <Layout.Section oneThird>
          <Card title="Teachers" sectioned>
            <TextContainer>
              <p>Some general statistics about how things are going</p>
              <ItemDetail title="Total">5</ItemDetail>
              <ItemDetail title="Available">4</ItemDetail>
              <ItemDetail title="Idle">1</ItemDetail>
              <ItemDetail title="In Meeting">3</ItemDetail>
              <ItemDetail title="Unavailable">1</ItemDetail>
            </TextContainer>
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  );
};
