import React from "react";
import { Page, Card, DataTable, Checkbox, Link } from "@shopify/polaris";
import { StudentMeetingStatus } from "../common/components/StudentMeetingStatus";
import { DataTableFooter } from "../common/components/DataTableFooter";

import rawData from "../../__data/meetings.json";

export const Meetings = () => {
  const data = rawData.data.map((row) => {
    const [
      ,
      studentId,
      studentFullName,
      teacherFullName,
      startDate,
      duration,
      status,
    ] = row;
    return [
      <Checkbox />,
      <Link url={`students/${studentId}`}>{studentFullName}</Link>,
      teacherFullName,
      <StudentMeetingStatus status={status} />,
      startDate,
      duration,
      status !== "waitinglist" ? <Link url="#">Download</Link> : "-",
    ];
  });

  return (
    <Page title="Meetings" fullWidth>
      <Card>
        <DataTable
          columnContentTypes={[
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
            "text",
          ]}
          hideScrollIndicator
          headings={[
            <Checkbox />,
            "Student",
            "Teacher",
            "Status",
            "Start Date",
            "Duration",
            "Recording",
          ]}
          rows={data}
          footerContent={<DataTableFooter data={data} />}
        />
      </Card>
    </Page>
  );
};
