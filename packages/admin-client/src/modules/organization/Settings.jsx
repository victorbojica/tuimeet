import React, { useState, useCallback } from "react";
import {
  Page,
  Card,
  Tabs,
  DataTable,
  Checkbox,
  Badge,
  Autocomplete,
  Stack,
  Tag,
  TextContainer,
  Button,
  FormLayout,
  TextField,
} from "@shopify/polaris";
import { useLocation, useHistory } from "react-router-dom";
import { DataTableFooter } from "../common/components/DataTableFooter";

import rawData from "../../__data/members.json";

const MembersSelect = ({ data }) => {
  const deselectedOptions = [
    { value: "owicklen8@wsj.com", label: "Orlan Wicklen" },
    { value: "antcpawson9@storify.comique", label: "Christel Pawson" },
  ];
  const [selectedOptions, setSelectedOptions] = useState([]);
  const [inputValue, setInputValue] = useState("");
  const [options, setOptions] = useState(deselectedOptions);

  const updateText = useCallback(
    (value) => {
      setInputValue(value);

      if (value === "") {
        setOptions(deselectedOptions);
        return;
      }

      const filterRegex = new RegExp(value, "i");
      const resultOptions = deselectedOptions.filter((option) =>
        option.label.match(filterRegex)
      );

      setOptions(resultOptions);
    },
    [deselectedOptions]
  );

  const removeTag = useCallback(
    (tag) => () => {
      const options = [...selectedOptions];
      options.splice(options.indexOf(tag), 1);
      setSelectedOptions(options);
    },
    [selectedOptions]
  );

  const tagsMarkup = selectedOptions.map((option) => {
    let { label } = deselectedOptions.find(({ value }) => value === option);
    return (
      <Tag key={`option${option}`} onRemove={removeTag(option)}>
        {label}
      </Tag>
    );
  });

  const textField = (
    <Autocomplete.TextField
      onChange={updateText}
      value={inputValue}
      placeholder="email@example.com"
    />
  );

  return (
    <>
      <Autocomplete
        allowMultiple
        options={options}
        selected={selectedOptions}
        textField={textField}
        onSelect={setSelectedOptions}
        listTitle="Suggested Tags"
      />
      <br />
      <TextContainer>
        <Stack>
          {tagsMarkup.length ? tagsMarkup : <Tag>Nothing added</Tag>}
        </Stack>
      </TextContainer>
    </>
  );
};

const OrganizationSection = () => {
  return (
    <Card.Section title="General">
      <FormLayout>
        <FormLayout.Group>
          <TextField label="Maximum Reschedules" type="number"></TextField>
          <TextField label="Minimum Meeting Time" type="number"></TextField>
        </FormLayout.Group>
        <div style={{ textAlign: "right" }}>
          <Button submit>Save</Button>
        </div>
      </FormLayout>
    </Card.Section>
  );
};

const MembersSection = () => {
  const data = rawData.data.map(
    ([, fullName, email, availability, enabled]) => [
      <Checkbox />,
      fullName,
      email,
      availability.length ? (
        availability.map((a) => <Badge status="success">{a}</Badge>)
      ) : (
        <Badge status="warning">No availability</Badge>
      ),
      enabled ? (
        <Button size="slim">Disable</Button>
      ) : (
        <Button primary size="slim">
          Enable
        </Button>
      ),
    ]
  );

  return (
    <>
      <Card.Section title="Existing members">
        <Card>
          <DataTable
            style={{ border: "1px solid black" }}
            hideScrollIndicator
            columnContentTypes={["text", "text", "text", "text"]}
            headings={[
              <Checkbox />,
              "Full Name",
              "Email",
              "Availability",
              "Enable",
            ]}
            rows={data}
            footerContent={<DataTableFooter data={data} />}
          />
        </Card>
      </Card.Section>
      <Card.Section title="Add Members">
        <MembersSelect />
        <div style={{ textAlign: "right" }}>
          <Button textAlign="right">Save</Button>
        </div>
      </Card.Section>
    </>
  );
};

export const Settings = () => {
  const { hash } = useLocation();
  const history = useHistory();

  const tabs = [
    {
      id: "members",
      accessibilityLabel: "Members",
      content: "Members",
      panelID: "members-content",
    },
    {
      id: "organization",
      content: "Organization",
      panelID: "organization-content",
    },
  ];

  const [selected, setSelected] = useState(0);
  const handleTabChange = useCallback(
    (selectedTabIndex) => history.push(`#${tabs[selectedTabIndex].id}`),
    [history, tabs]
  );

  const hashSelected = tabs.findIndex((tab) => tab.id === hash.slice(1));

  if (hashSelected !== -1 && hashSelected !== selected)
    setSelected(hashSelected);

  return (
    <Page title="Settings">
      <Card>
        <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}>
          {selected === 0 && <MembersSection />}
          {selected === 1 && <OrganizationSection />}
        </Tabs>
      </Card>
    </Page>
  );
};
