import React from "react";
import { Page, Card, DataTable, Checkbox, Link } from "@shopify/polaris";
import { StudentMeetingStatus } from "../common/components/StudentMeetingStatus";
import { DataTableFooter } from "../common/components/DataTableFooter";

import rawData from "../../__data/students.json";

export const Students = () => {
  const data = rawData.data.map((row) => {
    const [id, fullName, registeredAt, email, , status] = row;
    return [
      <Checkbox />,
      <Link url={`students/${id}`}>{fullName}</Link>,
      email,
      registeredAt,
      <StudentMeetingStatus status={status} />,
    ];
  });

  return (
    <Page title="Students" fullWidth>
      <Card>
        <DataTable
          columnContentTypes={["text", "text", "text", "text", "text"]}
          hideScrollIndicator
          headings={[
            <Checkbox />,
            "Full Name",
            "Email",
            "Registration Date",
            "Status",
          ]}
          rows={data}
          footerContent={<DataTableFooter data={data} />}
        />
      </Card>
    </Page>
  );
};
