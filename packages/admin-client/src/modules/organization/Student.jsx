import React from "react";
import { useParams } from "react-router-dom";
import { Page, Card, TextContainer, Link, DataTable } from "@shopify/polaris";
import { StudentMeetingStatus } from "../common/components/StudentMeetingStatus";
import { ItemDetail } from "../common/components/ItemDetail";

import rawData from "../../__data/students.json";

export const Student = () => {
  const { id } = useParams();
  let [, fullName, , email, , status] = rawData.data.find(
    ([_id]) => _id === id
  );

  return (
    <Page
      title={`${fullName}`}
      fullWidth
      breadcrumbs={[{ content: "Students", url: "/organization/students" }]}
    >
      <Card title="Info" sectioned>
        <TextContainer>
          <p>
            Some details about the <b>student</b>. Can be anything really. As
            i'm writing this, I realize that I'm out of words...
          </p>
          <ItemDetail title="Full Name">{fullName}</ItemDetail>
          <ItemDetail title="Email">{email}</ItemDetail>
          <ItemDetail title="ID">{id}</ItemDetail>
        </TextContainer>
      </Card>

      <Card title="Registration" sectioned>
        <TextContainer>
          <p>
            Some details about the <b>student's registration</b>. Can be
            anything really. As i'm writing this, I realize that I'm out of
            words...
          </p>
        </TextContainer>
      </Card>

      <Card>
        <Card.Header title="Meeting">
          <Link url="#">Download Recording</Link>
          <StudentMeetingStatus status={status} />
        </Card.Header>
        <Card.Section sectioned>
          <TextContainer>
            <p>
              Some details about the <b>student's meeting</b>. Can be anything
              really. As i'm writing this, I realize that I'm out of words...
            </p>
            <ItemDetail title="Teacher">
              John Doe - j.doe@example.com
            </ItemDetail>
            <ItemDetail title="Notes">
              Some meeting notes taken by the teacher
            </ItemDetail>
            <ItemDetail title="Meeting URL">
              <Link url="#">https://meet.google.com/ahd-scnh-uxk</Link>
            </ItemDetail>
            <ItemDetail title="Details">
              <DataTable
                columnContentTypes={["text", "text", "text", "text", "text"]}
                hideScrollIndicator
                headings={[
                  "Start Time",
                  "End Time",
                  "Duration",
                  "Time to response",
                  "Reschedules",
                ]}
                rows={[
                  [
                    "2020-03-29T05:51:03Z",
                    "2020-03-29T06:10:01Z",
                    "19:30",
                    "4:44",
                    "1",
                  ],
                ]}
              />
            </ItemDetail>
          </TextContainer>
        </Card.Section>
      </Card>
    </Page>
  );
};
