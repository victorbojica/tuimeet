import { setupI18n } from "@lingui/core";

import en from "./locale/en/messages";
import ro from "./locale/ro/messages";

const i18n = setupI18n();

i18n.load("en", en.messages);
i18n.load("ro", ro.messages);

i18n.activate("en");

export default i18n;
