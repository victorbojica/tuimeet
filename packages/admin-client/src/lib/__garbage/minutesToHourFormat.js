export const minutesToHourFormat = (totalMinutes) => {
  const negative = totalMinutes < 0;
  const _totalMinutes = Math.abs(totalMinutes);
  let hours = Math.floor(_totalMinutes / 60);
  let _hours = hours < 10 ? `0${hours}` : `${hours}`;

  let minutes = _totalMinutes - hours * 60;
  let _minutes = minutes < 10 ? `0${minutes}` : `${minutes}`;

  return `${negative ? "-" : ""}${_hours}:${_minutes}`;
};
