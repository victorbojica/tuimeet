import API from "./API";

export const getRegistration = (registrationId) => API.get(`member/registration/${registrationId}`);
