import API from "./API";

export const authenticate = (email, password) => API.post("auth/user", { email, password });
