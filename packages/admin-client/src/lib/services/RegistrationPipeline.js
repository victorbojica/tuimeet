import API from "./API";

export const listWaitinglist = () => API.get("member/waitinglist");

export const scheduleMeeting = (registrationId, { estimatedStartTime, estimatedDuration }) =>
  API.post(`member/pipeline/${registrationId}/schedule-meeting`, { estimatedStartTime, estimatedDuration });

export const rescheduleMeeting = (registrationId, { estimatedStartTime, estimatedDuration, reason }) =>
  API.post(`member/pipeline/${registrationId}/reschedule-meeting`, { estimatedStartTime, estimatedDuration, reason });

export const cancelMeeting = (registrationId, { reason }) =>
  API.post(`member/pipeline/${registrationId}/cancel-meeting`, { reason });

export const joinMeeting = (registrationId) => API.post(`member/pipeline/${registrationId}/join-meeting`);

export const completeMeeting = (registrationId, { notes, recording }) => {
  let payload = { notes };
  let config = {};

  if (recording && recording.length) {
    config = { headers: { "Content-Type": "multipart/form-data" } };
    payload = new FormData();
    payload.append("recording", recording[0]);
    payload.append("notes", notes);
  }

  return API.post(`member/pipeline/${registrationId}/complete-meeting`, payload, config);
};
