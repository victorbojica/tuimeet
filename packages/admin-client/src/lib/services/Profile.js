import API from "./API";

export const fecthProfle = () => API.get("member/profile");

export const updateProfile = ({ schedule, meetingDuration }) =>
  API.post("member/profile", { schedule, meetingDuration });
