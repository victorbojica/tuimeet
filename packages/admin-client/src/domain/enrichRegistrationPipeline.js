import * as DateUtils from "date-fns";
import { minutesToHourFormat } from "../lib/__garbage";
import _store from "store";
import { API_URL } from "../lib/services/API";

export const enrichRegistrationPipeline = ({ registration, meeting, waitinglist, user, ...rest }) => {
  if (!registration) registration = {};
  if (!waitinglist) waitinglist = {};
  if (!meeting) meeting = {};
  if (!user) user = {};

  if (registration.firstName && registration.lastName) {
    registration.fullName = `${registration.firstName} ${registration.lastName}`;
  } else {
    registration.fullName = null;
  }

  if (waitinglist.joinedAt) {
    waitinglist.minutesSinceJoined = () => {
      const diff = DateUtils.differenceInMinutes(new Date(), new Date(waitinglist.joinedAt));
      if (diff < 0) return null;
      return minutesToHourFormat(diff);
    };
  } else {
    waitinglist.minutesSinceJoined = () => null;
  }

  if (meeting.estimatedStartTime) meeting.estimatedStartTime = new Date(meeting.estimatedStartTime);
  if (meeting.estimatedStartTime) {
    meeting.startingInMinutes = () => {
      if (meeting.startedAt) return null;
      const diff = DateUtils.differenceInMinutes(meeting.estimatedStartTime, new Date());
      return minutesToHourFormat(diff);
    };
  } else {
    meeting.startingInMinutes = () => null;
  }

  if (meeting.startedAt) {
    meeting.startedForMinutes = () => {
      const diff = DateUtils.differenceInMinutes(new Date(), new Date(meeting.startedAt));
      if (diff < 0) return null;
      return minutesToHourFormat(diff);
    };
  } else {
    meeting.startedForMinutes = () => null;
  }

  if (meeting.recordingId) {
    const token = _store.get("accessToken");
    meeting.recordingUrl = `${API_URL}member/pipeline/${registration.id}/recording?tk=${token}`;
  } else {
    meeting.recordingUrl = null;
  }

  if (meeting.startedAt) meeting.startedAt = new Date(meeting.startedAt);
  if (meeting.endedAt) meeting.endedAt = new Date(meeting.endedAt);
  if (meeting.userJoinedAt) meeting.userJoinedAt = new Date(meeting.userJoinedAt);
  if (meeting.registrationJoinedAt) meeting.registrationJoinedAt = new Date(meeting.registrationJoinedAt);
  if (meeting.startedAt && meeting.endedAt) {
    meeting.duration = minutesToHourFormat(DateUtils.differenceInMinutes(meeting.endedAt, meeting.startedAt));
  } else {
    meeting.duration = null;
  }
  if (meeting.startedAt)
    meeting.timeToResponse = minutesToHourFormat(
      DateUtils.differenceInMinutes(meeting.startedAt, meeting.estimatedStartTime)
    );

  if (user.firstName && user.lastName) {
    user.fullName = `${user.firstName} ${user.lastName}`;
  } else {
    user.fullName = null;
  }

  return { registration, meeting, waitinglist, user, ...rest };
};
