# WARNING: This is purely informational. do not run this script as it hasn't been tested

apt update
apt install mariadb-server
echo "[mysqld]" >> /etc/mysql/my.cnf
echo "bind-address=0.0.0.0" >> /etc/mysql/my.cnf
echo "[server]" >> /etc/mysql/my.cnf
echo "default-time-zone=+00:00" >> /etc/mysql/my.cnf
systemctl restart mariadb
mysql -u root -e "CREATE DATABASE dev CHARACTER SET utf8mb4;"
mysql -u root -e "CREATE DATABASE ref CHARACTER SET utf8mb4;"
mysql -u root -e "CREATE USER 'admin'@'%' IDENTIFIED BY 'admin';"
mysql -u root -e "GRANT ALL PRIVILEGES ON * . * TO 'admin'@'%';"
mysql -u root -e "FLUSH PRIVILEGES;"

wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
nvm install 12.15.0
npm i -g pm2 typescript ts-node
echo "export NODE_ENV=production" >> /root/.profile
echo "export NODE_ENV=production" >> /root/.bashrc
source /root/.profile
source /root/.bashrc

apt install ufw
ufw allow 22
ufw allow 80
ufw allow 443
ufw allow 3306
ufw allow 63333

apt install nginx
apt install python3-acme python3-certbot python3-mock python3-openssl python3-pkg-resources python3-pyparsing python3-zope.interface
apt install python3-certbot-nginx

cd /root
mkdir tuimeet

# copy nginx configs
cat /root/provision/nginx.conf >> /etc/nginx/nginx.conf

cp /root/provision/api.tuimeet.wesync.io.conf /etc/nginx/sites-available/api.tuimeet.wesync.io.conf
cp /root/provision/admin.tuimeet.wesync.io.conf /etc/nginx/sites-available/admin.tuimeet.wesync.io.conf
cp /root/provision/student.tuimeet.wesync.io.conf /etc/nginx/sites-available/student.tuimeet.wesync.io.conf

ln -s /etc/nginx/sites-available/api.tuimeet.wesync.io.conf /etc/nginx/sites-enabled
ln -s /etc/nginx/sites-available/student.tuimeet.wesync.io.conf /etc/nginx/sites-enabled
ln -s /etc/nginx/sites-available/admin.tuimeet.wesync.io.conf /etc/nginx/sites-enabled

certbot --nginx -d admin.tuimeet.wesync.io -d www.admin.tuimeet.wesync.io
certbot --nginx -d student.tuimeet.wesync.io -d www.student.tuimeet.wesync.io
certbot --nginx -d api.tuimeet.wesync.io

cd /root
mkdir tuimeet

