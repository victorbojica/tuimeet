#!/usr/bin/env bash
set -e

npm set audit false

echo "[2.1] Cleaning up"
rm -rf /root/tuimeet/api && mkdir /root/tuimeet/api && mkdir /root/tuimeet/api/.tmp
rm -rf /root/tuimeet/student-client && mkdir /root/tuimeet/student-client
rm -rf /root/tuimeet/admin-client && mkdir /root/tuimeet/admin-client

echo "[2.1] Moving the app files"
echo "[2.1.1] Moving the api files"
cd /root/provision/builds
cp "$(ls -t | grep api | head -1)" /root/tuimeet/api/build.tar
cd /root/tuimeet/api
tar -xf build.tar
rm build.tar

echo "[2.1.2] Moving the student files"
cd /root/provision/builds
cp "$(ls -t | grep student | head -1)" /root/tuimeet/student-client/build.tar
cd /root/tuimeet/student-client
tar -xf build.tar
rm build.tar

echo "[2.1.3] Moving the admin files"
cd /root/provision/builds
cp "$(ls -t | grep admin | head -1)" /root/tuimeet/admin-client/build.tar
cd /root/tuimeet/admin-client
tar -xf build.tar
rm build.tar

echo "[2.2] Setting up the app"
echo "[2.2.1] Setting up the api"
cd /root/tuimeet/api
npm install --silent --no-warnings --no-progress --no-fund
cp /root/provision/.env.api .env.production
# TODO need to fix this
#npm run db:migrate:latest
pm2 -s restart pm2.config.js

echo "[2.2.2] Setting up the student client"
cd /root/tuimeet/student-client
cp ../../provision/config.student-client.js config.js

echo "[2.2.3] Setting up the admin client"
cd /root/tuimeet/admin-client
cp ../../provision/config.admin-client.js config.js

echo "[2.2.4] Finishing setting up"
systemctl restart nginx

echo "[3] Deploy done"

